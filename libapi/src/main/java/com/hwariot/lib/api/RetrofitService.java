package com.hwariot.lib.api;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.reactivex.Flowable;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.HTTP;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Part;
import retrofit2.http.QueryMap;
import retrofit2.http.Url;

public interface RetrofitService {

    @GET
    Flowable<String> getListDataWithQuery(@Url String url, @QueryMap Map<String, String> map);

    @GET
    Flowable<String> getData(@Url String url);

    @PUT
    Flowable<String> putData(@Url String url, @Body RequestBody request);

    @PUT
    Flowable<String> putNoBody(@Url String url);

    @POST
    Flowable<String> postNoBody(@Url String url);

    @POST
    Flowable<String> postData(@Url String url, @Body RequestBody request);

    @POST
    Flowable<String> uploadOneFile(@Url String url, @Body RequestBody Body);

    @Multipart
    @POST
    Flowable<String> uploadOneFileWithParams(@Url String url, @Part List<MultipartBody.Part> file);

    @DELETE
    Flowable<String> deleteData(@Url String url);

    @HTTP(method = "DELETE", hasBody = true)
    Flowable<String> deleteDataWithParams(@Url String url, @Body HashMap<String, String> params);


}
