package com.hwariot.lib.api;

import android.content.Context;
import android.util.Log;

import com.hwariot.lib.APILibAPP;
import com.hwariot.lib.bean.BaseBean;
import com.hwariot.lib.bean.StatusBean;

import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import okhttp3.Cache;
import okhttp3.CacheControl;
import okhttp3.Interceptor;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;

/***
 * @date 创建时间 2018/4/19 22:53
 * @author 作者: yulong
 * @description 通用的网络请求封装库
 */
public class PresenterSingleton {
    private static volatile Map<String, PresenterSingleton> singletonMap = new HashMap<>();
    protected CompositeDisposable compositeDisposable;
    protected RetrofitService retrofitService;

    /*** 缓存时间 7天*/
    private static final int CACHE_TIME = 60 * 60 * 24 * 7;
    /*** 设置网络请求超时时间*/
    private static final int TIME_OUT = 30;
    private volatile OkHttpClient okHttpClient;

    public static void clearAllData() {
        singletonMap.clear();
    }

    public static PresenterSingleton get(String service) {
        if (singletonMap.get(service) == null) {
            synchronized (PresenterSingleton.class) {
                if (singletonMap.get(service) == null) {
                    PresenterSingleton singleton = new PresenterSingleton(service);
                    singletonMap.put(service, singleton);
                }
            }
        }
        return singletonMap.get(service);
    }

    public static PresenterSingleton get() {
        return get(APILibAPP.getDefaultService());
    }


    private PresenterSingleton(String service) {

        Retrofit retrofit = new Retrofit.Builder().baseUrl(APILibAPP.getBaseAPIUrl(service))
                .client(getOkHttpClient())
                .addConverterFactory(new ToStringConverterFactory())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build();
        retrofitService = retrofit.create(RetrofitService.class);

        compositeDisposable = new CompositeDisposable();
    }


    private OkHttpClient getOkHttpClient() {
        if (okHttpClient == null) {
            synchronized (PresenterSingleton.class) {
                Cache cache = new Cache(new File(APILibAPP.get().getCacheDir() + "HttpCache"),
                        1024 * 1024 * 100);
                if (okHttpClient == null) {
                    HttpLogInterceptor logInterceptor = new HttpLogInterceptor();

                    okHttpClient = new OkHttpClient.Builder()
                            .cache(cache)
                            .connectTimeout(TIME_OUT, TimeUnit.SECONDS)
                            .readTimeout(TIME_OUT, TimeUnit.SECONDS)
                            .writeTimeout(TIME_OUT, TimeUnit.SECONDS)
                            .addInterceptor(interceptor)
                            .addInterceptor(headerInterceptor)
                            .addNetworkInterceptor(logInterceptor)
                            .build();
                }
            }
        }
        return okHttpClient;
    }

    /**
     * 配置缓存策略
     * 有网络读服务器最新数据，没网读缓存
     */
    private final Interceptor interceptor = new Interceptor() {
        @Override
        public Response intercept(Chain chain) throws IOException {
            Request request = chain.request();
            if (!NetWorkUtils.isNetworkAvailable()) {
                request = request.newBuilder().cacheControl(CacheControl.FORCE_CACHE).build();
            }
            Response response = chain.proceed(request);
            if (NetWorkUtils.isNetworkAvailable()) {
                response.newBuilder().header("Cache-Control", "public, max-age=" + 0)
                        .removeHeader("Pragma")
                        .build();
            } else {
                response.newBuilder().header("Cache-Control", "public, only-if-cached, max-stale=" + CACHE_TIME)
                        .removeHeader("Pragma")
                        .build();
            }

            return response;
        }
    };

    private final Interceptor headerInterceptor = new Interceptor() {
        @Override
        public Response intercept(Chain chain) throws IOException {
            Request original = chain.request();

            Request request = original.newBuilder()
                    .header("Authorization", APILibAPP.getToken())
                    .header("version", APILibAPP.getAPIVersion())
                    .method(original.method(), original.body())
                    .build();

            return chain.proceed(request);
        }
    };


    protected RequestBody createRequestBody(String content) {
        return RequestBody.create(MediaType.parse("application/json"), content);
    }


    /***************************POST请求********************************************************/

    public void uploadFileWithParams(String url, File file, Map<String, String> map, BaseViewInterface viewInterface) {
        List<File> list = new ArrayList<>();
        list.add(file);
        uploadFileWithParams(url, list, map, viewInterface);
    }

    public void uploadFileWithParams(String url, List<File> fileList, Map<String, String> map, BaseViewInterface viewInterface) {
        MultipartBody.Builder builder = new MultipartBody.Builder().setType(MultipartBody.FORM);//表单类型
        if (map != null) {
            for (String key : map.keySet()) {
                builder.addFormDataPart(key, map.get(key));
            }
        }

        for (File file : fileList) {
            RequestBody imageBody = RequestBody.create(MediaType.parse("image/jpeg"), file);
            builder.addFormDataPart("file", file.getName(), imageBody);
        }
        retrofitService.uploadOneFileWithParams(url, builder.build().parts())
                .subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<String>() {
                    private boolean isDoOnNext = false;

                    @Override
                    public void onSubscribe(Subscription s) {
                    }

                    @Override
                    public void onNext(String s) {
                        viewInterface.initData(url, s);
                        isDoOnNext = true;
                        Log.d("Test", "onNext = " + s);
                    }

                    @Override
                    public void onError(Throwable t) {
                        BaseBean baseBean = new BaseBean();
                        StatusBean statusBean = new StatusBean();
                        statusBean.setMessage("Error");
                        baseBean.setStatusBean(statusBean);
                        viewInterface.onFail(url, baseBean);
                    }

                    @Override
                    public void onComplete() {
                        if (!isDoOnNext) {
                            viewInterface.initData(url, "");
                        }
                    }
                });
    }


    public void doUploadOneFileAction(Context context, String api, File file, Map<String, String> params, Type type,
                                      BaseViewInterface viewInterface) {
        MultipartBody.Builder builder = new MultipartBody.Builder().setType(MultipartBody.FORM)
                .addFormDataPart("file", file.getName(), RequestBody.create(MediaType.parse("multipart/form-data"), file));
        if (params != null) {
            for (String key : params.keySet()) {
                builder.addFormDataPart(key, params.get(key));
            }
        }
        compositeDisposable.add(getUploadDisposable(context, api, builder.build(), type, viewInterface));
    }

    private <T> Disposable getUploadDisposable(Context context, String api, RequestBody body, Type type, BaseViewInterface viewInterface) {
        return retrofitService.uploadOneFile(api, body)
                .map(s -> {
                    BaseBean<T> data = JsonUtil.parseJsonToObject(s, type);
                    return data;
                })
                .compose(RxUtil.handleResult())
                .subscribeWith(new ABSNetWorkResponseProxy<T>(compositeDisposable, context) {
                    @Override
                    protected void onNextAction(T t) {
                        if (t != null) {
                            viewInterface.initData(api, t);
                        }
                    }

                    @Override
                    protected void onErrorAction(BaseBean msg) {
                        viewInterface.onFail(api, msg);
                    }
                });
    }


    /*Post请求*/
    public void doPostAction(String api, Map<String, String> postMap, Type type, BaseViewInterface viewInterface) {
        doPostAction(null, api, postMap, type, viewInterface);
    }


    /*带有Loading的Post请求*/
    public void doPostAction(Context context, String api, Map<String, String> map, Type type, BaseViewInterface viewInterface) {
        String postJson = JsonUtil.objectToJson(map);
        compositeDisposable.add(getPostDisposable(context, api, postJson, type, viewInterface));
    }

    public void doPostAction(Context context, String api, Type type, BaseViewInterface viewInterface) {
        compositeDisposable.add(getPostNobodyDisposable(context, api, type, viewInterface));
    }

    public void doPostAction(String api, String postJson, Type type, BaseViewInterface viewInterface) {
        doPostAction(null, api, postJson, type, viewInterface);
    }

    public void doPostAction(Context context, String api, String postJson, Type type, BaseViewInterface viewInterface) {
        compositeDisposable.add(getPostDisposable(context, api, postJson, type, viewInterface));
    }

    /*Post的请求,可直接传实体类*/
    public <E> void doPostActionWithEntity(String api, E e, Type type, BaseViewInterface viewInterface) {
        doPostActionWithEntity(null, api, e, type, viewInterface);
    }

    /*Post的请求,可直接传实体类*/
    public <E> void doPostActionWithEntity(Context context, String api, E e, Type type, BaseViewInterface viewInterface) {
        compositeDisposable.add(getPostDisposable(context, api, JsonUtil.objectToJson(e), type, viewInterface));
    }

    private <T> Disposable getPostNobodyDisposable(Context context, String api, Type type, BaseViewInterface viewInterface) {
        return retrofitService.postNoBody(api)
                .map(s -> {
                    BaseBean<T> data = JsonUtil.parseJsonToObject(s, type);
                    return data;
                })
                .compose(RxUtil.handleResult())
                .subscribeWith(new ABSNetWorkResponseProxy<T>(compositeDisposable, context) {
                    @Override
                    protected void onNextAction(T t) {
                        if (t != null) {
                            viewInterface.initData(api, t);
                        }
                    }

                    @Override
                    protected void onErrorAction(BaseBean msg) {
                        viewInterface.onFail(api, msg);
                    }
                });
    }

    private <T> Disposable getPostDisposable(Context context, String api, String postJson, Type type, BaseViewInterface viewInterface) {
        return retrofitService.postData(api, createRequestBody(postJson))
                .map(s -> {
                    BaseBean<T> data = JsonUtil.parseJsonToObject(s, type);
                    return data;
                })
                .compose(RxUtil.handleResult())
                .subscribeWith(new ABSNetWorkResponseProxy<T>(compositeDisposable, context) {
                    @Override
                    protected void onNextAction(T t) {
                        if (t != null) {
                            viewInterface.initData(api, t);
                        }
                    }

                    @Override
                    protected void onErrorAction(BaseBean msg) {
                        viewInterface.onFail(api, msg);
                    }
                });
    }

/***************************GET请求********************************************************/
    /*** 不带参数的Get请求*/
    public void doGetData(String api, Type type, BaseViewInterface viewInterface) {
        doGetData(null, api, type, viewInterface);
    }

    public void doGetData(Context context, String api, Type type, BaseViewInterface viewInterface) {
        compositeDisposable.add(getDoGetDisposable(context, api, type, viewInterface));
    }

    private <T> Disposable getDoGetDisposable(Context context, String api, Type type, BaseViewInterface viewInterface) {
        return retrofitService.getData(api)
                .map(s -> {
                    BaseBean<T> data = JsonUtil.parseJsonToObject(s, type);
                    return data;
                })
                .compose(RxUtil.handleResult())
                .subscribeWith(new ABSNetWorkResponseProxy<T>(compositeDisposable, context) {
                    @Override
                    protected void onNextAction(T t) {
                        if (t != null) {
                            viewInterface.initData(api, t);
                        }
                    }

                    @Override
                    protected void onErrorAction(BaseBean msg) {
                        viewInterface.onFail(api, msg);
                    }
                });
    }

    /*** 带有参数的Get请求*/
    public void doGetData(Context context, String api, Map<String, String> queryMap, Type type, BaseViewInterface viewInterface) {
        compositeDisposable.add(getDoGetWithMapDisposable(context, api, queryMap, type, viewInterface));
    }

    private <T> Disposable getDoGetWithMapDisposable(Context context, String api, Map<String, String> queryMap, Type type, BaseViewInterface viewInterface) {
        return retrofitService.getListDataWithQuery(api, queryMap)
                .map(s -> {
                    BaseBean<T> data = JsonUtil.parseJsonToObject(s, type);
                    return data;
                })
                .compose(RxUtil.handleResult())
                .subscribeWith(new ABSNetWorkResponseProxy<T>(compositeDisposable, context) {
                    @Override
                    protected void onNextAction(T t) {
                        if (t != null) {
                            viewInterface.initData(api, t);
                        }
                    }

                    @Override
                    protected void onErrorAction(BaseBean msg) {
                        viewInterface.onFail(api, msg);
                    }
                });
    }


    /**********************PUT请求*****************************************************/
    public void doPutAction(Context context, String api, Map<String, String> putMap, Type type, BaseViewInterface viewInterface) {
        compositeDisposable.add(getPutDisposable(context, api, JsonUtil.objectToJson(putMap), type, viewInterface));
    }

    public void doPutAction(Context context, String api, String requestBody, Type type, BaseViewInterface viewInterface) {
        compositeDisposable.add(getPutDisposable(context, api, requestBody, type, viewInterface));
    }

    public void doPutAction(Context context, String api, Type type, BaseViewInterface viewInterface) {
        compositeDisposable.add(getDoPutDisposable(context, api, type, viewInterface));
    }

    private <T> Disposable getDoPutDisposable(Context context, String api, Type type, BaseViewInterface viewInterface) {
        return retrofitService.putNoBody(api)
                .map(s -> {
                    BaseBean<T> data = JsonUtil.parseJsonToObject(s, type);
                    return data;
                })
                .compose(RxUtil.handleResult())
                .subscribeWith(new ABSNetWorkResponseProxy<T>(compositeDisposable, context) {
                    @Override
                    protected void onNextAction(T t) {
                        if (t != null) {
                            viewInterface.initData(api, t);
                        }
                    }

                    @Override
                    protected void onErrorAction(BaseBean msg) {
                        viewInterface.onFail(api, msg);
                    }
                });
    }

    private <T> Disposable getPutDisposable(Context context, String api, String postJson, Type type, BaseViewInterface viewInterface) {
        return retrofitService.putData(api, createRequestBody(postJson))
                .map(s -> {
                    BaseBean<T> data = JsonUtil.parseJsonToObject(s, type);
                    return data;
                })
                .compose(RxUtil.handleResult())
                .subscribeWith(new ABSNetWorkResponseProxy<T>(compositeDisposable, context) {
                    @Override
                    protected void onNextAction(T t) {
                        if (t != null) {
                            viewInterface.initData(api, t);
                        }
                    }

                    @Override
                    protected void onErrorAction(BaseBean msg) {
                        viewInterface.onFail(api, msg);
                    }
                });
    }


    /****************DELETE操作*****************************************/
    public void doDeleteAction(Context context, String api, Type type, BaseViewInterface viewInterface) {
        compositeDisposable.add(getDeleteDisposableByBean(context, api, type, viewInterface));
    }

    private <T> Disposable getDeleteDisposableByBean(Context context, String api, Type type, BaseViewInterface viewInterface) {
        Disposable disposable = retrofitService.deleteData(api)
                .map(s -> {
                    BaseBean<T> data = JsonUtil.parseJsonToObject(s, type);
                    return data;
                })
                .compose(RxUtil.handleResult())
                .subscribeWith(new ABSNetWorkResponseProxy<T>(compositeDisposable, context) {
                    @Override
                    protected void onNextAction(T t) {
                        if (t != null) {
                            viewInterface.initData(api, t);
                        }
                    }

                    @Override
                    protected void onErrorAction(BaseBean msg) {
                        viewInterface.onFail(api, msg);
                    }
                });
        return disposable;
    }


}
