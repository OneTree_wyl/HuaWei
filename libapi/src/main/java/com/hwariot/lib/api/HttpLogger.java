package com.hwariot.lib.api;

import okhttp3.logging.HttpLoggingInterceptor;

/***
 * @date 创建时间 2018/6/29 23:29
 * @author 作者: W.YuLong
 * @description
 */
public class HttpLogger implements HttpLoggingInterceptor.Logger {
    @Override
    public void log(String message) {
        APILog.i("HttpLogInfo:" + message);
    }
}
