package com.hwariot.lib.api;

import android.content.Context;

import com.hwariot.lib.bean.BaseBean;
import com.hwariot.lib.bean.StatusBean;
import com.hwariot.lib.tools.dialog.loadingdialog.LoadingDialogManagerSingleton;

import java.io.IOException;

import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.subscribers.DisposableSubscriber;
import retrofit2.HttpException;

/***
 *@date 创建时间 2018/5/7 20:16
 *@author 作者: YuLong
 *@description 代理 RxJava得到的网络请求返回，用于统一处理一些操作，比如异常情况
 */
public abstract class ABSNetWorkResponseProxy<T> extends DisposableSubscriber<T> {
    private CompositeDisposable compositeDisposable;
    private Context mContext;


    public ABSNetWorkResponseProxy(CompositeDisposable mCompositeSubscription) {
        this.compositeDisposable = mCompositeSubscription;
    }


    /**/
    public ABSNetWorkResponseProxy(CompositeDisposable mCompositeSubscription, Context context) {
        this.mContext = context;
        this.compositeDisposable = mCompositeSubscription;
    }

    /**
     * 这个一定要有 Presenter的逻辑在这里处理
     */
    @Override
    public void onNext(T t) {
        onNextAction(t);
    }

    @Override
    public void onError(Throwable e) {
        e.printStackTrace();
        //隐藏dialog
        LoadingDialogManagerSingleton.getInstance().hideDialog();
        if (!NetWorkUtils.isNetworkAvailable()) {
            //ToastUtil.toast(R.string.network_error);
        } else if (e instanceof RxUtil.ServerException) {
            String s = ((RxUtil.ServerException) e).getMsg();

            //token 过期了
            BaseBean baseBean = new BaseBean();
            StatusBean statusBean = new StatusBean();
            statusBean.setMessage(s);
            statusBean.setState(((RxUtil.ServerException) e).getState() + "");
            statusBean.setTime(System.currentTimeMillis());
            baseBean.setStatusBean(statusBean);
            onErrorAction(baseBean);
        } else {
            if (e instanceof HttpException) {
                HttpException exception = (HttpException) e;
                try {
                    String errorInfo = exception.response().errorBody().string();
                    BaseBean data = JsonUtil.parseJsonToObject(errorInfo, BaseBean.class);
                    onErrorAction(data);
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
            } else {
                onErrorAction(new BaseBean());
            }
        }
        if (compositeDisposable != null) {
            compositeDisposable.remove(this);
        }
    }

    @Override
    public void onComplete() {
        if (compositeDisposable != null) {
            compositeDisposable.remove(this);
        }
        //隐藏dialog
        LoadingDialogManagerSingleton.getInstance().hideDialog();
    }

    @Override
    public void onStart() {
        super.onStart();
        if (mContext != null) {
            //显示dialog
            LoadingDialogManagerSingleton.getInstance().showDialog(mContext);
        }
    }

    /**
     * 代理onNext的方法
     *
     * @param t
     */
    protected abstract void onNextAction(T t);

    /**
     * 代理OnError的方法
     *
     * @param msg
     */
    protected abstract void onErrorAction(BaseBean msg);

}
