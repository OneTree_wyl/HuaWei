package com.hwariot.lib.bean;

import com.google.gson.annotations.SerializedName;

/***
 * @date 创建时间 2018/3/22 15:54
 * @author 作者: yulong
 * @description 通用的状态bean
 */
public class StatusBean {


    /**
     * state : success
     * message : ok
     * time : 1538811073
     */

    @SerializedName("state") private String state;
    @SerializedName("message") private String message;
    @SerializedName("time") private long time;

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }
}
