package com.hwariot.lib.bean;

import com.google.gson.annotations.SerializedName;

/***
 *@date 创建时间 2018/5/7 18:46
 *@author 作者: YuLong
 *@description 所有Bean的基类
 */
public class BaseBean<T> {
    @SerializedName("status") private StatusBean statusBean;
    @SerializedName("data") private T data;

    public StatusBean getStatusBean() {
        return statusBean;
    }

    public void setStatusBean(StatusBean statusBean) {
        this.statusBean = statusBean;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }
}
