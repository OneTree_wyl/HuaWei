package com.hwariot.lib.tools.dialog.loadingdialog;

import android.content.Context;

/***
 * @date 创建时间 2018/3/23 10:44
 * @author 作者: liweifeng
 * @description 描述：加载等待的dialog
 *
 */
public final class LoadingDialogManagerSingleton implements LoadingDialogInterface {
    private LoadingDialog loadingDialog;

    public static LoadingDialogManagerSingleton getInstance(){
        return LoadingDialogManagerHolder.instance;
    }

    private static class LoadingDialogManagerHolder{
        private static final LoadingDialogManagerSingleton instance = new LoadingDialogManagerSingleton();
    }

    @Override
    public void showDialog(Context context) {
        if(loadingDialog == null){
            loadingDialog = new LoadingDialog(context)
                    .setMsg("加载中...")
                    .setDialogCancelable(false)
                    .setDialogOnCancelListener(null)
                    .showDialog();
        }

    }

    @Override
    public void hideDialog() {
        if(loadingDialog != null){
            loadingDialog.dismiss();
            loadingDialog = null;
        }
    }
}
