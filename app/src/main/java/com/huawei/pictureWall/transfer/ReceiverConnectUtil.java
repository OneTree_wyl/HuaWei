package com.huawei.pictureWall.transfer;

import android.text.TextUtils;

import com.huawei.pictureWall.tools.singleton.SPSingleton;
import com.huawei.pictureWall.tools.util.APPUtil;
import com.huawei.pictureWall.tools.util.CLog;
import com.huawei.pictureWall.tools.util.Configuration;
import com.huawei.pictureWall.tools.util.SPDefine;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.SocketException;
import java.net.SocketTimeoutException;

/***
 * @date 创建时间 2018/8/21 21:46
 * @author 作者: W.YuLong
 * @description 接收广播信息的类
 */
public class ReceiverConnectUtil {
    public static final int waitTime = 60 * 1000;
    private int port;


    public ReceiverConnectUtil(int port) {
        this.port = port;
    }

    public DatagramPacket waitClient() {
        //server wait and receive message broadcasted by client
        DatagramPacket connectPacket = null;
        byte[] receiveByte = new byte[Configuration.BUFFER_LENGTH];
        DatagramSocket datagramSocket = null;
        try {
            datagramSocket = new DatagramSocket(port);//设置服务器端口, 监听广播信息
            datagramSocket.setSoTimeout(waitTime);
            DatagramPacket messageDatagramPacket = new DatagramPacket(receiveByte, receiveByte.length);
            datagramSocket.receive(messageDatagramPacket);//接收client的广播信息
            String msgStr = APPUtil.byte2String(messageDatagramPacket.getData());
            CLog.debug("接收的报文:" + msgStr);

            String portStr = SPSingleton.get().getString(SPDefine.KEY_TCP_Port, "");
            int port = Configuration.currentTcpPort;
            if (!TextUtils.isEmpty(portStr)) {
                port = Integer.parseInt(portStr);
            }

            //将服务器的主机名发送给client
            messageDatagramPacket.setData((port + Configuration.DELIMITER).getBytes("utf-8"));

            datagramSocket.send(messageDatagramPacket);//回复信息tcp要使用的Tcp端口给client
            messageDatagramPacket.setData(msgStr.getBytes("utf-8"));

            connectPacket = messageDatagramPacket;
        } catch (SocketTimeoutException e) {
//            progressListener.updateProgress(-3, 100, 100, 999);
            CLog.e("udp server 等待超时");
        } catch (UnsupportedEncodingException e) {
            CLog.e("编码解释错误" + e.getMessage());
        } catch (SocketException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (datagramSocket != null) {
                datagramSocket.close();
            }
        }
        return connectPacket;//将client发送的数据包返回给调用者, 里面包含client的地址, 端口, 主机名
    }

}
