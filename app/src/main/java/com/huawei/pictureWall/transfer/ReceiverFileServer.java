package com.huawei.pictureWall.transfer;

import android.text.TextUtils;

import com.huawei.pictureWall.tools.FileDesc;
import com.huawei.pictureWall.tools.singleton.SPSingleton;
import com.huawei.pictureWall.tools.util.CLog;
import com.huawei.pictureWall.tools.util.Configuration;
import com.huawei.pictureWall.tools.util.JsonUtil;
import com.huawei.pictureWall.tools.util.SPDefine;
import com.huawei.pictureWall.tools.util.Utils;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketTimeoutException;

public class ReceiverFileServer {
    private ServerSocket serverSocket;
    private Socket channelSocket;
    private BufferedInputStream bufferedInputStream;
    private BufferedOutputStream bufferedOutputStream;

    public ReceiverFileServer() {
    }


    public FileDesc getClientFileDesc() {
        FileDesc fileDesc = null;

        byte[] inputBuf = new byte[Configuration.BUFFER_LENGTH];

        try {
            String portStr = SPSingleton.get().getString(SPDefine.KEY_TCPPort, "");
            int port = Configuration.currentTcpPort;
            if (!TextUtils.isEmpty(portStr)) {
                port = Integer.parseInt(portStr);
            }

            serverSocket = new ServerSocket(port);//创建tcp服务器, 接收文件

            serverSocket.setSoTimeout(Configuration.WAIT_TIME_SECOND * 1000);//设置等待连接时长
            CLog.d("tcp server is waiting");
            channelSocket = serverSocket.accept();//建立链接
            channelSocket.setKeepAlive(Boolean.TRUE);
            CLog.d("datagramSocket.accept()");

            //获取socket的输入输出流
            bufferedInputStream = new BufferedInputStream(channelSocket.getInputStream());
            bufferedOutputStream = new BufferedOutputStream(channelSocket.getOutputStream());

            bufferedInputStream.read(inputBuf);//读取要接收文件的描述信息<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
            String fileInfo = Utils.getMessage(inputBuf);
            CLog.d("接收到的json: " + fileInfo);
            fileDesc = JsonUtil.parseJsonToObject(fileInfo, FileDesc.class);
            //将客户端发送的信息原封回复, 表示可以开始传输文件>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
            bufferedOutputStream.write((Utils.getMessage(inputBuf) + Configuration.DELIMITER).getBytes("utf-8"));
            CLog.debug("将客户端发送的信息原封回复, 表示可以开始传输文件>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
            bufferedOutputStream.flush();
        } catch (SocketTimeoutException e) {
            try {
                serverSocket.close();
            } catch (IOException e1) {
                e1.printStackTrace();
            }
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return fileDesc;
    }

    public void receiveFile(FileDesc fileDesc,String newFileName, String savePath) {
        byte[] receiveBytes = new byte[Configuration.BUFFER_LENGTH];

        try {
            String msg = "";
            long hasReceive = 0;
            int actualLen;

            String filePath = savePath + newFileName;

            File newFile = new File(filePath);
            newFile.createNewFile();
            newFile.setWritable(true);
            FileOutputStream fileOutputStream = new FileOutputStream(newFile);
            CLog.d("接收即将要发送的文件");
            bufferedInputStream.read(receiveBytes);//接收即将要发送的文件<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
            msg = Utils.getMessage(receiveBytes);
//            msg += Configuration.DELIMITER;
            CLog.d("发送准备接收的确认,msg:" + msg);

            //发送准备接收的确认>>>>>>>>>>>>>>>>>>>>>>>>>>>
            bufferedOutputStream.write(msg.getBytes("utf-8"));
            bufferedOutputStream.flush();

//                startTime = System.nanoTime();
            if (fileDesc.getLength() == 0) { //接收空文件处理
                return;
            }

            //从网络中读取文件字节流
            byte[] buffer = new byte[Configuration.BUFFER_LENGTH];
            while ((actualLen = bufferedInputStream.read(buffer, 0, Configuration.BUFFER_LENGTH)) > 0) {
                //将网络中的字节流写入本地文件
                fileOutputStream.write(buffer, 0, actualLen);
                hasReceive += actualLen;
                // recieve all part of file
                if (hasReceive == fileDesc.getLength()) {
                    bufferedOutputStream.flush();
                    CLog.debug("接收完成");
                    break;
                }
            }

            if (hasReceive == fileDesc.getLength()) {
                String sizeAck = hasReceive + Configuration.DELIMITER;
                bufferedOutputStream.write(sizeAck.getBytes("utf-8"));
                bufferedOutputStream.flush();
            }

        } catch (FileNotFoundException e1) {
            e1.printStackTrace();
            CLog.e("FileNotFoundException :" + e1.getMessage());
        } catch (IOException e) {
            e.printStackTrace();
            CLog.e("出现了异常,IOException:" + e.getMessage());
        }
    }

    public void close() {
        try {
            bufferedInputStream.close();
            bufferedOutputStream.close();
            serverSocket.close();
            channelSocket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
