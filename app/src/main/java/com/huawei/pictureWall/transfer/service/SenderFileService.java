package com.huawei.pictureWall.transfer.service;


import android.app.AlarmManager;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.media.MediaPlayer;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Vibrator;
import android.support.annotation.RequiresApi;

import com.huawei.pictureWall.R;
import com.huawei.pictureWall.tools.singleton.SPSingleton;
import com.huawei.pictureWall.tools.util.APPUtil;
import com.huawei.pictureWall.tools.util.CLog;
import com.huawei.pictureWall.tools.util.PathUtil;
import com.huawei.pictureWall.tools.util.SPDefine;
import com.huawei.pictureWall.tools.util.ToastUtil;
import com.huawei.pictureWall.transfer.CustomHandlerThread;
import com.huawei.pictureWall.transfer.HostAddress;
import com.huawei.pictureWall.transfer.SendFileClient;
import com.huawei.pictureWall.transfer.SenderConnectUtil;
import com.huawei.pictureWall.transfer.WakeReceiver;
import com.huawei.pictureWall.ui.MainActivity;

import java.io.File;


/***
 *@date 创建时间 2018/10/3 15:10
 *@author 作者: W.YuLong
 *@description 摇一摇传图的后台服务
 */
public class SenderFileService extends Service {

    /*定时唤醒的时间间隔*/
    private final static int ALARM_INTERVAL = 2 * 60 * 1000;
    private final static int WAKE_REQUEST_CODE = 6666;

    private final static int GRAY_SERVICE_ID = 999;

    private static final int MSG_TYPE_SUCCESS = 1;
    private static final int MSG_TYPE_FAILURE = -1;

    private static final int MSG_TYPE_SEND_IMAGE = 2;

    private SensorManager sensorManager;
    private Vibrator vibrator;

    private volatile boolean isRunning = false;

    private long lastSendTime;
    //当上次传图之后，等待6秒之后再传图
    private long waitTime = 5000;

    private CustomHandlerThread customHandlerThread;

    @Override
    public void onCreate() {
        super.onCreate();


        sensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
        // 第一个参数是Listener，第二个参数是所得传感器类型，第三个参数值获取传感器信息的频率
        sensorManager.registerListener(sensorEventListener, sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER),
                SensorManager.SENSOR_DELAY_NORMAL);
    }


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        CLog.i("ShakeListenerService->onStartCommand");


        Intent innerIntent = new Intent(this, GrayInnerService.class);
        startService(innerIntent);

        startMyNotification(this);


        Intent serviceIntent = new Intent(this, NotifyService.class);
        startService(serviceIntent);


        //发送唤醒广播来促使挂掉的UI进程重新启动起来
        AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        Intent alarmIntent = new Intent();
        alarmIntent.setAction(WakeReceiver.GRAY_WAKE_ACTION);
        PendingIntent operation = PendingIntent.getBroadcast(this, WAKE_REQUEST_CODE, alarmIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        alarmManager.setInexactRepeating(AlarmManager.RTC_WAKEUP, System.currentTimeMillis(), ALARM_INTERVAL, operation);

        return START_STICKY;
    }


    public static void startMyNotification(Service service) {
        NotificationManager notificationManager = (NotificationManager) service.getSystemService(Context.NOTIFICATION_SERVICE);
        String channelId = "SendImageNotify";
        NotificationChannel notificationChannel = null;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            notificationChannel = new NotificationChannel(channelId, channelId, NotificationManager.IMPORTANCE_HIGH);
            notificationChannel.setDescription(channelId);
            notificationChannel.setSound(null, null);
            notificationManager.createNotificationChannel(notificationChannel);

            Intent intent = new Intent(service, MainActivity.class);
            PendingIntent pi = PendingIntent.getActivity(service, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);

            Notification notification = new Notification.Builder(service, channelId)
                    .setContentTitle("照片投屏")
                    .setContentText("华为照片投屏")
                    .setContentIntent(pi)
                    .build();
            service.startForeground(GRAY_SERVICE_ID, notification);
        } else {
            service.startForeground(GRAY_SERVICE_ID, new Notification());
        }
    }


    @Override
    public IBinder onBind(Intent intent) {
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR2)
    @Override
    public void onDestroy() {
        super.onDestroy();
        if (sensorManager != null) {
            sensorManager.unregisterListener(sensorEventListener);
        }
        if (customHandlerThread != null) {
            customHandlerThread.quitSafely();
            customHandlerThread = null;
        }
        CLog.e("服务关闭 onDestroy");
    }


    /*发送处理失败的消息*/
    private void sendFailureHandlerMessage(String content) {
        Message msg = Message.obtain();
        msg.what = MSG_TYPE_FAILURE;
        msg.arg1 = 11;
        msg.obj = content;
        uiHandler.sendMessage(msg);
    }

    private Handler.Callback sendCallback = new Handler.Callback() {
        @Override
        public boolean handleMessage(Message message) {

            File file = waitImageCreate();
            if (file == null || file.length() < 100) {
                CLog.debug("发送失败,拍摄的照片未生成文件");
                sendFailureHandlerMessage("发送失败,拍摄的照片未生成文件，请重新拍摄发送");
                return true;
            }

            SenderConnectUtil senderConnectUtil = new SenderConnectUtil();
            //发送广播寻找接收的IP地址
            HostAddress hostAddress = senderConnectUtil.connectReceiveServe();
            if (hostAddress == null) {
                CLog.debug("失败，请先搜索主机");
                sendFailureHandlerMessage("发送失败,未找到接收照片的服务器");
                isRunning = false;
                return true;
            }

            final String addressStr = hostAddress.getAddress().toString();
            final int port = hostAddress.getPort();
            CLog.debug("找到主机,IP 地址:" + addressStr + ",TCP端口:" + port);


            SendFileClient sendFileClient = new SendFileClient();
            sendFileClient.readyToSendFile(file, hostAddress, new OnSendFileCallBack() {
                @Override
                public void onSuccess(String message) {
                    Message msg = Message.obtain();
                    msg.what = MSG_TYPE_SUCCESS;
                    msg.obj = message;
                    uiHandler.sendMessage(msg);
                }

                @Override
                public void onFailure(String tips) {
                    Message msg = Message.obtain();
                    msg.what = MSG_TYPE_FAILURE;
                    msg.obj = tips;
                    uiHandler.sendMessage(msg);
                }
            });
            return true;
        }
    };

    /*摇一摇的监听事件*/
    private SensorEventListener sensorEventListener = new SensorEventListener() {
        int mediumValue = 20;

        @Override
        public void onSensorChanged(SensorEvent event) {
            //五秒内部允许第二次传图
            if (System.currentTimeMillis() - lastSendTime < waitTime) {
                return;
            }

            // 一般在这三个方向的重力加速度达到40就达到了摇晃手机的状态。
            if (event.sensor.getType() == Sensor.TYPE_ACCELEROMETER) {
                //这个是每次摇一摇的时候重新读取下之前保存的灵敏度值
                if (isShake(event.values, 15)) {
                    mediumValue = SPSingleton.get().getInt(SPDefine.KEY_ShakeSensor, 20);
                    CLog.debug("摇一摇的灵敏度:" + mediumValue);
                }

                if (isShake(event.values, mediumValue)) {
                    //当过去30秒后强制允许传图
                    if (!isRunning || System.currentTimeMillis() - lastSendTime > 30000) {
                        vibrator.vibrate(1000);
                        ToastUtil.showToastLong("正在发送照片...");
                        uiHandler.sendEmptyMessageDelayed(MSG_TYPE_SEND_IMAGE, 1500);

                    } else {
                        vibrator.cancel();
                        vibrator.vibrate(new long[]{100, 100, 100, 100}, -1);
                        ToastUtil.showToastLong("已经有发送任务了,请等待发送任务完成！");
                    }
                    lastSendTime = System.currentTimeMillis();
                }
            }
        }

        @Override
        public void onAccuracyChanged(Sensor sensor, int accuracy) {
        }
    };


    private boolean isShake(float[] values, int shakeValue) {
        return (Math.abs(values[0]) > shakeValue || Math.abs(values[1]) > shakeValue || Math.abs(2) > shakeValue);
    }

    private Handler uiHandler = new Handler(msg -> {
        if (msg.what == MSG_TYPE_SUCCESS) {
            isRunning = false;
            playSound(true);
            if (msg.obj != null) {
                ToastUtil.showToastShort(msg.obj.toString());
            }
            lastSendTime = System.currentTimeMillis();
        } else if (msg.what == MSG_TYPE_FAILURE) {
            isRunning = false;
            if (msg.obj != null) {
                playSound(false);
                ToastUtil.showToastLong(msg.obj.toString());
            }
        } else if (msg.what == MSG_TYPE_SEND_IMAGE) {
            isRunning = true;
            if (customHandlerThread == null) {
                customHandlerThread = new CustomHandlerThread("SenderThread", sendCallback);
            }
            customHandlerThread.getHandler().sendEmptyMessage(1);
        }
        return false;
    });

    private void playSound(boolean isSuccess) {
        try {
            int id = isSuccess ? R.raw.success : R.raw.failure;
            MediaPlayer mediaPlayer = MediaPlayer.create(this, id);//重新设置要播放的音频
            mediaPlayer.start();//开始播放
        } catch (Exception e) {
            e.printStackTrace();//输出异常信息
        }
    }


    /*等待拍的照片生成*/
    private File waitImageCreate() {
        int count = 10;
        File file = null;
        for (int i = 0; i < count; i++) {
            file = new File(PathUtil.getRecentlyPhotoPath());
            if (file == null || !APPUtil.isImageFile(file.getAbsolutePath())) {
                try {
                    Thread.sleep(1000);
                    CLog.debug("图片未生成，等待一秒重新执行，执行第几次:" + i);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                continue;
            } else {
                CLog.debug("图片完好:" + file.getPath());
                break;
            }
        }
        return file;
    }


    /**
     * 给 API >= 18 的平台上用的灰色保活手段
     */
    public static class GrayInnerService extends Service {

        @Override
        public void onCreate() {
            super.onCreate();
        }

        @Override
        public int onStartCommand(Intent intent, int flags, int startId) {
//            startForeground(GRAY_SERVICE_ID, new Notification());
            startMyNotification(this);

            stopSelf();
            return super.onStartCommand(intent, flags, startId);
        }

        @Override
        public IBinder onBind(Intent intent) {
            throw new UnsupportedOperationException("Not yet implemented");
        }

        @Override
        public void onDestroy() {
            super.onDestroy();
        }
    }


}
