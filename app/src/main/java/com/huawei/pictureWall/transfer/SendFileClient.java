package com.huawei.pictureWall.transfer;


import android.support.annotation.NonNull;
import android.text.TextUtils;

import com.huawei.pictureWall.tools.FileDesc;
import com.huawei.pictureWall.tools.util.CLog;
import com.huawei.pictureWall.tools.util.Configuration;
import com.huawei.pictureWall.tools.util.JsonUtil;
import com.huawei.pictureWall.tools.util.Utils;
import com.huawei.pictureWall.transfer.service.OnSendFileCallBack;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.Socket;


/***
 *@date 创建时间 2018/4/3 01:09
 *@author 作者: yulong
 *@description
 */
public class SendFileClient {

    public void readyToSendFile(File file, HostAddress address, @NonNull OnSendFileCallBack onSendFileCallBack) {
        FileDesc fileDesc = new FileDesc(file);
        String fileInfoStr = Utils.initMessage(JsonUtil.objectToJson(fileDesc));

        //在Tcp连接的建立中, 无需发送机器相关的信息, 只发送文件描述信息;
        String replyMsg = null;

        Socket clientSocket = null;
        BufferedInputStream bufferedInputStream = null;
        BufferedOutputStream bufferedOutputStream = null;
        FileInputStream fileInputStream = null;

        //创建socket并得到其输入输出流用于传输数据
        try {
            clientSocket = new Socket(address.getAddress(), address.getPort());
            bufferedInputStream = new BufferedInputStream(clientSocket.getInputStream());
            bufferedOutputStream = new BufferedOutputStream(clientSocket.getOutputStream());
            fileInputStream = new FileInputStream(file);//打开文件
            //先寻找接收的设备，尝试连接5次
            for (int i = 0; i < Configuration.CONNECT_TIMES; i++) {
                try {
                    //将文件描述发送给服务器, 以便服务器做好准备接收的工作
                    CLog.d("给接收方发送文件描述:" + fileInfoStr);
                    bufferedOutputStream.write(fileInfoStr.getBytes("utf-8"));//>>>>>>>>>>>>>>>>
                    bufferedOutputStream.flush();

                    byte[] buf = new byte[Configuration.BUFFER_LENGTH];
                    bufferedInputStream.read(buf);//<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
                    replyMsg = Utils.getMessage(buf);
                    CLog.d("收到回复信息:" + replyMsg);
                    break;
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }


            //如果接收的消息为空，表示未找到接收的设备
            if (TextUtils.isEmpty(replyMsg)) {
                onSendFileCallBack.onFailure("发送失败，未找到接收的主机，请尝试链接网络后重新发送");
            } else {
                long hasSendBytes = 0;
                int actualRead = 0;

                byte[] fileBuf = new byte[Configuration.BUFFER_LENGTH];
                byte[] responseBytes;// = new byte[Configuration.BUFFER_LENGTH];

                //发送当前文件描述
                bufferedOutputStream.write(fileInfoStr.getBytes());//>>>>>>>>>>>>>>>>>>>>>>>>>
                bufferedOutputStream.flush();

                responseBytes = new byte[Configuration.BUFFER_LENGTH];
                //接收确认
                bufferedInputStream.read(responseBytes);//<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<

                //读取文件并发送文件
                while ((actualRead = fileInputStream.read(fileBuf, 0, Configuration.BUFFER_LENGTH)) > 0) {//读本地文件
                    // send to server
                    bufferedOutputStream.write(fileBuf, 0, actualRead);//写入到网络
                    hasSendBytes += actualRead;
                    if (hasSendBytes >= file.length()) {//传输完毕
                        bufferedOutputStream.flush();
                        break;
                    }
                }

                responseBytes = new byte[Configuration.BUFFER_LENGTH];
                //接收接收文件大小确认
                bufferedInputStream.read(responseBytes);
                String ackSize = Utils.getMessage(responseBytes);
                CLog.debug("ackSize = " + ackSize);

                onSendFileCallBack.onSuccess("发送成功");
            }

        } catch (Exception e) {
            e.printStackTrace();
            onSendFileCallBack.onFailure("发送失败,请稍等几秒后重新发送");
        } finally {
            try {
                if (bufferedInputStream != null) {
                    bufferedInputStream.close();
                }
                if (bufferedOutputStream != null) {
                    bufferedOutputStream.close();
                }
                if (clientSocket != null) {
                    clientSocket.close();
                }
                if (fileInputStream != null) {
                    fileInputStream.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }


    }



}
