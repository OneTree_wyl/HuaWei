package com.huawei.pictureWall.transfer.service;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.os.SystemClock;
import android.support.annotation.Nullable;

import com.huawei.pictureWall.tools.util.CLog;

/***
 * @date 创建时间 2018/4/11 23:16
 * @author 作者: yulong
 * @description
 */
public class NotifyService extends Service {


    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        CLog.w("收到服务通知");
        //定时任务
        AlarmManager manager = (AlarmManager) getSystemService(ALARM_SERVICE);
        //此处是设置每隔1分钟启动一次
        //这是1分钟的毫秒数
        int Minutes = 1 * 60 * 1000;
//        int Minutes = bundle.getInt("minutes");
        //SystemClock.elapsedRealtime()表示1970年1月1日0点至今所经历的时间
        long triggerAtTime = SystemClock.elapsedRealtime() + Minutes;
        //此处设置开启AlarmReceiver这个Service
        Intent i = new Intent(this, WeakAlarmReceiver.class);
        PendingIntent pi = PendingIntent.getBroadcast(this, 0, i, 0);
        //ELAPSED_REALTIME_WAKEUP表示让定时任务的出发时间从系统开机算起，并且会唤醒CPU。
        manager.set(AlarmManager.ELAPSED_REALTIME_WAKEUP, triggerAtTime, pi);
        return super.onStartCommand(intent, flags, startId);
    }
}
