package com.huawei.pictureWall.transfer.service;

/***
 * @date 创建时间 2018/9/6 21:26
 * @author 作者: W.YuLong
 * @description
 */
public interface OnSendFileCallBack {
    void onSuccess(String message);
    void onFailure(String tips);
}
