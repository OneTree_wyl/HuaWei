package com.huawei.pictureWall.transfer;


import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.huawei.pictureWall.tools.util.CLog;
import com.huawei.pictureWall.transfer.service.SenderFileService;

/***
 *@date 创建时间 2018/4/4 22:52
 *@author 作者: yulong
 *@description
 */
public class WakeReceiver extends BroadcastReceiver {

    /**
     * 灰色保活手段唤醒广播的action
     */
    public final static String GRAY_WAKE_ACTION = "com.wake.gray";
    private final static String TAG = WakeReceiver.class.getSimpleName();
    private final static int WAKE_SERVICE_ID = -1111;

    @Override
    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();
        if (GRAY_WAKE_ACTION.equals(action)) {
            CLog.w(TAG + " wake !! wake !! ");

            Intent wakeIntent = new Intent(context, SenderFileService.class);
            wakeIntent.putExtra("from", "WakeReceiver");
            context.startService(wakeIntent);
        }
    }

//    /**
//     * 用于其他进程来唤醒UI进程用的Service
//     */
//    public static class WakeNotifyService extends Service {
//
//        @Override
//        public void onCreate() {
//            CLog.i(TAG, "WakeNotifyService->onCreate");
//            super.onCreate();
//        }
//
//        @Override
//        public int onStartCommand(Intent intent, int flags, int startId) {
//            CLog.i(TAG, "WakeNotifyService->onStartCommand");
//            if (Build.VERSION.SDK_INT < 18) {
//                startForeground(WAKE_SERVICE_ID, new Notification());//API < 18 ，此方法能有效隐藏Notification上的图标
//            } else {
//                Intent innerIntent = new Intent(this, WakeGrayInnerService.class);
//                startService(innerIntent);
//                startForeground(WAKE_SERVICE_ID, new Notification());
//            }
//            return START_STICKY;
//        }
//
//        @Override
//        public IBinder onBind(Intent intent) {
//            // TODO: Return the communication channel to the service.
//            throw new UnsupportedOperationException("Not yet implemented");
//        }
//
//        @Override
//        public void onDestroy() {
//            CLog.i(TAG, "WakeNotifyService->onDestroy");
//            super.onDestroy();
//        }
//    }
//
//    /**
//     * 给 API >= 18 的平台上用的灰色保活手段
//     */
//    public static class WakeGrayInnerService extends Service {
//
//        @Override
//        public void onCreate() {
//            CLog.i(TAG, "InnerService -> onCreate");
//            super.onCreate();
//        }
//
//        @Override
//        public int onStartCommand(Intent intent, int flags, int startId) {
//            CLog.i(TAG, "InnerService -> onStartCommand");
//            startForeground(WAKE_SERVICE_ID, new Notification());
//            //stopForeground(true);
//            stopSelf();
//            return super.onStartCommand(intent, flags, startId);
//        }
//
//        @Override
//        public IBinder onBind(Intent intent) {
//            // TODO: Return the communication channel to the service.
//            throw new UnsupportedOperationException("Not yet implemented");
//        }
//
//        @Override
//        public void onDestroy() {
//            CLog.i(TAG, "InnerService -> onDestroy");
//            super.onDestroy();
//        }
//    }
}
