package com.huawei.pictureWall.transfer.service;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.support.annotation.RequiresApi;


public class WeakAlarmReceiver extends BroadcastReceiver {
    private final String ACTION_DESTROY = "com.hengke.photowall.service.notify";

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent.getAction() == null || intent.getAction().equals(ACTION_DESTROY)) {
            Intent intent1 = new Intent();
            intent1.putExtra("from","WeakAlarmReceiver");
            intent1.setClass(context, SenderFileService.class);
            // 启动service
            // 多次调用startService并不会启动多个service 而是会多次调用onStart
            context.startForegroundService(intent1);
        }
    }
}