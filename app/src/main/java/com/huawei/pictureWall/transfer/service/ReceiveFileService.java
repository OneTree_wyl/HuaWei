package com.huawei.pictureWall.transfer.service;

import android.app.Service;
import android.content.Intent;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.text.TextUtils;

import com.huawei.pictureWall.tools.FileDesc;
import com.huawei.pictureWall.tools.singleton.SPSingleton;
import com.huawei.pictureWall.tools.util.APPUtil;
import com.huawei.pictureWall.tools.util.CLog;
import com.huawei.pictureWall.tools.util.Configuration;
import com.huawei.pictureWall.tools.util.PathUtil;
import com.huawei.pictureWall.tools.util.SPDefine;
import com.huawei.pictureWall.tools.util.ValueConstant;
import com.huawei.pictureWall.transfer.CustomHandlerThread;
import com.huawei.pictureWall.transfer.ReceiverConnectUtil;
import com.huawei.pictureWall.transfer.ReceiverFileServer;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetSocketAddress;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.util.Calendar;

/***
 * @date 创建时间 2018/3/25 16:29
 * @author 作者: yulong
 * @description 接受图片的后台服务监听
 */
public class ReceiveFileService extends Service {

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    private CustomHandlerThread receiveHandlerThread;

    private final int MSG_FAILURE = -1;
    private final int MSG_SUCCESS = 12;

    /*这个相当于是在子线程里操作*/
    private Handler.Callback receiveCallback = new Handler.Callback() {
        @Override
        public boolean handleMessage(Message message) {
            long lastTime = System.currentTimeMillis();
            CLog.debug("执行等待连接任务");
            DatagramPacket senderPacket = waitClientConnect();
            if (senderPacket == null) {
                uiHandler.sendEmptyMessage(MSG_FAILURE);
                return false;//超时得到null
            }

            long successTime = System.currentTimeMillis();
            CLog.d("耗时:" + (successTime - lastTime));

            String senderIP = senderPacket.getAddress().getHostName();
            CLog.debug("发送的设备ID为:" + senderIP);


            //启动Tcp server
            ReceiverFileServer receiverFileServer = new ReceiverFileServer();

            //等待与发送方建立Tcp连接
            FileDesc fileDesc = receiverFileServer.getClientFileDesc();
            if (fileDesc == null) {
                uiHandler.sendEmptyMessage(MSG_FAILURE);
                return false;//等待连接失败或
            }

            String newFileName = ValueConstant.sdf_ymdhms.format(Calendar.getInstance().getTime()) + ".jpg";
            receiverFileServer.receiveFile(fileDesc, newFileName, PathUtil.getReceivePath());
            receiverFileServer.close();

            Message msg = Message.obtain();
            msg.what = MSG_SUCCESS;
//            msg.obj = fileDesc.getName();
            msg.obj = newFileName;
            uiHandler.sendMessage(msg);

            return false;
        }
    };


    public DatagramPacket waitClientConnect() {
        DatagramPacket connectPacket = null;
        DatagramSocket datagramSocket = null;
//        int waitTime = 60 * 1000;
        try {

            String udpPortStr = SPSingleton.get().getString(SPDefine.KEY_UDPPort, "");
            int udpPort = Configuration.UDP_PORT;
            if (!TextUtils.isEmpty(udpPortStr)) {
                udpPort = Integer.parseInt(udpPortStr);
            }

            datagramSocket = new DatagramSocket(null);
            datagramSocket.setReuseAddress(true);
            datagramSocket.bind(new InetSocketAddress(udpPort));
            datagramSocket.setSoTimeout(ReceiverConnectUtil.waitTime);


            byte[] receiveByte = new byte[Configuration.BUFFER_LENGTH];
            DatagramPacket receivePacket = new DatagramPacket(receiveByte, receiveByte.length);
            datagramSocket.receive(receivePacket);//接收client的广播信息
            String msgStr = APPUtil.byte2String(receivePacket.getData());
            CLog.debug("接收的报文:" + msgStr);

            String portStr = SPSingleton.get().getString(SPDefine.KEY_TCP_Port, "");
            int port = Configuration.currentTcpPort;
            if (!TextUtils.isEmpty(portStr)) {
                port = Integer.parseInt(portStr);
            }

            //将服务器的主机名发送给client
            receivePacket.setData((port + Configuration.DELIMITER).getBytes("utf-8"));
            datagramSocket.send(receivePacket);//回复信息tcp要使用的Tcp端口给client

            receivePacket.setData(msgStr.getBytes("utf-8"));

            connectPacket = receivePacket;

        } catch (SocketTimeoutException e) {
            CLog.e("udp server 等待超时");
        } catch (UnsupportedEncodingException e) {
            CLog.e("编码解释错误" + e.getMessage());
        } catch (SocketException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (datagramSocket != null) {
                datagramSocket.close();
            }
        }
        return connectPacket;//将client发送的数据包返回给调用者, 里面包含client的地址, 端口, 主机名
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        boolean isStop = false;
        if (intent != null) {
            isStop = intent.getBooleanExtra(ValueConstant.EXTRA_IS_STOP_SERVICE, false);
        }
        if (isStop) {
            stopSelf();
        } else {

            startReceiveTask(1);
        }
        return super.onStartCommand(intent, flags, startId);
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR2)
    @Override
    public void onDestroy() {
        super.onDestroy();
        if (receiveHandlerThread != null) {
            receiveHandlerThread.quitSafely();
            receiveHandlerThread = null;
        }
    }

    private Handler uiHandler = new Handler(new Handler.Callback() {
        @Override
        public boolean handleMessage(Message msg) {
            if (msg.what == MSG_SUCCESS) {
                String name = (String) msg.obj;

                Intent intent = new Intent();
                intent.setAction(ValueConstant.RECEIVE_PHOTO);
                intent.putExtra(ValueConstant.EXTRA_FILE_NAME, PathUtil.getReceivePath() + name);
                sendBroadcast(intent);
            }




            /*这个是接收照片的后台线程*/
            if (receiveHandlerThread == null) {
                receiveHandlerThread = new CustomHandlerThread("", receiveCallback);
                CLog.d("创建接收的线程");
            }
            receiveHandlerThread.getHandler().removeMessages(1);
            receiveHandlerThread.getHandler().sendEmptyMessage(1);

            startReceiveTask(ReceiverConnectUtil.waitTime);
            return false;
        }
    });

    private void startReceiveTask(long delayTime) {
        uiHandler.removeMessages(1);
        uiHandler.sendEmptyMessageDelayed(1, delayTime);
    }


}
