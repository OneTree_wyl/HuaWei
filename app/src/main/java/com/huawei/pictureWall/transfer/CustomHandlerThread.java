package com.huawei.pictureWall.transfer;

import android.os.Handler;
import android.os.HandlerThread;

/***
 * @date 创建时间 2018/10/3 20:14
 * @author 作者: W.YuLong
 * @description 自定义的HandlerThread 对Handler做了进一步的封装
 */
public class CustomHandlerThread extends HandlerThread {
    private Handler handler;
    public CustomHandlerThread(String name, Handler.Callback callback) {
        super(name);
        start();
        handler = new Handler(getLooper(), callback);

    }


    public Handler getHandler() {
        return handler;
    }


}
