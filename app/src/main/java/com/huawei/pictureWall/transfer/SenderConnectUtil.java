package com.huawei.pictureWall.transfer;

import android.text.TextUtils;

import com.huawei.pictureWall.tools.singleton.SPSingleton;
import com.huawei.pictureWall.tools.util.CLog;
import com.huawei.pictureWall.tools.util.Configuration;
import com.huawei.pictureWall.tools.util.SPDefine;
import com.huawei.pictureWall.tools.util.Utils;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.InterfaceAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.util.Enumeration;

/***
 * @date 创建时间 2018/8/21 21:31
 * @author 作者: W.YuLong
 * @description 发送端搜索接收端的工具类
 */
public class SenderConnectUtil {
    private static int times = 5;

    public HostAddress connectReceiveServe() {
        InetAddress inetAddress = null;

        byte[] receiveBytes = new byte[Configuration.BUFFER_LENGTH];
        byte[] sendBytes = new byte[Configuration.BUFFER_LENGTH];

        String saveIP = SPSingleton.get().getString(SPDefine.KEY_IPAddress, "");
        if (TextUtils.isEmpty(saveIP)) {
            inetAddress = getBroadcastAddress(getLocalHostLanIP());//设置广播地址
        } else {
            try {
                inetAddress = InetAddress.getByName(saveIP);
            } catch (UnknownHostException e) {
                e.printStackTrace();
            }
        }

        if (inetAddress == null){
            return null;
        }

        String portStr = SPSingleton.get().getString(SPDefine.KEY_UDP_Port, "");
        int port = Configuration.UDP_PORT;
        if (!TextUtils.isEmpty(portStr)) {
            port = Integer.parseInt(portStr);
        }

        DatagramSocket clientSocket = null;
        try {
            clientSocket = new DatagramSocket();//创建一个udpClient
            clientSocket.setBroadcast(true);//广播信息
            clientSocket.setSoTimeout(3 * 1000);//如果3秒后没后得到服务器的回应, 抛出超时异常, 以便重新广播
            CLog.e("本机ip:" + getLocalHostLanIP() + " 广播地址:" + inetAddress);

            String msg = "发送的IP地址，" + inetAddress.toString() + Configuration.DELIMITER;
            sendBytes = msg.getBytes("utf-8");
            CLog.debug("发送的IP地址:" + inetAddress.toString());

        } catch (SocketException e3) {
            e3.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        DatagramPacket sendPacket = new DatagramPacket(sendBytes, sendBytes.length, inetAddress, port);
        DatagramPacket receivePacket = new DatagramPacket(receiveBytes, receiveBytes.length);

        int tryTimes = 1;
        while (tryTimes <= times) {//多次尝试

            try {
                clientSocket.send(sendPacket);//向服务器发送数据包
                clientSocket.receive(receivePacket);//如果没有收到数据包, 那么尝试多次
                if (receivePacket != null && new String(receivePacket.getData()).length() > 0) {
                    break;
                }
            } catch (SocketTimeoutException e) {
                e.printStackTrace();
                CLog.e("超时: " + tryTimes + "次" + "共:" + times + "次");
                tryTimes++;
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        if (tryTimes <= times) {
            String strPort = Utils.getMessage(receivePacket.getData());
            CLog.d("接收的数据:" + strPort);
            if (!TextUtils.isEmpty(strPort)) {
                int serverPort = Integer.parseInt(strPort);

                CLog.d("IP和端口："+new HostAddress(receivePacket.getAddress(), serverPort).toString());

                return new HostAddress(receivePacket.getAddress(), serverPort);
            }
        }
        return null;
    }


    public static InetAddress getBroadcastAddress(InetAddress address) {
        //获取本机在局域网中的广播地址
        if (address == null) {
            return null;
        }
        InetAddress broadcastAddr = null;
        NetworkInterface networkInterface;
        try {
            networkInterface = NetworkInterface.getByInetAddress(address);
            for (InterfaceAddress taddr : networkInterface.getInterfaceAddresses()) {
                //获取指定ip的广播地址
                if (taddr.getAddress().getHostAddress().equals(address.getHostAddress())) {
                    broadcastAddr = taddr.getBroadcast();
//                    CLog.debug("broadcastAddr=" + broadcastAddr.toString());
                } else {
//                    CLog.debug("taddr.getAddress().getHostAddress() = " + taddr.getAddress().getHostAddress());
                }
            }
        } catch (SocketException e) {
            e.printStackTrace();
        }
        return broadcastAddr;
    }


    public InetAddress getLocalHostLanIP() {
        //获取本机在局域网中的IP
        Enumeration<?> allNetInterfaces;
        InetAddress IP = null;
        try {
            allNetInterfaces = NetworkInterface.getNetworkInterfaces();
            while (allNetInterfaces.hasMoreElements()) {
                NetworkInterface netInterface = (NetworkInterface) allNetInterfaces.nextElement();
                //System.out.println(netInterface.getName());
                Enumeration<?> addresses = netInterface.getInetAddresses();
                while (addresses.hasMoreElements()) {
                    InetAddress tempIP = (InetAddress) addresses.nextElement();
                    if (tempIP != null && tempIP instanceof Inet4Address && !tempIP.getHostAddress().equals("127.0.0.1")) {
                        //System.out.println("本机的IP=" + tempIP.getHostAddress());
                        IP = tempIP;
                        CLog.debug("本机的IP=" + tempIP.getHostAddress());
                    }
                }
            }
        } catch (SocketException e) {
            e.printStackTrace();
        }
        return IP;
    }
}
