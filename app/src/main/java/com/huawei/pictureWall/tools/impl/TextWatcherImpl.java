package com.huawei.pictureWall.tools.impl;

import android.text.Editable;
import android.text.TextWatcher;

/***
 * @date 创建时间 2018/8/23 23:36
 * @author 作者: W.YuLong
 * @description
 */
public class TextWatcherImpl implements TextWatcher {
    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override
    public void afterTextChanged(Editable s) {

    }
}
