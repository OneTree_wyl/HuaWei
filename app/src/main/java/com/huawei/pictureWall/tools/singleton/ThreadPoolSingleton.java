package com.huawei.pictureWall.tools.singleton;

import java.util.concurrent.Callable;
import java.util.concurrent.Future;
import java.util.concurrent.SynchronousQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/***
 *@date 创建时间 2018/4/18 15:15
 *@author 作者: yulong
 *@description  线程池的单例模式
 */
public class ThreadPoolSingleton {

//    private final int availableProcessor = Runtime.getRuntime().availableProcessors();
    private ThreadPoolExecutor executorService;


    private ThreadPoolSingleton() {
        if (executorService == null) {
            int maxProcessor = 500;// (availableProcessor * 2 + 1) * 2 ;

            executorService = new ThreadPoolExecutor(0, maxProcessor,
                    60L, TimeUnit.SECONDS,
                    new SynchronousQueue<>());

        }
    }

    public static ThreadPoolSingleton get() {
        return SingleTonHelper.instance;
    }

    public void executeTask(Runnable runnable) {
        executorService.execute(runnable);
    }

    public <T> Future<T> executeCallableTask(Callable<T> callable) {
        return executorService.submit(callable);
    }

    private static class SingleTonHelper {
        private static ThreadPoolSingleton instance = new ThreadPoolSingleton();
    }
}
