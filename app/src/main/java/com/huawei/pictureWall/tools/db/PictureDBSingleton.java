package com.huawei.pictureWall.tools.db;

import android.content.ContentValues;
import android.database.Cursor;

import com.huawei.pictureWall.base.APP;
import com.huawei.pictureWall.tools.db.entity.PictureInfoEntity;
import com.huawei.pictureWall.tools.util.PathUtil;

import java.util.ArrayList;
import java.util.List;

/***
 * @date 创建时间 2018/9/18 22:19
 * @author 作者: W.YuLong
 * @description
 */
public class PictureDBSingleton {
    DatabaseHelper dbHelper;

    private final static String TABLE_NAME = "PICTURE_INFO_TABLE";

    public final static String KEY_ID = "ID";
    public final static String KEY_NAME = "NAME";
    public final static String KEY_URL = "URL";
    public final static String KEY_DATE_TIME = "DATE_TIME";
    public final static String KEY_DIR_PATH = "DIR_PATH";


    public final static String KEY_DATA1 = "DATA_1";
    public final static String KEY_DATA2 = "DATA_2";


    private static PictureDBSingleton instance;

    public static PictureDBSingleton get() {
        if (instance == null) {
            synchronized (PictureDBSingleton.class) {
                if (instance == null) {
                    instance = new PictureDBSingleton();
                }
            }
        }
        return instance;
    }

    private PictureDBSingleton() {
        this.dbHelper = new DatabaseHelper(APP.get(), PathUtil.getRootPath() + "pictureInfo.db");
        createTable();
    }



    private void createTable() {
        String sqlStr = "CREATE TABLE IF NOT EXISTS " + TABLE_NAME + " ( "
                + KEY_ID + " INTEGER PRIMARY KEY, " + KEY_NAME + " VARCHAR, "
                + KEY_DIR_PATH + " VARCHAR, " + KEY_URL + " VARCHAR, " + KEY_DATE_TIME + " LONG, "
                + KEY_DATA1 + " VARCHAR, " + KEY_DATA2
                + " VARCHAR);";
        try {
            dbHelper.getWritableDatabase().execSQL(sqlStr);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            dbHelper.close();
        }
    }

    /*插入或更新数据*/
    public void insertOrUpdateData(PictureInfoEntity entity) {
        ContentValues cv = putPlanDataToValue(entity);
        try {
            if (isResourceDataExist(entity)) {
                dbHelper.getWritableDatabase().update(TABLE_NAME, cv,
                        String.format("%s='%s'", KEY_NAME, entity.getFileName()), null);
            } else {
                dbHelper.getWritableDatabase().insert(TABLE_NAME, null, cv);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            dbHelper.close();
        }
    }


    private boolean isResourceDataExist(PictureInfoEntity entity) {
        String sql = String.format("select count() from %s where %s = '%s'",
                TABLE_NAME, KEY_NAME, entity.getFileName());
        boolean flag = false;
        try {
            Cursor cursor = dbHelper.getReadableDatabase().rawQuery(sql, null);
            if (cursor != null) {
                if (cursor.moveToFirst()){
                    if (cursor.getInt(0) > 0) {
                        flag = true;
                    }
                }
                cursor.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return flag;
    }


    /*根据文件名查询是否数据库有数据*/
    public PictureInfoEntity queryFileUrl(String fileName){
        String sql = String.format("select * from %s where %s = '%s'",
                TABLE_NAME, KEY_NAME, fileName);
        PictureInfoEntity entity = null;
        try {
            Cursor cursor = dbHelper.getReadableDatabase().rawQuery(sql, null);
            if (cursor != null) {
                if(cursor.moveToFirst()){
                    entity = readDataToEntity(cursor);
                }
                cursor.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            dbHelper.close();
        }
        return entity;
    }


    public void deleteOneData(String fileName){
        ArrayList<String> list = new ArrayList<>();
        list.add(fileName);
        deleteData(list);
    }

    public void deleteData(List<String> fileNames){
        for (String name: fileNames){
            String sql = String.format("DELETE FROM %s WHERE %s = '%s'", TABLE_NAME, KEY_NAME, name);
            dbHelper.getWritableDatabase().rawQuery(sql, null);
        }
    }

    private PictureInfoEntity readDataToEntity(Cursor cursor){
        PictureInfoEntity entity = new PictureInfoEntity();
        entity.setId(cursor.getInt(cursor.getColumnIndex(KEY_ID)));
        entity.setFileName(cursor.getString(cursor.getColumnIndex(KEY_NAME)));
        entity.setDirPath(cursor.getString(cursor.getColumnIndex(KEY_DIR_PATH)));
        entity.setUrl(cursor.getString(cursor.getColumnIndex(KEY_URL)));
        entity.setDateTime(cursor.getLong(cursor.getColumnIndex(KEY_DATE_TIME)));
        return entity;
    }

    private ContentValues putPlanDataToValue(PictureInfoEntity entity) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(KEY_NAME, entity.getFileName());
        contentValues.put(KEY_URL, entity.getUrl());
        contentValues.put(KEY_DIR_PATH, entity.getDirPath());
        contentValues.put(KEY_DATE_TIME, entity.getDateTime());
        return contentValues;
    }



}
