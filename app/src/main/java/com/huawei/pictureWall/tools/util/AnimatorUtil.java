package com.huawei.pictureWall.tools.util;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.view.View;

/***
 * @date 创建时间 2018/10/11 21:24
 * @author 作者: W.YuLong
 * @description
 */
public class AnimatorUtil {

    public static void alphaAnimator(View v, Animator.AnimatorListener animatorListener){
//        ObjectAnimator alphaOutAnimator = ObjectAnimator.ofFloat(v, "alpha", 1f, 0.7f);
//        if (animatorListener != null){
//            alphaOutAnimator.addListener(animatorListener);
//        }
//        alphaOutAnimator.setRepeatCount(-1);
//        alphaOutAnimator.setRepeatMode(ValueAnimator.REVERSE);

        ObjectAnimator scaleXAnimator = ObjectAnimator.ofFloat(v, "scaleX", 1, 1.1f);
        ObjectAnimator scaleYAnimator = ObjectAnimator.ofFloat(v, "scaleY", 1, 1.1f);

        scaleXAnimator.setRepeatCount(-1);
        scaleXAnimator.setRepeatMode(ValueAnimator.REVERSE);

        scaleYAnimator.setRepeatCount(-1);
        scaleYAnimator.setRepeatMode(ValueAnimator.REVERSE);

        AnimatorSet animatorSet = new AnimatorSet();//组合动画
        animatorSet.setDuration(1000);
        animatorSet.play(scaleXAnimator).with(scaleYAnimator);
        animatorSet.start();

    }


}
