package com.huawei.pictureWall.tools.db.entity;

/***
 * @date 创建时间 2018/9/18 22:59
 * @author 作者: W.YuLong
 * @description
 */
public class PictureInfoEntity {

    private int id;
    private String fileName;
    private String url;
    private String dirPath;

    private long dateTime;


    public int getId() {
        return id;
    }

    public long getDateTime() {
        return dateTime;
    }

    public PictureInfoEntity setDateTime(long dateTime) {
        this.dateTime = dateTime;
        return this;
    }

    public PictureInfoEntity setId(int id) {
        this.id = id;
        return this;
    }

    public String getFileName() {
        return fileName;
    }

    public PictureInfoEntity setFileName(String fileName) {
        this.fileName = fileName;
        return this;
    }

    public String getUrl() {
        return url;
    }

    public PictureInfoEntity setUrl(String url) {
        this.url = url;
        return this;
    }

    public String getDirPath() {
        return dirPath;
    }

    public PictureInfoEntity setDirPath(String dirPath) {
        this.dirPath = dirPath;
        return this;
    }
}
