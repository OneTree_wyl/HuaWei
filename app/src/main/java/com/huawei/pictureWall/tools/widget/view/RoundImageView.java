package com.huawei.pictureWall.tools.widget.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ColorFilter;
import android.graphics.LinearGradient;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.PixelFormat;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Shader;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.support.annotation.IntDef;
import android.support.v4.view.ViewCompat;
import android.util.AttributeSet;
import android.view.View;

import com.huawei.pictureWall.R;

/***
 * @date 创建时间 2018/9/29 18:10
 * @author 作者: W.YuLong
 * @description 圆角，圆形，带有阴影的ImageView控件
 */
public class RoundImageView extends android.support.v7.widget.AppCompatImageView {
    public static final int TYPE_NONE = 0;
    /* 圆形 */
    public static final int TYPE_CIRCLE = 1;
    /* 圆角矩形 */
    public static final int TYPE_ROUNDED_RECT = 2;

    private static final int DEFAULT_TYPE = TYPE_NONE;
    private static final int DEFAULT_BORDER_COLOR = Color.TRANSPARENT;
    private static final int DEFAULT_BORDER_WIDTH = 0;
    private static final int DEFAULT_RECT_ROUND_RADIUS = 0;

    private int roundType;
    private int borderColor;
    private int borderWidth;
    private int roundSize;
    private int shadowSize;

    private int shadowColor;
    private int bgColor;

    private Paint bitmapPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
    private Paint borderPaint = new Paint(Paint.ANTI_ALIAS_FLAG);

    private RectF borderRect = new RectF();
    private RectF imageRect = new RectF();

    private Bitmap mRawBitmap;
    private BitmapShader bitmapShader;
    private Matrix matrix = new Matrix();
    private ShadowBGDrawable shadowDrawable;

    public RoundImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.RoundImageView);
        roundType = a.getInt(R.styleable.RoundImageView_app_round_type, DEFAULT_TYPE);
        borderColor = a.getColor(R.styleable.RoundImageView_app_border_color, DEFAULT_BORDER_COLOR);
        borderWidth = a.getDimensionPixelSize(R.styleable.RoundImageView_app_border_size, dp2px(DEFAULT_BORDER_WIDTH));
        roundSize = a.getDimensionPixelSize(R.styleable.RoundImageView_app_round_size, dp2px(DEFAULT_RECT_ROUND_RADIUS));
        shadowSize = a.getDimensionPixelSize(R.styleable.RoundImageView_app_shadow_size, dp2px(DEFAULT_RECT_ROUND_RADIUS));

        shadowColor = a.getColor(R.styleable.RoundImageView_app_shadow_color, Color.TRANSPARENT);
        bgColor = a.getColor(R.styleable.RoundImageView_app_bg_color, Color.TRANSPARENT);
        a.recycle();

        setPadding(shadowSize, shadowSize, shadowSize, shadowSize);

        ShadowBGDrawable shadowDrawable = ShadowBGDrawable.with().setBgColors(new int[]{bgColor})
                .setShadowColor(shadowColor).setShadowRadius(shadowSize)
                .setShapeRadius(roundSize)
                .setShapeType(roundType).create();
        setShadowBg(shadowDrawable);
    }


    public void setShadowBg(ShadowBGDrawable shadowBg) {
        this.shadowDrawable = shadowBg;
        roundType = shadowBg.getBuilder().shapeType;
        setLayerType(View.LAYER_TYPE_SOFTWARE, null);
        ViewCompat.setBackground(RoundImageView.this, shadowDrawable);
        invalidate();
    }

    public ShadowBGDrawable getShadowDrawable() {
        return shadowDrawable;
    }

    @Override
    protected void onDraw(Canvas canvas) {

        Bitmap rawBitmap = getBitmap(getDrawable());

        if (rawBitmap != null && roundType != TYPE_NONE) {
            int viewWidth = getWidth() - getPaddingLeft() - getPaddingRight();
            int viewHeight = getHeight() - getPaddingTop() - getPaddingBottom();
            int viewMinSize = Math.min(viewWidth, viewHeight);
            float dstWidth = roundType == TYPE_CIRCLE ? viewMinSize : viewWidth;
            float dstHeight = roundType == TYPE_CIRCLE ? viewMinSize : viewHeight;
            float halfBorderWidth = borderWidth / 2.0f;
            float doubleBorderWidth = borderWidth * 2;

            if (bitmapShader == null || !rawBitmap.equals(mRawBitmap)) {
                mRawBitmap = rawBitmap;
                bitmapShader = new BitmapShader(mRawBitmap, Shader.TileMode.CLAMP, Shader.TileMode.CLAMP);
            }
            if (bitmapShader != null) {
                matrix.setScale((dstWidth - doubleBorderWidth) / rawBitmap.getWidth(), (dstHeight - doubleBorderWidth) / rawBitmap.getHeight());
                bitmapShader.setLocalMatrix(matrix);
            }

            bitmapPaint.setShader(bitmapShader);

            borderPaint.setStyle(Paint.Style.STROKE);
            borderPaint.setStrokeWidth(borderWidth);
            borderPaint.setColor(borderWidth > 0 ? borderColor : Color.TRANSPARENT);

            if (roundType == TYPE_CIRCLE) {
                float radius = viewMinSize / 2.0f;
                canvas.save();
                canvas.translate(getPaddingLeft(), getPaddingTop());
                canvas.drawCircle(radius, radius, radius - halfBorderWidth, borderPaint);
                canvas.translate(borderWidth, borderWidth);
                canvas.drawCircle(radius - borderWidth, radius - borderWidth, radius - borderWidth, bitmapPaint);
                canvas.restore();
            } else if (roundType == TYPE_ROUNDED_RECT) {
                float left = 0; //getPaddingLeft();
                float right = dstWidth - doubleBorderWidth;
                float top = 0; //getPaddingTop();
                float bottom = dstHeight - doubleBorderWidth;


                borderRect.set(halfBorderWidth, halfBorderWidth, dstWidth - halfBorderWidth, dstHeight - halfBorderWidth);
                imageRect.set(left, top, right, bottom);
                float borderRadius = roundSize - halfBorderWidth > 0.0f ? roundSize - halfBorderWidth : 0.0f;
                float bitmapRadius = roundSize - borderWidth > 0.0f ? roundSize - borderWidth : 0.0f;

                canvas.save();
                canvas.translate(getPaddingLeft(), getPaddingTop());
                canvas.drawRoundRect(borderRect, borderRadius, borderRadius, borderPaint);
                canvas.translate(borderWidth, borderWidth);
                canvas.drawRoundRect(imageRect, bitmapRadius, bitmapRadius, bitmapPaint);
                canvas.restore();
            }
        } else {
            super.onDraw(canvas);
        }
    }

    private int dp2px(int dipVal) {
        float scale = getResources().getDisplayMetrics().density;
        return (int) (dipVal * scale + 0.5f);
    }

    private Bitmap getBitmap(Drawable drawable) {
        if (drawable instanceof BitmapDrawable) {
            return ((BitmapDrawable) drawable).getBitmap();
        } else if (drawable instanceof ColorDrawable) {
            Rect rect = drawable.getBounds();
            int width = rect.right - rect.left;
            int height = rect.bottom - rect.top;
            int color = ((ColorDrawable) drawable).getColor();
            Bitmap bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
            Canvas canvas = new Canvas(bitmap);
            canvas.drawARGB(Color.alpha(color), Color.red(color), Color.green(color), Color.blue(color));
            return bitmap;
        } else {
            return null;
        }
    }


    /***
     *@date 创建时间 2018/9/30 13:47
     *@author 作者: W.YuLong
     *@description 阴影背景
     */
    public static class ShadowBGDrawable extends Drawable {

        private Paint shadowPaint;
        private Paint bgPaint;
        private RectF rectF;
        private Builder builder;

        private ShadowBGDrawable(Builder builder) {
            this.builder = builder;

            shadowPaint = new Paint();
            shadowPaint.setColor(Color.TRANSPARENT);
            shadowPaint.setAntiAlias(true);
            shadowPaint.setShadowLayer(builder.shadowRadius, 0, 0, builder.shadowColor);
            shadowPaint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.DST_ATOP));

            bgPaint = new Paint();
            bgPaint.setAntiAlias(true);
        }

        public static ShadowBGDrawable.Builder with() {
            return new Builder();
        }

        public Builder getBuilder() {
            return builder;
        }

        @IntDef({TYPE_NONE, TYPE_CIRCLE, TYPE_ROUNDED_RECT})
        public @interface ShapeType {
        }


        public ShadowBGDrawable setShadowColor(int shadowColor) {
            builder.shadowColor = shadowColor;
            return this;
        }

        public ShadowBGDrawable setShadowRadius(int shadowRadius) {
            builder.shadowRadius = shadowRadius;
            return this;
        }


        public ShadowBGDrawable setShapeType(@ShapeType int shapeType) {
            builder.shapeType = shapeType;
            return this;
        }


        public ShadowBGDrawable setShapeRadius(int shapeRadius) {
            builder.shapeRadius = shapeRadius;
            return this;
        }

        public ShadowBGDrawable setBgColors(int[] bgColors) {
            builder.bgColors = bgColors;
            return this;
        }


        @Override
        public void setBounds(int left, int top, int right, int bottom) {
            super.setBounds(left, top, right, bottom);
            rectF = new RectF(left + builder.shadowRadius, top + builder.shadowRadius, right - builder.shadowRadius,
                    bottom - builder.shadowRadius);
        }

        @Override
        public void draw(Canvas canvas) {
            if (builder.bgColors != null) {
                if (builder.bgColors.length == 1) {
                    bgPaint.setColor(builder.bgColors[0]);
                } else {
                    bgPaint.setShader(new LinearGradient(rectF.left, rectF.height() / 2, rectF.right,
                            rectF.height() / 2, builder.bgColors, null, Shader.TileMode.CLAMP));
                }
            }

            if (builder.shapeType == TYPE_ROUNDED_RECT) {
                canvas.drawRoundRect(rectF, builder.shapeRadius, builder.shapeRadius, shadowPaint);
                canvas.drawRoundRect(rectF, builder.shapeRadius, builder.shapeRadius, bgPaint);
            } else if (builder.shapeType == TYPE_CIRCLE) {
                canvas.drawCircle(rectF.centerX(), rectF.centerY(), Math.min(rectF.width(), rectF.height()) / 2, shadowPaint);

                canvas.drawCircle(rectF.centerX(), rectF.centerY(), Math.min(rectF.width(), rectF.height()) / 2, bgPaint);
            }
        }

        @Override
        public void setAlpha(int alpha) {
            shadowPaint.setAlpha(alpha);
        }

        @Override
        public void setColorFilter(ColorFilter colorFilter) {
            shadowPaint.setColorFilter(colorFilter);
        }

        @Override
        public int getOpacity() {
            return PixelFormat.TRANSLUCENT;
        }


        /***
         *@date 创建时间 2018/9/30 14:50
         *@author 作者: W.YuLong
         *@description
         */
        public static class Builder {
            private int shadowRadius;

            @ShapeType
            private int shapeType;

            private int shapeRadius;
            private int bgColors[];
            private int shadowColor;

            public ShadowBGDrawable create() {
                return new ShadowBGDrawable(this);
            }


            public Builder setShadowColor(int shadowColor) {
                this.shadowColor = shadowColor;
                return this;
            }

            public Builder setShadowRadius(int shadowRadius) {
                this.shadowRadius = shadowRadius;
                return this;
            }


            public Builder setShapeType(@ShapeType int shapeType) {
                this.shapeType = shapeType;
                return this;
            }


            public Builder setShapeRadius(int shapeRadius) {
                this.shapeRadius = shapeRadius;
                return this;
            }

            public Builder setBgColors(int[] bgColors) {
                this.bgColors = bgColors;
                return this;
            }
        }
    }
}