package com.huawei.pictureWall.tools.util;

/***
 * @date 创建时间 2018/9/27 23:43
 * @author 作者: W.YuLong
 * @description
 */
public class DimenSizeUtil {
    private static int showAllImageHeight = -1;
    public static int getShowAllImageHeight(){
        if (showAllImageHeight == -1){
            showAllImageHeight = (int) (Math.min(APPUtil.getScreenHeight(), APPUtil.getScreenWidth()) * 0.1);
        }
        return showAllImageHeight;
    }



}
