package com.huawei.pictureWall.tools.util;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Random;

/***
 * @date 创建时间 2018/8/16 23:47
 * @author 作者: W.YuLong
 * @description
 */
public class PictureLoadUtil {
    private static Random random = new Random();

    public static ArrayList<File> initFiles(int maxSize, String path) {
        File demoDir = new File(path);
        File[] demoFileArray = demoDir.listFiles(new PhotoFileFilter());

        List<File> demoFileList = Arrays.asList(demoFileArray);
        Collections.sort(demoFileList, new FileComparator(false));

        Calendar calendar = Calendar.getInstance();
//        int day = 5;
        int loadedPhotoCount = 0;
        List<File> photoList = new ArrayList<>();
        File photoDir = new File(PathUtil.getReceivePath());
        File[] photoArray = photoDir.listFiles(new PhotoFileFilter());
        if (photoArray != null) {
            List<File> fileList = Arrays.asList(photoArray);
            Collections.sort(fileList, new FileComparator(false));
            for (File f : fileList) {
                if (loadedPhotoCount < maxSize) {
                    if (APPUtility.isImageFile(f.getAbsolutePath())) {
                        photoList.add(f);
                        loadedPhotoCount++;
                    } else {
                        f.delete();
                    }
                } else {
                    break;
                }
            }
        }

        Collections.sort(photoList, new FileComparator(false));

        int photoSize = photoList.size();

        ArrayList<File> allFileList = new ArrayList<>();

        int index = 0;
        int demoSize = demoFileList.size();
        int length = demoSize < maxSize ? maxSize : demoSize;
        int demoIndex = 0;
        for (int i = 0; i < length; i++) {

            if (demoSize != 0) {
                allFileList.add(demoFileList.get(i % demoSize));
            }
            if (photoSize > 0) {
                for (int j = 0, len = random.nextInt(2) + 1; j < len; j++) {
                    if (index < photoSize - 1) {
                        allFileList.add(photoList.get(index));
                    }
                    index++;
                }
            }

        }
        return allFileList;
    }

    /*删除过期的照片*/
    public static void deleteExpiredImage(long expireTime) {
        File dir = new File(PathUtil.getReceivePath());
        if (dir != null && dir.exists() && dir.isDirectory()) {
            File[] fileArray = dir.listFiles();

            if (fileArray != null) {
                List<File> fileList = Arrays.asList(fileArray);
                Collections.sort(fileList, new Comparator<File>() {
                    @Override
                    public int compare(File f1, File f2) {
                        if (f1.lastModified() - f2.lastModified() > 0) {
                            return 1;
                        } else {
                            return -1;
                        }
                    }
                });

                long currentTime = System.currentTimeMillis();
                CLog.d(String.format("文件数: %d,转换列表: %d", fileArray.length, fileList.size()));
                int i = 1;
                for (File f : fileArray) {
                    CLog.d(String.format("%d、f.lastModified() = %d", i, f.lastModified()));
                    i++;
                    if (currentTime - f.lastModified() > expireTime) {
                        f.delete();
                        CLog.d("做删除操作");
                    }
                }
            }
        }
    }


}
