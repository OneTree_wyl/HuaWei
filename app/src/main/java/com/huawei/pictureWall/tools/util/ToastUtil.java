package com.huawei.pictureWall.tools.util;

import android.support.annotation.StringRes;
import android.text.TextUtils;
import android.view.Gravity;
import android.widget.Toast;

import com.huawei.pictureWall.base.APP;


/***
 * @date 创建时间 2018/5/22 15:34
 * @author 作者: W.YuLong
 * @description Toast提示的基类
 */
public class ToastUtil {
    private static Toast sToast;

    private static Toast getsToast() {
        if (sToast == null) {
            sToast = Toast.makeText(APP.get(), "", Toast.LENGTH_SHORT);
            sToast.setGravity(Gravity.CENTER, 0, 0);
        }
        return sToast;
    }

    /**/
    public static void showToastShort(@StringRes int textId) {
        getsToast().setDuration(Toast.LENGTH_SHORT);
        getsToast().setText(textId);
        getsToast().show();
    }

    public static void showToastShort(CharSequence text) {
        if (TextUtils.isEmpty(text)) {
            return;
        }
        getsToast().setDuration(Toast.LENGTH_SHORT);
        getsToast().setText(text);
        getsToast().show();
    }

    /**/
    public static void showToastLong(CharSequence text) {
        if (TextUtils.isEmpty(text)) {
            return;
        }
        getsToast().setDuration(Toast.LENGTH_LONG);
        getsToast().setText(text);
        getsToast().show();
    }

    public static void showToastLong(@StringRes int textId) {
        getsToast().setDuration(Toast.LENGTH_LONG);
        getsToast().setText(textId);
        getsToast().show();
    }

    public static void showApiError(String api, String msg, String requestApi) {
        if (APPUtil.isApkInDebug()) {
            if (api.equals(requestApi)) {
                showToastShort(msg);
            }
        } else {
            showToastShort(msg);
        }
    }


}
