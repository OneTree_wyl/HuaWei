package com.huawei.pictureWall.tools.impl;

import android.widget.SeekBar;

/***
 * @date 创建时间 2018/9/22 22:27
 * @author 作者: W.YuLong
 * @description
 */
public class OnSeekBarChangeListenerImpl implements SeekBar.OnSeekBarChangeListener {
    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {

    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {

    }
}
