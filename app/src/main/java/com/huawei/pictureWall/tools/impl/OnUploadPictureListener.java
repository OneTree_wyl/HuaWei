package com.huawei.pictureWall.tools.impl;

import java.io.File;

/***
 * @date 创建时间 2018/9/25 22:50
 * @author 作者: W.YuLong
 * @description 图片上传的回调接口
 */
public interface OnUploadPictureListener {
    void doUploadPicture(File file);
}
