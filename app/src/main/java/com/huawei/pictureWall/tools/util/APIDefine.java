package com.huawei.pictureWall.tools.util;

/***
 * @date 创建时间 2018/10/8 18:04
 * @author 作者: W.YuLong
 * @description
 */
public class APIDefine {


    public static final String API_resources_signature = "/upload/prepare-upload";

    public static final String API_get_upload_config = "/upload/prepare-upload-copy";
}
