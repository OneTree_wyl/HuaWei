package com.huawei.pictureWall.tools.util;

import java.text.SimpleDateFormat;

/***
 * @date 创建时间 2018/9/4 22:20
 * @author 作者: W.YuLong
 * @description
 */
public class ValueConstant {
    public static final String RECEIVE_PHOTO = "com.hengke.photowall.Receive_photo";

    public static final String EXTRA_FILE_NAME = "FileName";
    public static final String EXTRA_IS_STOP_SERVICE = "StopService";

    public static final SimpleDateFormat sdf_ymdhms = new SimpleDateFormat("yyyy_MM_dd_HH_mm_ss");

}
