package com.huawei.pictureWall.tools.upload;

/***
 * @date 创建时间 2018/7/30 10:53
 * @author 作者: W.YuLong
 * @description
 */
public interface OnUploadFileCallback {
    void onCallBack(String url, float faceScore);
    void onFail(String msg);
}
