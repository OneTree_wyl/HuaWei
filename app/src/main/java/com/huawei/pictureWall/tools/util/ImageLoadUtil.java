package com.huawei.pictureWall.tools.util;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DecodeFormat;
import com.bumptech.glide.load.resource.bitmap.CenterCrop;
import com.bumptech.glide.load.resource.bitmap.CircleCrop;
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;
import com.huawei.pictureWall.base.APP;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;

/***
 *@date 创建时间 2018/3/23 11:02
 *@author 作者: yulong
 *@description 图片的加载工具类
 */
public class ImageLoadUtil {


    public static <T> void loadImageWithOption(final ImageView imageView, T url, RequestOptions options) {
        System.out.println("url = " + url);
        Glide.with(imageView.getContext()).load(url)
//                .transition(DrawableTransitionOptions.withCrossFade())
                .apply(options).into(imageView);
    }

    public static <T> void loadImageWithDefault(final ImageView imageView, T imgUrl, int defaultId) {
        if (imgUrl != null) {
            RequestOptions requestOptions = new RequestOptions();

            requestOptions.placeholder(defaultId).format(DecodeFormat.PREFER_RGB_565);
            Glide.with(imageView.getContext()).load(imgUrl)
                    .transition(DrawableTransitionOptions.withCrossFade())
                    .thumbnail(0.4f)
                    .apply(requestOptions).into(imageView);
        }
    }


    public static <T> void loadRoundImage(final ImageView imageView, T t, int radius) {
        RequestOptions options = new RequestOptions();
        options.transform(new GlideRoundTransform(imageView.getContext(), radius))
                .format(DecodeFormat.PREFER_RGB_565);
        Glide.with(imageView.getContext()).load(t).apply(options)
                .transition(DrawableTransitionOptions.withCrossFade())
                .into(imageView);
    }

    public static <T> void loadImage(final ImageView imageView, T t) {
        Glide.with(imageView.getContext()).load(t)
                .transition(DrawableTransitionOptions.withCrossFade())
                .into(imageView);
    }


    public static void loadImageByUrl(final ImageView imageView, String imgUrl) {
        if (!TextUtils.isEmpty(imgUrl)) {
            Glide.with(imageView.getContext()).load(imgUrl)
                    .transition(DrawableTransitionOptions.withCrossFade())
                    .into(imageView);
        }
    }

    public static <T> void loadCircleImageWithDefault(final ImageView imageView, T imgUrl, int defaultResId) {
        RequestOptions requestOptions = new RequestOptions();
        requestOptions.transform(new CenterCrop());
        Glide.with(imageView.getContext()).load(imgUrl).apply(requestOptions).into(new SimpleTarget<Drawable>() {
            @Override
            public void onResourceReady(@NonNull Drawable resource, @Nullable Transition<? super Drawable> transition) {
                loadCircleImage(imageView, imgUrl);
            }

            @Override
            public void onLoadFailed(@Nullable Drawable errorDrawable) {
                super.onLoadFailed(errorDrawable);
                loadCircleImage(imageView, defaultResId);
            }
        });
    }

    /*加载图片转换为圆形*/
    public static <T> void loadCircleImage(final ImageView imageView, T path) {
        RequestOptions options = RequestOptions.bitmapTransform(new CircleCrop()).format(DecodeFormat.PREFER_RGB_565);
        Glide.with(imageView.getContext()).load(path).apply(options).into(imageView);
    }


    public static void getBitmapFromUrl(String url, int defaultImg, SimpleTarget<Drawable> simpleTarget) {
        Glide.with(APP.get()).load(url).into(simpleTarget);
    }

    /*将Bitmap写入到文件中*/
    public static boolean writeBitmapToFile(Bitmap bitmap, String path) {
        boolean isSuccess = false;
        File file = new File(path);
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        try {
            FileOutputStream fos = new FileOutputStream(file);
            try {
                fos.write(baos.toByteArray());
                fos.flush();
                fos.close();

                if (bitmap.isRecycled()) {
                    bitmap.recycle();
                }

                isSuccess = true;
            } catch (IOException e) {
                e.printStackTrace();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        return isSuccess;
    }

    public static byte[] bmpToByteArray(final Bitmap bmp, final boolean needRecycle) {
        ByteArrayOutputStream output = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.PNG, 100, output);
        if (needRecycle) {
            bmp.recycle();
        }

        byte[] result = output.toByteArray();
        try {
            output.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }


    public static Bitmap getRoundCorner(File file, int size) {
        return getRoundCorner(loadBitmapFromFile(file, size));
    }

    public static Bitmap getRoundCorner(Bitmap bitmap) {
        Bitmap output = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(output);
        final Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());
        final RectF rectF = new RectF(0, 0, bitmap.getWidth(), bitmap.getHeight());


        final float roundPy = /*Math.min(APPUtil.getScreenWidth(), APPUtil.getScreenHeight()) * 0.02f; */
                Math.min(output.getWidth(), output.getHeight()) * 0.01f;

        final Paint paint = new Paint();
        paint.setAntiAlias(true);
        canvas.drawARGB(0, 0, 0, 0);
        canvas.drawRoundRect(rectF, roundPy, roundPy, paint);

        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        canvas.drawBitmap(bitmap, rect, rect, paint);

        bitmap.recycle();

        return output;
    }

    public static Bitmap loadBitmapFromFile(File file, int size) {
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(file.getAbsolutePath(), options);

        int minSize = Math.min(options.outHeight, options.outWidth);

//        CLog.d(String.format("w= %d, h = %d, minsize = %d", options.outWidth, options.outHeight, minSize));
        int samplePart = 1;

        for (int i = 0; i < 15; i++) {
            if (minSize / samplePart * 2 < size * 2) {
                break;
            }
            samplePart = samplePart << 1;
//            CLog.d("samplePart = " + samplePart);
        }

        options.inSampleSize = samplePart;
        options.inJustDecodeBounds = false;
        Bitmap bitmap = BitmapFactory.decodeFile(file.getAbsolutePath(), options);
        CLog.d(String.format("name = %s, samplePart = %d,缩小后:bitmap_w = %d, bitmap_H = %d",
                file.getName(), samplePart, options.outWidth, options.outHeight, bitmap.getWidth(), bitmap.getHeight()));
        return bitmap;
    }

    public static void loadSmallBitmap(ImageView imageView, File file, int size) {
        Observable.create(new ObservableOnSubscribe() {
            @Override
            public void subscribe(ObservableEmitter e) throws Exception {
                e.onNext(file);
                e.onComplete();
            }
        }).map(new Function<File, Bitmap>() {
            @Override
            public Bitmap apply(File file) throws Exception {
                return loadBitmapFromFile(file, size);
            }
        }).observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.newThread())
                .subscribe(new Observer<Bitmap>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(Bitmap bitmap) {
                        imageView.setImageBitmap(bitmap);
                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onComplete() {

                    }
                });

    }
}
