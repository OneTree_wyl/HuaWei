package com.huawei.pictureWall.tools.util;

/***
 * @date 创建时间 2018/8/21 21:33
 * @author 作者: W.YuLong
 * @description 一些常量的定义
 */
public class Configuration {

    public static final int BUFFER_LENGTH = 1024 * 8;

    public static final String EOF = "\neofeof";
    public static final String DELIMITER = EOF + EOF;

    public static final int UDP_PORT = 9105;
    public static int currentTcpPort = 34523;

//    public static int rawCount = 4;


    public static final int WAIT_TIME_SECOND = 5;

    public static final int CONNECT_TIMES = 5;
}
