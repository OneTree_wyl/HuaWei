package com.huawei.pictureWall.tools.util;
/**/

import android.app.Activity;
import android.content.Context;
import android.content.CursorLoader;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.WindowManager;

import com.google.gson.reflect.TypeToken;
import com.huawei.pictureWall.base.APP;
import com.huawei.pictureWall.tools.upload.OnUploadFileCallback;
import com.huawei.pictureWall.tools.upload.PictureUpLoadBean;
import com.hwariot.lib.api.BaseViewInterface;
import com.hwariot.lib.api.PresenterSingleton;
import com.hwariot.lib.bean.BaseBean;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.Map;

/***
 * @date 创建时间 2018/3/23 11:32
 * @author 作者: yulong
 * @description APP的相关的操作的工具方法
 */
public class APPUtil {
    private static int screenWidth, screenHeight;

    /*这个是为了计算View在GONE的情况下计算出测量出来宽高的*/
    public static int[] measureGoneViewSize(View view) {
        int size[] = new int[2];
        int width = View.MeasureSpec.makeMeasureSpec(0,
                View.MeasureSpec.UNSPECIFIED);
        int height = View.MeasureSpec.makeMeasureSpec(0,
                View.MeasureSpec.UNSPECIFIED);
        view.measure(width, height);
        size[0] = view.getMeasuredWidth();
        size[1] = view.getMeasuredHeight();
        return size;
    }

    public static int getVersionCode() {
        PackageManager packageManager = APP.get().getPackageManager();
        PackageInfo packageInfo;
        int versionCode = 0;
        try {
            packageInfo = packageManager.getPackageInfo(APP.get().getPackageName(), 0);
            versionCode = packageInfo.versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return versionCode;
    }

    public static String getVersionName() {
        PackageManager packageManager = APP.get().getPackageManager();
        PackageInfo packageInfo;
        String versionName = "";
        try {
            packageInfo = packageManager.getPackageInfo(APP.get().getPackageName(), 0);
            versionName = packageInfo.versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return versionName;
    }

    /**
     * 判断当前应用是否是debug状态
     */
    public static boolean isApkInDebug() {
        try {
            ApplicationInfo info = APP.get().getApplicationInfo();
            return (info.flags & ApplicationInfo.FLAG_DEBUGGABLE) != 0;
        } catch (Exception e) {
            return false;
        }
    }

    public static int getScreenHeight() {
        if (screenHeight <= 0) {
            initScreenSize();
        }
        return screenHeight;
    }


    public static int getMinScreenSize() {
        return Math.min(getScreenHeight(), getScreenWidth());
    }

    public static int getMaxScreenSize() {
        return Math.max(getScreenHeight(), getScreenWidth());
    }


    public static int getScreenWidth() {
        if (screenWidth <= 0) {
            initScreenSize();
        }
        return screenWidth;
    }


    private static void initScreenSize() {
        WindowManager manager = (WindowManager) APP.get().getSystemService(Context.WINDOW_SERVICE);
        DisplayMetrics outMetrics = new DisplayMetrics();
        manager.getDefaultDisplay().getMetrics(outMetrics);
        screenWidth = outMetrics.widthPixels;
        screenHeight = outMetrics.heightPixels;
    }

    public static String byte2String(byte[] buffer) {
        try {
            String msg = new String(buffer, "utf-8");
            int eof = msg.indexOf(Configuration.EOF);
            if (eof > 0) {
                return msg.substring(0, eof);
            } else {
                return msg;
            }
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return null;
    }


    public static int dp2px(float dpValue) {
        final float scale = APP.get().getResources().getDisplayMetrics().density;
        return (int) (dpValue * scale + 0.5f);
    }

    public static boolean checkAllStringsNotNull(String... inputs) {
        for (String input : inputs) {
            if (TextUtils.isEmpty(input)) {
                return false;
            }
        }
        return true;
    }

    public static String getDeviceIdString() {
        try {
            String mSzDevIDShort = "35" + // we make this look like a valid IMEI
                    Build.BOARD.length() % 10 + Build.BRAND.length() % 10 + Build.CPU_ABI.length() % 10
                    + Build.DEVICE.length() % 10 + Build.DISPLAY.length() % 10 + Build.HOST.length() % 10
                    + Build.ID.length() % 10 + Build.MANUFACTURER.length() % 10 + Build.MODEL.length() % 10
                    + Build.PRODUCT.length() % 10 + Build.TAGS.length() % 10 + Build.TYPE.length() % 10
                    + Build.USER.length() % 10; // 13 digits
            return mSzDevIDShort;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "888";
    }

    public static String getMacAddressString(Context act) {
        try {
            WifiManager wifi = (WifiManager) act.getSystemService(Context.WIFI_SERVICE);
            WifiInfo info = wifi.getConnectionInfo();
            return info.getMacAddress();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "888";
    }

    public static void doCall(Activity act, String number) {
        if (TextUtils.isEmpty(number)) {
            ToastUtil.showToastShort("电话号码为空");
            return;
        }
        Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + number));
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        act.startActivity(intent);
    }


    public static String getVersionInfo() {
        // 获取packagemanager的实例
        PackageManager packageManager = APP.get().getPackageManager();
        // getPackageName()是你当前类的包名，0代表是获取版本信息
        PackageInfo packInfo = null;
        try {
            packInfo = packageManager.getPackageInfo(APP.get().getPackageName(), 0);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        StringBuilder sb = new StringBuilder();
        if (packInfo != null) {
            sb.append("V" + packInfo.versionName)
                    .append("(" + packInfo.versionCode + ")");
        }
        return sb.toString();
    }

    /*判断传入的字符串是否都为空*/
    public static boolean checkALlStringsEmpty(String... args) {
        boolean flag = true;
        for (String str : args) {
            if (!TextUtils.isEmpty(str)) {
                flag = false;
                break;
            }
        }
        return flag;
    }

    /*判断文件是不是图片*/
    public static boolean isImageFile(String filePath) {
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(filePath, options);
        if (options.outWidth == -1) {
            return false;
        }
        return true;
    }

    /**
     * 从URI获取本地路径
     */
    public static String getAbsoluteImagePath(Uri contentUri) {

        //如果是对媒体文件，在android开机的时候回去扫描，然后把路径添加到数据库中。
        //由打印的contentUri可以看到：2种结构。正常的是：content://那么这种就要去数据库读取path。
        //另外一种是Uri是 file:///那么这种是 Uri.fromFile(File file);得到的
//        CLog.d(contentUri.toString());

        String[] projection = {MediaStore.Images.Media.DATA};
        String urlpath;
        CursorLoader loader = new CursorLoader(APP.get(), contentUri, projection, null, null, null);
        Cursor cursor = loader.loadInBackground();
        try {
            int column_index = cursor.getColumnIndex(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            urlpath = cursor.getString(column_index);
            //如果是正常的查询到数据库。然后返回结构
            return urlpath;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
        //如果是文件。Uri.fromFile(File file)生成的uri。那么下面这个方法可以得到结果
        urlpath = contentUri.getPath();
        return urlpath;
    }


    public static void uploadFileToAliYun(File file, OnUploadFileCallback callback) {
//        CommProgressDialog commProgressDialog = null;
//        if (context != null) {
//            commProgressDialog = new CommProgressDialog.Builder(context)
//                    .setTitle("正在上传中...").create();
//            commProgressDialog.show();
//        }
//        CommProgressDialog finalCommProgressDialog = commProgressDialog;

        BaseViewInterface baseViewInterface = new BaseViewInterface() {
            private String successUrl;
            private PictureUpLoadBean dataInfo;

            @Override
            public void onFail(String api, BaseBean msg) {
//                if (finalCommProgressDialog != null && finalCommProgressDialog.isShowing()) {
//                    finalCommProgressDialog.dismiss();
//                }
                callback.onFail(msg.getStatusBean().getMessage());
            }

            @Override
            public <T> void initData(String api, T t) {
                if (api.equals(APIDefine.API_resources_signature)) {
                    if (t == null) {
                        ToastUtil.showToastShort("获取的数据为空");
                        return;
                    }
                    dataInfo = (PictureUpLoadBean) t;
                    CLog.d(JsonUtil.objectToJson(t));
//                    successUrl = dataInfo.getUrl();
//
//                    Map<String, String> map = new HashMap<>();
//                    map.clear();
//                    map.put("key", dataInfo.getKey());
//                    map.put("Content-Type", dataInfo.getContentType());
//                    map.put("policy", dataInfo.getPolicy());
//                    map.put("signature", dataInfo.getSignature());
//                    map.put("OSSAccessKeyId", dataInfo.getOSSAccessKeyId());
//                    PresenterSingleton.get().uploadFileWithParams(dataInfo.getDomain(), file, map, this);
                } else if (dataInfo != null && api.equals(dataInfo.getHost())) {
                    if (!APPUtil.isApkInDebug() && file != null && file.exists()) {
                        file.delete();
                        CLog.d("上传成功，删除文件:" + file.getAbsolutePath());
                    }
//                    if (finalCommProgressDialog != null && finalCommProgressDialog.isShowing()) {
//                        finalCommProgressDialog.dismiss();
//                    }

                    callback.onCallBack(successUrl, 0);
                }
            }
        };

        Map<String, String> map = new HashMap<>();
//        String randomName = (new Date()).getTime() + "-" + EncoderUtil.encode(file.getName());
//        int id = 0;
//        if (UserSingleton.get().hasLogin()) {
//            id = UserSingleton.get().getId();
//        }
//        map.put("path", serviceName + id);
//        map.put("file_name", randomName);

        Type type = new TypeToken<BaseBean<PictureUpLoadBean>>() {
        }.getType();
        PresenterSingleton.get().doGetData(null, APIDefine.API_resources_signature,
                map, type, baseViewInterface);
    }


}
