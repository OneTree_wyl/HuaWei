package com.huawei.pictureWall.tools.widget.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.util.AttributeSet;
import android.view.View;

import com.huawei.pictureWall.R;


public class CustomRecyclerView extends RecyclerView {

    public static final int LIST_VERTICAL = 1;
    public static final int LIST_HORIZONTAL = 0;
    public static final int GRID_VERTICAL = 2;
    public static final int GRID_HORIZONTAL = 4;
    public static final int OTHER = 9;

    private boolean isExpanded = false;
    private int layoutStyle;
    private int maxHeight = -1;
    private int spanCount;



    public CustomRecyclerView(Context context, AttributeSet attrs) {
        super(context, attrs);
        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.CustomRecyclerView);
        layoutStyle = a.getInt(R.styleable.CustomRecyclerView_app_layout_style, 0);
        int divideLineStyle = a.getInt(R.styleable.CustomRecyclerView_app_divide_line_style, 0);
        spanCount = a.getInteger(R.styleable.CustomRecyclerView_app_span_count, 2);
        int itemSpace = a.getDimensionPixelOffset(R.styleable.CustomRecyclerView_app_item_margin, 0);
        int topMargin = a.getDimensionPixelOffset(R.styleable.CustomRecyclerView_app_first_item_top_margin, 0);
        int bottomMargin = a.getDimensionPixelOffset(R.styleable.CustomRecyclerView_app_last_item_bottom_margin, 0);
        int divideLineColor = a.getColor(R.styleable.CustomRecyclerView_app_item_divide_line_color, -1);
        maxHeight = a.getDimensionPixelOffset(R.styleable.CustomRecyclerView_app_recycler_view_max_height, -1);
        isExpanded = a.getBoolean(R.styleable.CustomRecyclerView_app_is_expanded, false);
        a.recycle();

        addItemDecoration(new SpacesItemDecoration(layoutStyle, itemSpace, topMargin, bottomMargin, spanCount));
        setLayoutStyle(layoutStyle, spanCount);
        setDivideLineStyle(divideLineStyle, divideLineColor);
    }


    @Override
    protected void onMeasure(int widthSpec, int heightSpec) {
        if (maxHeight != -1) {
            int width = getDefaultSize(getWidth(), widthSpec);
            int height = getDefaultSize(getHeight(), heightSpec);
            height = Math.min(height, maxHeight);
            setMeasuredDimension(width, height);
        } else {
            if (isExpanded) {
                int measureSpec = MeasureSpec.makeMeasureSpec(Integer.MAX_VALUE >> 2,
                        MeasureSpec.AT_MOST);
                super.onMeasure(widthSpec, measureSpec);
            } else {
                super.onMeasure(widthSpec, heightSpec);
            }
        }

    }

    private void setDivideLineStyle(int divideLine, int divideColor) {
        if (divideLine != 0) {
            int orientation;
            if (divideLine == 1) {
                orientation = LinearLayoutManager.HORIZONTAL;
            } else {
                orientation = LinearLayoutManager.VERTICAL;
            }
            DividerLine dividerItemDecoration = new DividerLine(orientation, divideColor);
            addItemDecoration(dividerItemDecoration);
        }

    }



    public void setLayoutStyle(int layoutStyle, int spanCount) {
        LayoutManager layoutManager;
        switch (layoutStyle) {
            case LIST_HORIZONTAL:
                layoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false);
                break;
            case GRID_VERTICAL:
                layoutManager = new GridLayoutManager(getContext(), spanCount, GridLayoutManager.VERTICAL, false);
                break;
            case GRID_HORIZONTAL:
                layoutManager = new GridLayoutManager(getContext(), spanCount, GridLayoutManager.HORIZONTAL, false);
                break;
            case 9:
                layoutManager = new StaggeredGridLayoutManager(spanCount, StaggeredGridLayoutManager.VERTICAL);
                break;
            case LIST_VERTICAL:
            default: //默认的就是列表模式
                layoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
                break;
        }
        setLayoutManager(layoutManager);
    }

    public static class DividerLine extends ItemDecoration {
        // 画笔
        private Paint paint;
        // 布局方向
        private int orientation;
        // 分割线尺寸
        private int size = 2;

        public DividerLine(int orientation, int color) {
            this.orientation = orientation;
            paint = new Paint();
            paint.setAntiAlias(true);
            if (color != -1) {
                paint.setColor(color);
            }
        }

        @Override
        public void onDrawOver(Canvas c, RecyclerView parent, State state) {
            super.onDrawOver(c, parent, state);
            if (orientation == LinearLayoutManager.HORIZONTAL) {
                drawVerticalLine(c, parent);
            } else {
                drawHorizontalLine(c, parent);
            }
        }

        /*设置分割线颜色*/
        public void setColor(int color) {
            paint.setColor(color);
        }

        /* 设置分割线尺寸*/
        public void setSize(int size) {
            this.size = size;
        }

        // 画水平分割线
        protected void drawHorizontalLine(Canvas c, RecyclerView parent) {
            final int top = parent.getPaddingTop();
            final int bottom = parent.getHeight() - parent.getPaddingBottom();

            final int childCount = parent.getChildCount();
            for (int i = 0; i < childCount; i++) {
                final View child = parent.getChildAt(i);
                final LayoutParams params = (LayoutParams) child.getLayoutParams();
                final int left = child.getRight() + params.rightMargin;
                final int right = left + size;
                c.drawRect(left, top, right, bottom, paint);
            }
        }

        // 画垂直分割线
        protected void drawVerticalLine(Canvas c, RecyclerView parent) {
            final int left = parent.getPaddingLeft();
            final int right = parent.getWidth() - parent.getPaddingRight();

            final int childCount = parent.getChildCount();
            for (int i = 0; i < childCount; i++) {
                final View child = parent.getChildAt(i);
                final LayoutParams params = (LayoutParams) child.getLayoutParams();
                final int top = child.getBottom() + params.bottomMargin;
                final int bottom = top + size;
                c.drawRect(left, top, right, bottom, paint);
            }
        }
    }


    public static class SpacesItemDecoration extends ItemDecoration {
        private int space;
        private int startMargin;
        private int endMargin;
        private int spanCount;
        int orientation;

        public SpacesItemDecoration(int orientation, int space, int topMargin, int bottomMargin, int spanCount) {
            this.orientation = orientation;
            this.space = space;
            this.endMargin = bottomMargin;
            this.startMargin = topMargin;
            this.spanCount = spanCount;
        }

        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent, State state) {
            int position = parent.getChildAdapterPosition(view);

            int itemCount = parent.getAdapter().getItemCount();
            int lastRemainCount = itemCount % spanCount;
            if (lastRemainCount == 0) {
                lastRemainCount = spanCount;
            }

            switch (orientation) {
                case LIST_HORIZONTAL:
                    if (position == 0) {
                        outRect.left = startMargin;
                        outRect.right = space;
                    } else if (position == parent.getAdapter().getItemCount() - 1) {
                        outRect.right = endMargin;
                    } else {
                        outRect.right = space;
                    }
                    break;
                case LIST_VERTICAL:
                    if (position == 0) {
                        outRect.top = startMargin;
                        outRect.bottom = space;
                    } else if (position == parent.getAdapter().getItemCount() - 1) {
                        outRect.bottom = endMargin;
                    } else {
                        outRect.bottom = space;
                    }
                    break;
                case GRID_VERTICAL:
                    //最左边的item
                    if (position % spanCount == 0) {
                        if (position < spanCount) {
                            outRect.top = startMargin;
                        } else {
                            outRect.top = space;
                        }
//                        outRect.right = space / 2;
                        outRect.bottom = space / 2;
                    }
                    //最右边的item
                    else if (position + 1 % spanCount == 0) {
                        if (position < spanCount) {
                            outRect.top = startMargin;
                        } else {
                            outRect.top = space;
                        }
                        outRect.left = space / 2;
                        outRect.bottom = space / 2;
                    }
                    //中间的item
                    else {
                        if (position < spanCount) {
                            outRect.top = startMargin;
                        } else {
                            outRect.top = space;
                        }
                        outRect.left = space / 2;
                        outRect.right = space / 2;
                    }
                    //最底部一排和下边界的间距
                    if (itemCount - position <= lastRemainCount) {
                        outRect.bottom = endMargin;
                    }
                    break;
                case GRID_HORIZONTAL:
                    //最顶部一排边的item
                    if (position % spanCount == 0) {
                        if (position == 0) {
                            outRect.left = startMargin;
                        } else {
                            outRect.left = space;
                        }
                        outRect.bottom = space / 2;

                    }
                    //最底部一排边的item
                    else if (position + 1 % spanCount == 0) {
                        if (position < spanCount) {
                            outRect.left = startMargin;
                        } else {
                            outRect.left = space;
                        }
                        outRect.top = space / 2;
                    }
                    //中间的item
                    else {
                        if (position < spanCount) {
                            outRect.left = startMargin;
                        } else {
                            outRect.left = space;
                        }
                        outRect.top = space / 2;
                        outRect.bottom = space / 2;
                    }

                    if (itemCount - position <= lastRemainCount) {
                        outRect.right = endMargin;
                    }
                    break;
                case OTHER:
                    break;
            }
        }
    }

}
