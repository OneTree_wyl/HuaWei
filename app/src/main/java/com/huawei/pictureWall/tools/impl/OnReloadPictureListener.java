package com.huawei.pictureWall.tools.impl;

/***
 * @date 创建时间 2018/9/7 22:17
 * @author 作者: W.YuLong
 * @description
 */
public interface OnReloadPictureListener {
    void onReloadPicture();


}
