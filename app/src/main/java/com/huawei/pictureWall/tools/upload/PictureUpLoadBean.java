package com.huawei.pictureWall.tools.upload;

import com.google.gson.annotations.SerializedName;

/***
 *@date 创建时间 2018/4/10
 *@author 作者: Su MingHui
 *@description 图片或视频上传阿里云bean
 */

public class PictureUpLoadBean {

    /**
     * accessid : STS.NKWjbwE9iHF8ZDY8FQTEQbcPo
     * secret : AbM9Tjm72rCJDzPh4RvboUhw3S1wcBAnYpV2owdM9j4n
     * host : http://hkxy-server.oss-cn-shanghai.aliyuncs.com
     * endpoint : oss-cn-shanghai
     * bucketName : hkxy-server
     * policy :
     * signature : CAISlQJ1q6Ft5B2yfSjIr4jiIdjDqOZI/4TTWGLo3EYEWMp9jab7jTz2IH1PenVoAeoXs/s+mWhR6vwalrJaT55UWErjVvBM6Zda9yysZYfbstCy94YDjJD9y859kpO/jqHoeOzcYI73WJXEMiLp9EJaxb/9ak/RPTiMOoGIjphKd8keWhLCAxNNGNZRIHkJyqZYTwyzU8ygKRn3mGHdIVN1sw5n8wNF5L+439eX52i17jS46JdM+9+hfMT6MZE9ZsckCIft5oEsKPqdihw3wgNR6aJ7gJZD/Tr6pdyHCzFTmU7baLSIqoU1fVImN/ViQvIe9OKPnPl5q/HVkJ/s1xFOMOdaXiLSXom8x9HeH+ekJgrD934D3dunGoABRIPp2N76GzY3a27oT7yFVh72DZjl3SM6NdO3blpMzr2b/9kI/pqj2sxHpEsicBVx9Fb0MFPbbg8WhmUrw3pdjcrqX5eVBmJpS2JYwsLK5a1GwqOTtC4HrLlgfB0wbPwieWFq4t7gyLkXZmLwBI+HW4STMt4YGor6KafLjvXBmfY=
     * expire : 2018-10-10T13:19:15Z
     * objectKey : images
     */

    @SerializedName("accessid") private String accessid;
    @SerializedName("secret") private String secret;
    @SerializedName("host") private String host;
    @SerializedName("endpoint") private String endpoint;
    @SerializedName("bucketName") private String bucketName;
    @SerializedName("policy") private String policy;
    @SerializedName("signature") private String signature;
    @SerializedName("expire") private String expire;
    @SerializedName("objectKey") private String objectKey;

    public String getAccessid() {
        return accessid;
    }

    public void setAccessid(String accessid) {
        this.accessid = accessid;
    }

    public String getSecret() {
        return secret;
    }

    public void setSecret(String secret) {
        this.secret = secret;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public String getEndpoint() {
        return endpoint;
    }

    public void setEndpoint(String endpoint) {
        this.endpoint = endpoint;
    }

    public String getBucketName() {
        return bucketName;
    }

    public void setBucketName(String bucketName) {
        this.bucketName = bucketName;
    }

    public String getPolicy() {
        return policy;
    }

    public void setPolicy(String policy) {
        this.policy = policy;
    }

    public String getSignature() {
        return signature;
    }

    public void setSignature(String signature) {
        this.signature = signature;
    }

    public String getExpire() {
        return expire;
    }

    public void setExpire(String expire) {
        this.expire = expire;
    }

    public String getObjectKey() {
        return objectKey;
    }

    public void setObjectKey(String objectKey) {
        this.objectKey = objectKey;
    }
}
