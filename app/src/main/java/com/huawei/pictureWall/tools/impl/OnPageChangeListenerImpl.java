package com.huawei.pictureWall.tools.impl;

import android.support.v4.view.ViewPager;

/***
 * @date 创建时间 2018/9/15 11:47
 * @author 作者: W.YuLong
 * @description
 */
public class OnPageChangeListenerImpl implements ViewPager.OnPageChangeListener {
    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {

    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }
}
