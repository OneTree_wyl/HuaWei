package com.huawei.pictureWall.tools.util;

import android.Manifest;

/***
 * @date 创建时间 2018/5/18 18:19
 * @author 作者: W.YuLong
 * @description 一些权限的申请定义
 */
public class PermissionGroupDefine {

    public final static String[] READ_PHONE_STATE= {
            Manifest.permission.READ_PHONE_STATE,
    };

    public final static String[] PERMISSION_RECORD_VIDEO= {
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.RECORD_AUDIO,
            Manifest.permission.CAMERA,
            Manifest.permission.READ_EXTERNAL_STORAGE,
    };

    public final static String[] PERMISSION_LOCATION_AND_WIF = {
            Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.ACCESS_COARSE_LOCATION,
            Manifest.permission.CHANGE_WIFI_STATE,
    };

    public final static String[] PERMISSION_LOCATION_ONLY = {
            Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.ACCESS_COARSE_LOCATION,
    };

    public final static String[] PERMISSION_LOCATION = {
            Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.ACCESS_COARSE_LOCATION,
            Manifest.permission.READ_PHONE_STATE,
    };

    public final static String[] PERMISSION_DIAL_PHONE = {
            Manifest.permission.CALL_PHONE,
    };

    public final static String[] PERMISSION_CAMERA = {
            Manifest.permission.CAMERA,
    };


    public final static String[] PERMISSION_SD_CARD = {
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
    };

    public final static String[] PERMISSION_SD_CARD_AND_CAMERA = {
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.CAMERA,
    };

    public final static String[] PERMISSION_SD_CARD_AND_PHONE = {
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.READ_PHONE_STATE,
    };

}
