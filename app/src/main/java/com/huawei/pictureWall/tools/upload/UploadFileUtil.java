package com.huawei.pictureWall.tools.upload;

import com.alibaba.sdk.android.oss.ClientConfiguration;
import com.alibaba.sdk.android.oss.ClientException;
import com.alibaba.sdk.android.oss.OSS;
import com.alibaba.sdk.android.oss.OSSClient;
import com.alibaba.sdk.android.oss.ServiceException;
import com.alibaba.sdk.android.oss.callback.OSSCompletedCallback;
import com.alibaba.sdk.android.oss.common.OSSLog;
import com.alibaba.sdk.android.oss.common.auth.OSSCredentialProvider;
import com.alibaba.sdk.android.oss.common.auth.OSSStsTokenCredentialProvider;
import com.alibaba.sdk.android.oss.model.PutObjectRequest;
import com.alibaba.sdk.android.oss.model.PutObjectResult;
import com.google.gson.reflect.TypeToken;
import com.huawei.pictureWall.base.APP;
import com.huawei.pictureWall.tools.db.PictureDBSingleton;
import com.huawei.pictureWall.tools.db.entity.PictureInfoEntity;
import com.huawei.pictureWall.tools.event.UploadFileEvent;
import com.huawei.pictureWall.tools.singleton.ThreadPoolSingleton;
import com.huawei.pictureWall.tools.util.APIDefine;
import com.huawei.pictureWall.tools.util.CLog;
import com.huawei.pictureWall.tools.util.JsonUtil;
import com.huawei.pictureWall.tools.util.ToastUtil;
import com.hwariot.lib.api.BaseViewInterface;
import com.hwariot.lib.api.PresenterSingleton;
import com.hwariot.lib.bean.BaseBean;

import org.greenrobot.eventbus.EventBus;

import java.io.File;
import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.Map;

/***
 * @date 创建时间 2018/10/10 14:35
 * @author 作者: W.YuLong
 * @description
 */
public class UploadFileUtil {


    public static void doUploadFile(File file) {
        if (file == null){
            ToastUtil.showToastShort("图片文件不存在");
            return;
        }

        Type type = new TypeToken<BaseBean<PictureUpLoadBean>>() {
        }.getType();
        Map map = new HashMap();
        map.put("file_name", getFileWithoutSuffix(file));
        PresenterSingleton.get().doGetData(null, APIDefine.API_get_upload_config,
                map, type, new BaseViewInterface() {
                    @Override
                    public void onFail(String api, BaseBean msg) {

                    }

                    @Override
                    public <T> void initData(String api, T t) {
                        PictureUpLoadBean bean = (PictureUpLoadBean) t;
                        CLog.json(JsonUtil.objectToJson(t));
                        ThreadPoolSingleton.get().executeTask(new Runnable() {
                            @Override
                            public void run() {
                                uploadFile(bean, file);
                            }
                        });
                    }
                });

    }

    private static String getFileWithoutSuffix(File file){
        // 构造上传请求
        String fileName = "";
        if (file.getName().contains(".")) {
            fileName = file.getName().substring(0, file.getName().indexOf("."));
        }
        return fileName;
    }

    public static void uploadFile(PictureUpLoadBean bean, File file) {
        // 在移动端建议使用STS方式初始化OSSClient。
        // 更多信息可查看sample 中 sts 使用方式(https://github.com/aliyun/aliyun-oss-android-sdk/tree/master/app/src/main/java/com/alibaba/sdk/android/oss/app)
//        OSSCredentialProvider credentialProvider = new OSSStsTokenCredentialProvider("<StsToken.AccessKeyId>", "<StsToken.SecretKeyId>", "<StsToken.SecurityToken>");
        OSSCredentialProvider credentialProvider =
                new OSSStsTokenCredentialProvider(bean.getAccessid(), bean.getSecret(), bean.getSignature());
        //该配置类如果不设置，会有默认配置，具体可看该类
        ClientConfiguration conf = new ClientConfiguration();
        conf.setConnectionTimeout(15 * 1000); // 连接超时，默认15秒
        conf.setSocketTimeout(15 * 1000); // socket超时，默认15秒
        conf.setMaxConcurrentRequest(5); // 最大并发请求数，默认5个
        conf.setMaxErrorRetry(2); // 失败后最大重试次数，默认2次
        OSSLog.enableLog();


        OSS oss = new OSSClient(APP.get(), bean.getEndpoint(), credentialProvider);

        // 构造上传请求
        String fileName = getFileWithoutSuffix(file);

        PutObjectRequest request = new PutObjectRequest(bean.getBucketName(), bean.getObjectKey() + "/" + fileName, file.getAbsolutePath());

        oss.asyncPutObject(request, new OSSCompletedCallback<PutObjectRequest, PutObjectResult>() {
            @Override
            public void onSuccess(PutObjectRequest request, PutObjectResult result) {
                PictureInfoEntity entity = new PictureInfoEntity();
                entity.setDateTime(System.currentTimeMillis());
                entity.setDirPath(file.getParent());
//                entity.setUrl(String.format("%s/%s/%s", bean.getHost(), bean.getObjectKey(), finalFileName));
                entity.setUrl(bean.getHost());
                entity.setFileName(file.getName());
                PictureDBSingleton.get().insertOrUpdateData(entity);

                //发送时间消息
                UploadFileEvent uploadFileEvent = new UploadFileEvent();
                uploadFileEvent.setFileName(file.getName());
                uploadFileEvent.setUrl(entity.getUrl());
                EventBus.getDefault().post(uploadFileEvent);

            }

            @Override
            public void onFailure(PutObjectRequest request, ClientException clientExcepion, ServiceException serviceException) {
                // 请求异常
                if (clientExcepion != null) {
                    // 本地异常如网络异常等
                    clientExcepion.printStackTrace();
                }
//                ToastUtil.showToastLong("上传失败，请检查");
                if (serviceException != null) {
                }
            }
        });
// task.cancel(); // 可以取消任务
// task.waitUntilFinished(); // 可以等待直到任务完成
    }
}
