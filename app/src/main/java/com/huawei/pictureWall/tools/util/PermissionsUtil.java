package com.huawei.pictureWall.tools.util;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.PermissionChecker;
import android.support.v7.app.AlertDialog;

import java.util.HashMap;
import java.util.Map;

/***
 * @date 创建时间 2018/8/18 21:21
 * @author 作者: W.YuLong
 * @description 权限申请的封装
 */
public class PermissionsUtil extends Activity {
    private static final int PERMISSION_REQUEST_CODE = 64;

    private static final int REQUEST_SETTING_CODE = 121;

    private String[] permission;
    private String key;
    private boolean isShowTip;
    private PermissionTipInfo tipInfo;
    private static Map<String, OnPermissionCallback> listenersMap = new HashMap<>();

    public static void requestPermission(Context context, String[] permission, OnPermissionCallback onPermissionCallback) {
        requestPermission(context, null, permission, true, onPermissionCallback);
    }

    public static void requestPermission(Context context, String[] permission, boolean isShowTip, OnPermissionCallback onPermissionCallback) {
        requestPermission(context, null, permission, isShowTip, onPermissionCallback);
    }

    public static void requestPermission(Context context, PermissionTipInfo tipInfo, String[] permission, boolean isShowTip, OnPermissionCallback onPermissionCallback) {
        Intent intent = new Intent(context, PermissionsUtil.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

        if (tipInfo != null) {
            intent.putExtra("tipInfo", tipInfo);
        }

        String key = System.currentTimeMillis() + "";
        intent.putExtra("key", key);
        intent.putExtra("permission", permission);
        intent.putExtra("isShowTip", isShowTip);
        listenersMap.put(key, onPermissionCallback);
        context.startActivity(intent);
        if (context instanceof Activity) {
            ((Activity) context).overridePendingTransition(0, 0);
        }
    }


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Intent intent = getIntent();
        key = intent.getStringExtra("key");
        tipInfo = intent.getParcelableExtra("tipInfo");
        permission = intent.getStringArrayExtra("permission");
        isShowTip = intent.getBooleanExtra("isShowTip", false);

        ActivityCompat.requestPermissions(this, permission, PERMISSION_REQUEST_CODE);
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        //部分厂商手机系统返回授权成功时，厂商可以拒绝权限，所以要用PermissionChecker二次判断
        if (requestCode == PERMISSION_REQUEST_CODE && isGranted(grantResults)
                && hasPermission(this, permissions)) {
            OnPermissionCallback listener = listenersMap.remove(key);
            if (listener != null) {
                listener.onSuccess(permission);
            }
            finish();

        } else if (isShowTip) {
            showMissingPermissionDialog();
        } else { //不需要提示用户
            permissionsRefused();
        }
    }

    private void showMissingPermissionDialog() {
        if (tipInfo == null) {
            tipInfo = PermissionTipInfo.newInstance();
        }

        new AlertDialog.Builder(this).setTitle(tipInfo.getTitle()).setMessage(tipInfo.getMessage())
                .setNegativeButton(tipInfo.getCancelText(), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        permissionsRefused();
                    }
                })
                .setPositiveButton(tipInfo.getOkText(), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                        intent.setData(Uri.parse("package:" + getPackageName()));
                        startActivityForResult(intent, REQUEST_SETTING_CODE);

                    }
                }).setCancelable(false).create().show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        //去设置页面设置完成之后再次请求看是否已打开权限
        if (requestCode == REQUEST_SETTING_CODE) {
            ActivityCompat.requestPermissions(this, permission, PERMISSION_REQUEST_CODE);
        }
    }

    public static boolean hasPermission(Context context, String... permissions) {
        if (permissions.length == 0) {
            return false;
        }
        for (String per : permissions) {
            int result = PermissionChecker.checkSelfPermission(context, per);
            if (result != PermissionChecker.PERMISSION_GRANTED) {
                return false;
            }
        }
        return true;
    }

    //
    private void permissionsRefused() {
        OnPermissionCallback listener = listenersMap.remove(key);
        if (listener != null) {
            listener.onRefused(permission);
        }
        finish();
    }

    private boolean isGranted(@NonNull int... grantResult) {

        if (grantResult.length == 0) {
            return false;
        }
        for (int result : grantResult) {
            if (result != PackageManager.PERMISSION_GRANTED) {
                return false;
            }
        }
        return true;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        listenersMap.remove(key);
    }

    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(0, 0);
    }

    /***
     *@date 创建时间 2018/8/18 21:37
     *@author 作者: W.YuLong
     *@description
     */
    public static class PermissionTipInfo implements Parcelable {
        private String title = "温馨提示";
        private String message = "当前应用缺少必要权限。\n \n 请点击 \"设置\"-\"权限\"-打开所需权限。";
        private String cancelText = "取消";
        private String okText = "去设置";

        public PermissionTipInfo(String title, String message, String cancelText, String okText) {
            this.title = title;
            this.message = message;
            this.cancelText = cancelText;
            this.okText = okText;
        }

        public PermissionTipInfo(){
        }


        public static PermissionTipInfo newInstance() {
            return new PermissionTipInfo();
        }

        public String getTitle() {
            return title;
        }

        public PermissionTipInfo setTitle(String title) {
            this.title = title;
            return this;
        }

        public String getMessage() {
            return message;
        }

        public PermissionTipInfo setMessage(String message) {
            this.message = message;
            return this;
        }

        public String getCancelText() {
            return cancelText;
        }

        public PermissionTipInfo setCancelText(String cancelText) {
            this.cancelText = cancelText;
            return this;
        }

        public String getOkText() {
            return okText;
        }

        public PermissionTipInfo setOkText(String okText) {
            this.okText = okText;
            return this;
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(this.title);
            dest.writeString(this.message);
            dest.writeString(this.cancelText);
            dest.writeString(this.okText);
        }

        protected PermissionTipInfo(Parcel in) {
            this.title = in.readString();
            this.message = in.readString();
            this.cancelText = in.readString();
            this.okText = in.readString();
        }

        public static final Creator<PermissionTipInfo> CREATOR = new Creator<PermissionTipInfo>() {
            @Override
            public PermissionTipInfo createFromParcel(Parcel source) {
                return new PermissionTipInfo(source);
            }

            @Override
            public PermissionTipInfo[] newArray(int size) {
                return new PermissionTipInfo[size];
            }
        };
    }


    /***
     *@date 创建时间 2018/8/18 21:27
     *@author 作者: W.YuLong
     *@description
     */
    public static class OnPermissionCallbackImpl implements OnPermissionCallback {
        @Override
        public void onSuccess(String[] permission) {

        }

        @Override
        public void onRefused(String[] permission) {

        }
    }

    /***
     *@date 创建时间 2018/8/18 21:27
     *@author 作者: W.YuLong
     *@description
     */
    public interface OnPermissionCallback {
        void onSuccess(String[] permission);

        void onRefused(String[] permission);
    }
}
