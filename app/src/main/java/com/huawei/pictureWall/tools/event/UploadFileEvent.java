package com.huawei.pictureWall.tools.event;

/***
 * @date 创建时间 2018/10/15 21:17
 * @author 作者: W.YuLong
 * @description
 */
public class UploadFileEvent {
    private String fileName;
    private String url;

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
