package com.huawei.pictureWall.tools.util;

import android.annotation.TargetApi;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.os.Build;
import android.text.TextUtils;

import com.huawei.pictureWall.R;
import com.huawei.pictureWall.base.APP;
import com.huawei.pictureWall.tools.singleton.SPSingleton;
import com.huawei.pictureWall.ui.SettingActivity;

import java.io.File;

/***
 * @date 创建时间 2018/9/12 22:25
 * @author 作者: W.YuLong
 * @description
 */
public class ImageMergeUtil {


    public static Bitmap createBitmap(String filePath) {
        int outHeight;
        int outWidth;
        float imageRate;
        float width = SPSingleton.get().getInt(SPDefine.KEY_PhotoWidth, 100);
        float height = SPSingleton.get().getInt(SPDefine.KEY_PhotoHeight, 150);
        float radio;

//        Bitmap srcBitmap = BitmapFactory.decodeFile(filePath); //rotateImage();
        Bitmap srcBitmap = ImageLoadUtil.loadBitmapFromFile(new File(filePath), 3000);

        imageRate = (float) srcBitmap.getWidth() / srcBitmap.getHeight();
        if (srcBitmap.getWidth() <= srcBitmap.getHeight()) {
            radio = width / height;
        } else {
            radio = height / width;
        }


        //横版图片过宽
        if (imageRate > radio) {
            outHeight = srcBitmap.getHeight();
            outWidth = (int) (outHeight * radio);
        } else {
            //横版图片过高
            outWidth = srcBitmap.getWidth();
            outHeight = (int) (outWidth / radio);
        }

        Bitmap outBitmap = Bitmap.createBitmap(outWidth, outHeight, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(outBitmap);
        canvas.drawColor(Color.WHITE);

        float paddingSize = 0;
        if (SPSingleton.get().getBoolean(SPDefine.KEY_CropCorrectPrint, true)) {
            int correctValue;
            //表示是横版照片
            if (srcBitmap.getWidth() > srcBitmap.getHeight()) {
                correctValue = SPSingleton.get().getInt(SPDefine.KEY_horizontalPrintCorrect, SettingActivity.DEFAULT_PRINT_CORRECT_VALUE);
            } else {
                correctValue = SPSingleton.get().getInt(SPDefine.KEY_VerticalPrintCorrect, SettingActivity.DEFAULT_PRINT_CORRECT_VALUE);
            }
            float rate = (float) correctValue / 1000;

            paddingSize = Math.min(outHeight, outWidth) * rate;
        }
//        float logoLeftMargin = srcBitmap.getWidth() * 0.045f;
        float logoHeight = Math.max(outHeight, outWidth) * 0.05f;
        if (outHeight < outWidth){
            logoHeight *= 0.85f;
        }

        drawBottomLogo(canvas, logoHeight, paddingSize);

        int drawHeight = (int) (outHeight - logoHeight - paddingSize);

        int left = 0, top = 0;
        //不管是过宽还是过高，这二种中会有一个的值是 0
        left = (srcBitmap.getWidth() - outBitmap.getWidth()) / 2;
        top = (srcBitmap.getHeight() - outBitmap.getHeight()) / 2;


        Bitmap drawMainBitmap = Bitmap.createBitmap(srcBitmap, left, top, canvas.getWidth(), drawHeight);
        CLog.d(String.format("drawWidth = %d, drawHeight = %d", canvas.getWidth(), drawHeight));
        srcBitmap.recycle();

        canvas.drawBitmap(drawMainBitmap, 0, 0, null);
        ImageLoadUtil.writeBitmapToFile(outBitmap, PathUtil.getRootPath() + "mergePic.jpg");
        return outBitmap;
    }


    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    private static void drawBottomLogo(Canvas canvas, float logoHeight, float paddingSize) {
        float textSize = logoHeight * 0.45f;
        int imageHeight = (int) (logoHeight * 0.37f);
        int canvasHeight = canvas.getHeight();
        int drawWidth = (int) (canvas.getWidth() * 0.35f);

        int leftValue = SPSingleton.get().getInt(SPDefine.KEY_LogoLeftMargin, 0);
        float leftMarginRate = (float) leftValue / 1000;

        float logoLeftMargin = Math.min(canvas.getWidth(), canvas.getHeight()) * leftMarginRate;


        Bitmap leftBitmap = decodeResBitmap(R.drawable.huawei_logo, drawWidth, imageHeight);


        float logoTop = canvasHeight - (logoHeight + imageHeight * 0.63f) / 2 - paddingSize;
        canvas.drawBitmap(leftBitmap, logoLeftMargin, logoTop, null);

        float leftTextWidth = leftBitmap.getWidth(); //paint.measureText("HUAWEI · ");


        Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
        paint.setTextSize(textSize);
        paint.setFakeBoldText(true);
        paint.setColor(0xff595757);
        paint.setHinting(Paint.HINTING_ON);
        paint.setSubpixelText(true);

        paint.setStyle(Paint.Style.FILL_AND_STROKE);
        paint.setLetterSpacing(0.1f);
        paint.setStrokeWidth(textSize * 0.0f);

        try {
            Typeface textFont = Typeface.createFromFile(PathUtil.getRootPath() + "font.ttf");
            if (textFont != null) {
                paint.setTypeface(textFont);
            }
        } catch (Exception e) {
            e.printStackTrace();
            CLog.e("没有字体文件");
        }
        String logoText = SPSingleton.get().getString(SPDefine.KEY_WaterMarkLogoText, "");
        if (TextUtils.isEmpty(logoText)) {
            logoText = "智能生活馆 上海爱琴海";
        }
        logoText = "  " + logoText;

        float top = canvasHeight - (logoHeight - textSize) / 2 - paddingSize;
        canvas.drawText(logoText, logoLeftMargin + leftTextWidth, top, paint);


//        canvas.drawBitmap(rightBitmap, width - rightBitmap.getWidth() - logoPadding, top, null);
    }


    private static Bitmap decodeResBitmap(int resId, float width, float height) {
        Bitmap bitmap = BitmapFactory.decodeResource(APP.get().getResources(), resId);
        float rate = (float) bitmap.getWidth() / bitmap.getHeight();

        float srcRate = width / height;

        float scale;
        //过宽
        if (rate > srcRate) {
            scale = width / bitmap.getWidth();
        } else { //过高
            scale = height / bitmap.getHeight();
        }

        Matrix matrix = new Matrix();
        matrix.setScale(scale, scale);
        Bitmap outBitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
        bitmap.recycle();
        return outBitmap;
    }


}
