package com.huawei.pictureWall.tools.util;

import android.database.Cursor;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;

import com.huawei.pictureWall.base.APP;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;

/***
 *@date 创建时间 2018/10/15 22:36
 *@author 作者: W.YuLong
 *@description
 */
public class PathUtil {


    private static SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");

    private static String userPath;

    public static String getReceivePath() {
        if (userPath == null) {
            userPath = getRootPath() + "receive/";
            checkOrMakeDir(userPath);
        }
        return userPath;
    }


    private static String guidePath;

    public static String getGuidePicturePath() {
        if (guidePath == null) {
            guidePath = getRootPath() + "guide.jpg";
        }
        return guidePath;
    }


//
//    private static String compressPath;
//    public static String getCompressPath(){
//        if (compressPath == null){
//            compressPath = getRootPath() + "Compress";
//            checkOrMakeDir(compressPath);
//        }
//        return compressPath;
//    }

    private static String rootPath;

    public static String getRootPath() {
        if (rootPath == null) {
            rootPath = Environment.getExternalStorageDirectory().getPath() + "/APhotoWall/";
            checkOrMakeDir(rootPath);
        }
        return rootPath;
    }


    private static String logPath;

    public static String getLogPath() {
        if (logPath == null) {
            logPath = getRootPath() + "Log/" + sdf.format(Calendar.getInstance().getTime()) + "/";
            checkOrMakeDir(logPath);
        }
        return logPath;
    }

//    private static File logFile;
//    public static File getLogFile(){
//        if (logFile == null){
//            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd_HH_mm_ss");
//            logFile = new File(getLogPath() + "log_"+sdf.format(Calendar.getInstance().getTime()) +".txt");
//        }
//        return logFile;
//    }

    private static void checkOrMakeDir(String path) {
        File file = new File(path);
        if (!file.exists()) {
            boolean flag = file.mkdir();
            if (!flag) {
                CLog.e("新建文件夹失败:" + path);
            }
        }
    }

    private static String demoPhotoPath;

    public static String getDemoPhotoPath() {
        if (demoPhotoPath == null) {
            demoPhotoPath = getRootPath() + "Demo/";
            checkOrMakeDir(demoPhotoPath);
        }
        return demoPhotoPath;
    }


    public static List<File> loadFileFromSDCard(boolean isDesc) {
        List<File> list = new ArrayList<>();
        File dirFile = new File(getRootPath());
        if (dirFile.exists()) {
            File[] listFile = dirFile.listFiles(new PhotoFileFilter());


            if (listFile != null) {
                List<File> sourceFileList = Arrays.asList(listFile);
                Collections.sort(sourceFileList, new FileComparator(false));

                int max = 30;
                int i = 0;
                for (File f : sourceFileList) {
                    if (i < max) {
                        list.add(f);
                    }
                    i++;
                }
                Collections.sort(list, new FileComparator(isDesc));
            }
        }
        return list;
    }

    /*获取相册中最新拍照的照片*/
    public static String getRecentlyPhotoPath() {
        String searchPath = MediaStore.Files.FileColumns.DATA + " LIKE '%" + "/DCIM/Camera/" + "%' ";
        Uri uri = MediaStore.Files.getContentUri("external");
        Cursor cursor = APP.get().getContentResolver().query(uri, new String[]{MediaStore.Files.FileColumns.DATA, MediaStore.Files.FileColumns.SIZE}, searchPath, null, MediaStore.Files.FileColumns.DATE_ADDED + " DESC");
        String filePath = "";
        long fileSize;

        if (cursor != null && cursor.moveToFirst()) {
            filePath = cursor.getString(cursor.getColumnIndex(MediaStore.Files.FileColumns.DATA));
            fileSize = cursor.getLong(cursor.getColumnIndex(MediaStore.Files.FileColumns.SIZE));
            CLog.d("图片大小：" + fileSize);
        }

        if (!cursor.isClosed()) {
            cursor.close();
        }
        return filePath;
    }
}
