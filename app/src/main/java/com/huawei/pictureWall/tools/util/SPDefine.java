package com.huawei.pictureWall.tools.util;

/***
 * @date 创建时间 2018/8/21 21:35
 * @author 作者: W.YuLong
 * @description
 */
public class SPDefine {


    public static final String KEY_IPAddress = "IPAddress";

    public static final String KEY_UDP_Port = "UDP_Port";
    public static final String KEY_TCP_Port = "TCP_Port";
    public static final String KEY_RowCount = "RowCount";
    public static final String KEY_TCPPort = "TCPPort";
    public static final String KEY_ShakeSensor = "ShakeSensor";
    public static final String KEY_UDPPort = "UDPPort";

    public static final String KEY_PictureDialogShowTime = "PictureDialogShowTime";

    public static final String KEY_PhotoWidth = "PhotoWidth";
    public static final String KEY_PhotoHeight = "PhotoHeight";
    public static final String KEY_ScrollSpeed = "ScrollSpeed";
    public static final String KEY_ShowGuideDialog = "ShowGuideDialog";
    public static final String KEY_DeleteExpiredFile = "DeleteExpiredFile";
    public static final String KEY_PrintMode = "PrintMode";

    //是否开启打印水印纠偏
    public static final String KEY_CropCorrectPrint = "CropCorrectPrint";
    //打印水晶纠偏的参数
    public static final String KEY_VerticalPrintCorrect = "PrintCorrect";
    public static final String KEY_horizontalPrintCorrect = "horizontalPrintCorrect";

    public static final String KEY_WaterMarkLogoText = "WaterMarkLogoText";
    public static final String KEY_LogoLeftMargin = "LogoLeftMargin";
//    public static final String KEY_ = "";
//    public static final String KEY_ = "";


}
