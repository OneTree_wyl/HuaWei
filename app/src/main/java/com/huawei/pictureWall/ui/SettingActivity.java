package com.huawei.pictureWall.ui;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.TextView;

import com.huawei.pictureWall.R;
import com.huawei.pictureWall.base.BaseRootActivity;
import com.huawei.pictureWall.tools.impl.OnSeekBarChangeListenerImpl;
import com.huawei.pictureWall.tools.impl.TextWatcherImpl;
import com.huawei.pictureWall.tools.singleton.SPSingleton;
import com.huawei.pictureWall.tools.util.SPDefine;

import butterknife.BindView;
import butterknife.ButterKnife;

/***
 * @date 创建时间 2018/8/21 21:18
 * @author 作者: W.YuLong
 * @description
 */
public class SettingActivity extends BaseRootActivity {
    public static final int DEFAULT_PRINT_CORRECT_VALUE = 33;

    @BindView(R.id.setting_sensor_SeekBar) SeekBar sensorSeekBar;
    @BindView(R.id.setting_speed_SeekBar) SeekBar speedSeekBar;

    @BindView(R.id.setting_sensor_value_TextView) TextView sensorValueTextView;
    @BindView(R.id.setting_speed_value_TextView) TextView speedValueTextView;

    @BindView(R.id.setting_photo_wall_row_count_EditText) EditText rowCountEditText;
    @BindView(R.id.setting_photo_wall_dialog_show_time_EditText) EditText dialogShowTimeEditText;
    @BindView(R.id.setting_photo_width_EditText) EditText widthEditText;
    @BindView(R.id.setting_photo_height_EditText) EditText heightEditText;
    @BindView(R.id.setting_show_guide_dialog_time_EditText) EditText guideEditText;

    @BindView(R.id.setting_print_correct_vertical_EditText) EditText verticalCorrectEditText;
    @BindView(R.id.setting_print_correct_horizontal_EditText) EditText horizontalCorrectEditText;
    @BindView(R.id.setting_photo_left_margin_EditText) EditText leftMarginEditText;

    @BindView(R.id.setting_delete_expired_file_EditText) EditText expiredTimeEditText;
    @BindView(R.id.setting_print_mode_CheckBox) CheckBox printModeCheckBox;

    @BindView(R.id.setting_print_crop_CheckBox) CheckBox cropCorrectCheckBox;

    @BindView(R.id.setting_ip_address_EditText) EditText ipAddressEditText;
    @BindView(R.id.setting_water_mark_logo_EditText) EditText logoTextEditText;

    private final int TAG_ROW_COUNT = 1;
    private final int TAG_DIALOG_SHOW_TIME = 2;
    private final int TAG_PHOTO_WIDTH = 3;
    private final int TAG_PHOTO_HEIGHT = 4;

    private final int TAG_SENSOR = 5;

    private final int TAG_SCROLL_SPEED = 6;

    private final int TAG_SHOW_GUIDE_IMAGE = 7;
    private final int TAG_DELETE_EXPIRED_FILE = 8;
    private final int TAG_IP_ADDRESS = 9;

    private final int TAG_PRINT_CORRECT_VERTICAL_VALUE = 10;
    private final int TAG_PRINT_CORRECT_HORIZONTAL_VALUE = 11;
    private final int TAG_LOGO_TEXT = 12;
    private final int TAG_LEFT_MARGIN = 13;


    /*灵敏度的默认差值*/
    private final int sensorOffsetValue = 10;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settint_layout);
        ButterKnife.bind(this);

        initUI();

        rowCountEditText.addTextChangedListener(getTextWatcher(TAG_ROW_COUNT));
        dialogShowTimeEditText.addTextChangedListener(getTextWatcher(TAG_DIALOG_SHOW_TIME));

        widthEditText.addTextChangedListener(getTextWatcher(TAG_PHOTO_WIDTH));
        heightEditText.addTextChangedListener(getTextWatcher(TAG_PHOTO_HEIGHT));
        leftMarginEditText.addTextChangedListener(getTextWatcher(TAG_LEFT_MARGIN));

        guideEditText.addTextChangedListener(getTextWatcher(TAG_SHOW_GUIDE_IMAGE));
        expiredTimeEditText.addTextChangedListener(getTextWatcher(TAG_DELETE_EXPIRED_FILE));

        ipAddressEditText.addTextChangedListener(getTextWatcher(TAG_IP_ADDRESS));
        verticalCorrectEditText.addTextChangedListener(getTextWatcher(TAG_PRINT_CORRECT_VERTICAL_VALUE));
        horizontalCorrectEditText.addTextChangedListener(getTextWatcher(TAG_PRINT_CORRECT_HORIZONTAL_VALUE));
        logoTextEditText.addTextChangedListener(getTextWatcher(TAG_LOGO_TEXT));

        sensorSeekBar.setOnSeekBarChangeListener(getOnSeekBarChangeListener(TAG_SENSOR));
        speedSeekBar.setOnSeekBarChangeListener(getOnSeekBarChangeListener(TAG_SCROLL_SPEED));
//        printCorrectSeekBar.setOnSeekBarChangeListener(getOnSeekBarChangeListener(TAG_PRINT_CORRECT_VERTICAL_VALUE));


        printModeCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                SPSingleton.get().putBoolean(SPDefine.KEY_PrintMode, isChecked);
            }
        });
        cropCorrectCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                SPSingleton.get().putBoolean(SPDefine.KEY_CropCorrectPrint, isChecked);
            }
        });


    }


    private SeekBar.OnSeekBarChangeListener getOnSeekBarChangeListener(int tag) {
        return new OnSeekBarChangeListenerImpl() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                switch (tag) {
                    case TAG_SENSOR:
                        sensorValueTextView.setText(String.valueOf(progress + sensorOffsetValue));
                        SPSingleton.get().putInt(SPDefine.KEY_ShakeSensor, progress + sensorOffsetValue);
                        break;
                    case TAG_SCROLL_SPEED:
                        speedValueTextView.setText(String.valueOf(progress));
                        SPSingleton.get().putInt(SPDefine.KEY_ScrollSpeed, progress);
                        break;

                }
            }
        };
    }

    private void initUI() {
        rowCountEditText.setText("" + SPSingleton.get().getInt(SPDefine.KEY_RowCount, 4));

        dialogShowTimeEditText.setText(SPSingleton.get().getInt(SPDefine.KEY_PictureDialogShowTime, 10) + "");

        int sensorValue = SPSingleton.get().getInt(SPDefine.KEY_ShakeSensor, 20);
        sensorValueTextView.setText("" + sensorValue);
        sensorSeekBar.setProgress(sensorValue - sensorOffsetValue);

        int photoWidth = SPSingleton.get().getInt(SPDefine.KEY_PhotoWidth, 0);
        int photoHeight = SPSingleton.get().getInt(SPDefine.KEY_PhotoHeight, 0);
        if (photoWidth > 0) {
            widthEditText.setText(photoWidth + "");
        }
        if (photoHeight > 0) {
            heightEditText.setText(photoHeight + "");
        }

        int speed = SPSingleton.get().getInt(SPDefine.KEY_ScrollSpeed, 15);
        speedSeekBar.setProgress(speed);
        speedValueTextView.setText("" + speed);

        guideEditText.setText(SPSingleton.get().getInt(SPDefine.KEY_ShowGuideDialog, 300) + "");

        expiredTimeEditText.setText(SPSingleton.get().getLong(SPDefine.KEY_DeleteExpiredFile, 5 * 60) + "");
        printModeCheckBox.setChecked(SPSingleton.get().getBoolean(SPDefine.KEY_PrintMode, true));

        cropCorrectCheckBox.setChecked(SPSingleton.get().getBoolean(SPDefine.KEY_CropCorrectPrint, true));

        ipAddressEditText.setText(SPSingleton.get().getString(SPDefine.KEY_IPAddress, ""));


        int printCorrect = SPSingleton.get().getInt(SPDefine.KEY_VerticalPrintCorrect, DEFAULT_PRINT_CORRECT_VALUE);
        verticalCorrectEditText.setText(printCorrect + "");
        horizontalCorrectEditText.setText("" + SPSingleton.get().getInt(SPDefine.KEY_horizontalPrintCorrect, DEFAULT_PRINT_CORRECT_VALUE));

        leftMarginEditText.setText("" + SPSingleton.get().getInt(SPDefine.KEY_LogoLeftMargin, 0));

        logoTextEditText.setText(SPSingleton.get().getString(SPDefine.KEY_WaterMarkLogoText, ""));
    }


    private TextWatcher getTextWatcher(int tag) {
        return new TextWatcherImpl() {
            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() > 0) {
                    switch (tag) {
                        case TAG_ROW_COUNT:
                            int count = Integer.parseInt(s.toString());
                            SPSingleton.get().putInt(SPDefine.KEY_RowCount, count);
                            break;
                        case TAG_DIALOG_SHOW_TIME:
                            int dialogTime = Integer.parseInt(s.toString());
                            SPSingleton.get().putInt(SPDefine.KEY_PictureDialogShowTime, dialogTime);
                            break;
                        case TAG_PHOTO_WIDTH:
                            int width = Integer.parseInt(s.toString());
                            SPSingleton.get().putInt(SPDefine.KEY_PhotoWidth, width);
                            break;
                        case TAG_PHOTO_HEIGHT:
                            int height = Integer.parseInt(s.toString());
                            SPSingleton.get().putInt(SPDefine.KEY_PhotoHeight, height);
                            break;
                        // 未做操作显示引导图的时间设置
                        case TAG_SHOW_GUIDE_IMAGE:
                            int noActionTime = Integer.parseInt(s.toString());
                            SPSingleton.get().putInt(SPDefine.KEY_ShowGuideDialog, noActionTime);
                            break;
                        case TAG_DELETE_EXPIRED_FILE:
                            long expiredTime = Long.parseLong(s.toString());
                            SPSingleton.get().putLong(SPDefine.KEY_DeleteExpiredFile, expiredTime);
                            break;
                        case TAG_IP_ADDRESS:
                            SPSingleton.get().putString(SPDefine.KEY_IPAddress, s.toString());
                            break;
                        case TAG_PRINT_CORRECT_VERTICAL_VALUE:
                            int verticalValue = Integer.parseInt(s.toString());
                            SPSingleton.get().putInt(SPDefine.KEY_VerticalPrintCorrect, verticalValue);
                            break;
                        case TAG_PRINT_CORRECT_HORIZONTAL_VALUE:
                            int value = Integer.parseInt(s.toString());
                            SPSingleton.get().putInt(SPDefine.KEY_horizontalPrintCorrect, value);
                            break;
                        case TAG_LOGO_TEXT:
                            SPSingleton.get().putString(SPDefine.KEY_WaterMarkLogoText, s.toString());
                            break;
                        case TAG_LEFT_MARGIN:
                            int leftMargin= Integer.parseInt(s.toString());
                            SPSingleton.get().putInt(SPDefine.KEY_LogoLeftMargin, leftMargin);
                            break;
                    }
                } else {
                    switch (tag) {
                        case TAG_IP_ADDRESS:
                            SPSingleton.get().putString(SPDefine.KEY_IPAddress, s.toString());
                            break;
                    }
                }

            }
        };
    }


}
