package com.huawei.pictureWall.ui.dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.huawei.pictureWall.R;
import com.huawei.pictureWall.tools.singleton.SPSingleton;
import com.huawei.pictureWall.tools.util.APPUtil;
import com.huawei.pictureWall.tools.util.DimenSizeUtil;
import com.huawei.pictureWall.tools.util.PathUtil;
import com.huawei.pictureWall.tools.util.PictureLoadUtil;
import com.huawei.pictureWall.tools.util.SPDefine;
import com.huawei.pictureWall.tools.util.ToastUtil;
import com.huawei.pictureWall.tools.util.UIConfig;
import com.huawei.pictureWall.ui.adapter.ShowAllPictureAdapter;

import butterknife.BindView;
import butterknife.ButterKnife;

/***
 * @date 创建时间 2018/8/22 22:31
 * @author 作者: W.YuLong
 * @description 显示所有的图片对话框
 */
public class ShowAllPictureDialog extends BaseDialog implements View.OnClickListener, OnAllItemSelectListener {
    @BindView(R.id.show_all_select_ImageView) ImageView allImageView;
    @BindView(R.id.show_all_all_select_layout) LinearLayout selectContainerLayout;
    @BindView(R.id.show_all_edit_Button) Button editButton;

    @BindView(R.id.show_all_bottom_container_layout) RelativeLayout bottomContainerLayout;
    @BindView(R.id.show_all_data_recyclerView) RecyclerView dataRecyclerView;
    @BindView(R.id.show_all_delete_ImageView) ImageView deleteImageView;
    @BindView(R.id.show_all_back_ImageView) ImageView backImageView;
    @BindView(R.id.show_all_root_layout) RelativeLayout rootLayout;


    private ShowAllPictureAdapter allPictureAdapter;

    public ShowAllPictureDialog(@NonNull Context context) {
        super(context, R.style.DialogTheme);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_show_all_picture_layout);
        ButterKnife.bind(this);

        GridLayoutManager layoutManager = new GridLayoutManager(getContext(), 8, GridLayoutManager.VERTICAL, false);
        dataRecyclerView.setLayoutManager(layoutManager);

        setViewListeners();

        rootLayout.setPadding(0, 0, 0, DimenSizeUtil.getShowAllImageHeight() + APPUtil.dp2px(5));

        int bottomSize = (int) (0.06f * Math.min(APPUtil.getScreenHeight(), APPUtil.getScreenWidth()));
        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) bottomContainerLayout.getLayoutParams();
        params.height = bottomSize;
        bottomContainerLayout.setLayoutParams(params);

        bottomContainerLayout.setPadding(bottomSize, 0, bottomSize, 0);
        setLayoutParams(backImageView, bottomSize);


        setAutoDismissTime(SPSingleton.get().getInt(SPDefine.KEY_PictureDialogShowTime, 10));

        allPictureAdapter = new ShowAllPictureAdapter();
        allPictureAdapter.setOnAllItemSelectListener(this);
        dataRecyclerView.setAdapter(allPictureAdapter);

        allPictureAdapter.setData(PictureLoadUtil.initFiles(30, PathUtil.getDemoPhotoPath()));

        configDialog(UIConfig.ShowAllDialogWidthMutiple);

    }

    @Override
    protected void configDialog(float widthMutiple) {
        WindowManager.LayoutParams wl = getWindow().getAttributes();
        wl.gravity = Gravity.BOTTOM | Gravity.CENTER_HORIZONTAL;
        getWindow().setWindowAnimations(R.style.centerDialogWindowAnim);
        getWindow().setAttributes(wl);
        getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        if (APPUtil.getScreenWidth() > APPUtil.getScreenHeight()) {
            getWindow().setLayout((int) (APPUtil.getScreenWidth() * UIConfig.ShowAllDialogWidthMutiple),
                    (int) (APPUtil.getScreenHeight() * 0.9f));
        } else {
            getWindow().setLayout((int) (APPUtil.getScreenHeight() * UIConfig.ShowAllDialogWidthMutiple),
                    (int) (APPUtil.getScreenWidth() * 0.9f));
        }

    }

    private void setViewListeners() {
        deleteImageView.setOnClickListener(this);
        backImageView.setOnClickListener(this);
        selectContainerLayout.setOnClickListener(this);
        allImageView.setOnClickListener(this);
        editButton.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        if (v == deleteImageView) {
            CommAlertDialog.with(getContext()).setMessage("确定删除所选择的照片吗？")
                    .setLeftText("删除").setRightText("取消")
                    .setClickDismiss(true).setOnViewClickListener(new OnDialogViewClickListener() {
                @Override
                public void onViewClick(Dialog dialog, View v, int tag) {
                    if (tag == CommAlertDialog.TAG_CLICK_LEFT) {
                        ToastUtil.showToastShort("点击删除");
                        setCurrentEditModeShow(false);
                        ShowAllPictureDialog.this.dismiss();
                    }
                }
            }).create().show();

        } else if (v == backImageView) {
            if (!allPictureAdapter.cancelSelectMode()) {
                dismiss();
            } else {
                setCurrentEditModeShow(false);
            }
        } else if (v == editButton) {
            allPictureAdapter.setSelectMode(true);

            setCurrentEditModeShow(true);
        } else if (v == selectContainerLayout) {
            allImageView.performClick();
        } else if (v == allImageView) {
            if (allImageView.isSelected()) {
                allImageView.setSelected(false);
            } else {
                allImageView.setSelected(true);
            }
            allPictureAdapter.setAllSelectAction(allImageView.isSelected());
        }
    }


    private void setCurrentEditModeShow(boolean isEditMode) {
        if (isEditMode) {
            editButton.setVisibility(View.INVISIBLE);
            selectContainerLayout.setVisibility(View.VISIBLE);
        } else {
            editButton.setVisibility(View.VISIBLE);
            selectContainerLayout.setVisibility(View.INVISIBLE);
        }

    }

    @Override
    public void isOnItemAllSelected(boolean isAllSelected) {
        allImageView.setSelected(isAllSelected);
    }
}
