package com.huawei.pictureWall.ui;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.ImageView;

import com.huawei.pictureWall.R;
import com.huawei.pictureWall.base.BaseRootActivity;
import com.huawei.pictureWall.tools.event.DialogDismissEvent;
import com.huawei.pictureWall.tools.event.OnReloadPictureEvent;
import com.huawei.pictureWall.tools.event.ReceiveTouchEvent;
import com.huawei.pictureWall.tools.impl.OnReloadPictureListener;
import com.huawei.pictureWall.tools.impl.OnUploadPictureListener;
import com.huawei.pictureWall.tools.singleton.SPSingleton;
import com.huawei.pictureWall.tools.upload.UploadFileUtil;
import com.huawei.pictureWall.tools.util.CLog;
import com.huawei.pictureWall.tools.util.DimenSizeUtil;
import com.huawei.pictureWall.tools.util.PathUtil;
import com.huawei.pictureWall.tools.util.PictureLoadUtil;
import com.huawei.pictureWall.tools.util.SPDefine;
import com.huawei.pictureWall.tools.util.ValueConstant;
import com.huawei.pictureWall.tools.widget.view.CustomRecyclerView;
import com.huawei.pictureWall.transfer.service.ReceiveFileService;
import com.huawei.pictureWall.ui.adapter.MainPictureAdapter;
import com.huawei.pictureWall.ui.dialog.ShowAllPictureDialog;
import com.huawei.pictureWall.ui.dialog.ShowFullImageDialog;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.io.File;

import butterknife.BindView;
import butterknife.ButterKnife;

/***
 * @date 创建时间 2018/8/20 21:40
 * @author 作者: W.YuLong
 * @description 照片墙的主页面
 */
public class PhotoWallActivity extends BaseRootActivity implements View.OnClickListener {
    @BindView(R.id.photo_wall_data_RecyclerView) CustomRecyclerView dataRecyclerView;
    @BindView(R.id.photo_wall_show_all_ImageView) ImageView showAllImageView;
    private MainPictureAdapter pictureAdapter;
    private StaggeredGridLayoutManager customLayoutManager;
    public PhotoReceiveBroadcast photoReceiveBroadcast;

    public static final int MSG_SCROLL_TAG = 1;
    public static final int MSG_SHOW_GUIDE_DIALOG = 2;
    public static final int MSG_DELETE_EXPIRED_FILE = 3;

    private int scrollSpeedTime = 20;
    private int showGuideTime = 5 * 1000;

    // 接收照片的有效期，默认5分钟
    private long deleteExpiredTime = 5 * 60;

    private ShowFullImageDialog guideDialog;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_photo_wall_layout);
        ButterKnife.bind(this);

        showAllImageView.setOnClickListener(this);

        FrameLayout.LayoutParams params = (FrameLayout.LayoutParams) showAllImageView.getLayoutParams();
        params.height = DimenSizeUtil.getShowAllImageHeight();
        params.width = (int) (params.height * 6.61f);
        showAllImageView.setLayoutParams(params);


        int rowCount = SPSingleton.get().getInt(SPDefine.KEY_RowCount, 4);
        customLayoutManager = new StaggeredGridLayoutManager(rowCount, StaggeredGridLayoutManager.VERTICAL);
        dataRecyclerView.setLayoutManager(customLayoutManager);

        // 滑动速度
        scrollSpeedTime = 2 * SPSingleton.get().getInt(SPDefine.KEY_ScrollSpeed, scrollSpeedTime);
        // 未操作的时长显示引导页
        showGuideTime = SPSingleton.get().getInt(SPDefine.KEY_ShowGuideDialog, 300) * 1000;
        // 接收的照片有效时间
        deleteExpiredTime = SPSingleton.get().getLong(SPDefine.KEY_DeleteExpiredFile, deleteExpiredTime);


        pictureAdapter = new MainPictureAdapter();
        dataRecyclerView.setAdapter(pictureAdapter);

        dataRecyclerView.post(new Runnable() {
            @Override
            public void run() {
                int itemImageWidth = dataRecyclerView.getWidth() / rowCount;
                pictureAdapter.setItemWidth(itemImageWidth);

                pictureAdapter.setData(PictureLoadUtil.initFiles(30, PathUtil.getDemoPhotoPath()));
                handler.sendEmptyMessage(MSG_SCROLL_TAG);

            }
        });

        photoReceiveBroadcast = new PhotoReceiveBroadcast();
        photoReceiveBroadcast.setOnReloadPictureListener(new OnReloadPictureListener() {
            @Override
            public void onReloadPicture() {
                reloadPicture(null);
            }
        });

        photoReceiveBroadcast.setOnUploadPictureListener(onUploadPictureListener);

        guideDialog = new ShowFullImageDialog(PhotoWallActivity.this);
        guideDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                handler.sendEmptyMessage(MSG_SCROLL_TAG);
                sendShowGuideMessage();
            }
        });

        hideSystemNavigationBar();

        EventBus.getDefault().register(this);

        Intent intent = new Intent(this, ReceiveFileService.class);
        startService(intent);
    }

    /*隐藏虚拟按键*/
    private void hideSystemNavigationBar() {
        if (Build.VERSION.SDK_INT > 11 && Build.VERSION.SDK_INT < 19) {
            View view = this.getWindow().getDecorView();
            view.setSystemUiVisibility(View.GONE);
        } else if (Build.VERSION.SDK_INT >= 19) {
            View decorView = getWindow().getDecorView();
            int uiOptions = View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                    | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                    | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                    | View.SYSTEM_UI_FLAG_FULLSCREEN
                    | View.SYSTEM_UI_FLAG_IMMERSIVE
                    | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION;
            decorView.setSystemUiVisibility(uiOptions);
        }
    }


    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        CLog.d("点击操作");
        sendShowGuideMessage();
        return super.dispatchTouchEvent(ev);
    }



    @Subscribe(threadMode = ThreadMode.MAIN)
    public void reloadPicture(OnReloadPictureEvent event){
        if (guideDialog != null && guideDialog.isShowing()) {
            guideDialog.dismiss();
        }

        pictureAdapter.setData(PictureLoadUtil.initFiles(30, PathUtil.getDemoPhotoPath()));
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onReceiveTouchEvent(ReceiveTouchEvent receiveTouchEvent){
        sendShowGuideMessage();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onReceiveDialogDismiss(DialogDismissEvent event){
        hideSystemNavigationBar();
    }

    OnUploadPictureListener onUploadPictureListener = new OnUploadPictureListener() {
        /*这个是接收到照片之后做上传照片的操作*/
        @Override
        public void doUploadPicture(File file) {

        UploadFileUtil uploadFileUtil = new UploadFileUtil();
        uploadFileUtil.doUploadFile(file);


        }
    };



    @Override
    protected void onDestroy() {
        super.onDestroy();
        Intent intent = new Intent(this, ReceiveFileService.class);
        intent.putExtra(ValueConstant.EXTRA_IS_STOP_SERVICE, true);
        startService(intent);

        EventBus.getDefault().unregister(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(photoReceiveBroadcast);

        handler.removeMessages(MSG_SCROLL_TAG);
        handler.removeMessages(MSG_SHOW_GUIDE_DIALOG);
        handler.removeMessages(MSG_DELETE_EXPIRED_FILE);

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
    }


    @Override
    protected void onResume() {
        super.onResume();
//        hideSystemNavigationBar();

        registerReceiver(photoReceiveBroadcast, new IntentFilter(ValueConstant.RECEIVE_PHOTO));

        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        if (pictureAdapter.getRealCount() > 0) {
            sendScrollMsg();
        }

        sendShowGuideMessage();
        sendDeleteExpiredFileMsg();
    }

    /*滚动的消息*/
    private void sendScrollMsg() {
        handler.removeMessages(MSG_SCROLL_TAG);
        handler.sendEmptyMessage(MSG_SCROLL_TAG);
    }

    /*发送删除过期照片的消息*/
    private void sendDeleteExpiredFileMsg() {
        handler.removeMessages(MSG_DELETE_EXPIRED_FILE);
        handler.sendEmptyMessageDelayed(MSG_DELETE_EXPIRED_FILE, 15000);
    }

    /*发送显示引导框的消息*/
    private void sendShowGuideMessage() {
        handler.removeMessages(MSG_SHOW_GUIDE_DIALOG);
        handler.sendEmptyMessageDelayed(MSG_SHOW_GUIDE_DIALOG, showGuideTime);
    }


    private Handler handler = new Handler(new Handler.Callback() {
        @Override
        public boolean handleMessage(Message msg) {
            if (msg.what == MSG_SCROLL_TAG) {
                dataRecyclerView.scrollBy(0, 1);

                handler.sendEmptyMessageDelayed(MSG_SCROLL_TAG, scrollSpeedTime);

            } else if (msg.what == MSG_SHOW_GUIDE_DIALOG) {
                if (guideDialog != null && !guideDialog.isShowing()) {
                    guideDialog.show();
                }
                handler.removeMessages(MSG_SCROLL_TAG);
            } else if (msg.what == MSG_DELETE_EXPIRED_FILE) {
                PictureLoadUtil.deleteExpiredImage(deleteExpiredTime * 1000);
            }
            return false;
        }
    });

    @Override
    public void onClick(View v) {
        if (v == showAllImageView) {
            ShowAllPictureDialog dialog = new ShowAllPictureDialog(this);
            dialog.show();
        }
    }


}
