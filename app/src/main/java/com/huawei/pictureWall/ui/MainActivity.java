package com.huawei.pictureWall.ui;

import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.Button;

import com.huawei.pictureWall.R;
import com.huawei.pictureWall.base.BaseRootActivity;
import com.huawei.pictureWall.tools.util.APPUtil;
import com.huawei.pictureWall.transfer.service.SenderFileService;

import butterknife.BindView;
import butterknife.ButterKnife;

/***
 * @date 创建时间 2018/8/23 23:42
 * @author 作者: W.YuLong
 * @description
 */
public class MainActivity extends BaseRootActivity implements View.OnClickListener {

    //    3840 2160
    @BindView(R.id.main_photo_wall_Button) Button photoWallButton;
    @BindView(R.id.main_take_photo_listener_Button) Button takePhotoListenerButton;
    @BindView(R.id.main_setting_Button) Button settingButton;
//    @BindView(R.id.main_upload_test_Button) Button uploadButton;
//    @BindView(R.id.main_info_TextView) TextView infoTextView;

    private OnepxReceiver onepxReceiver;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_layout);
        ButterKnife.bind(this);

        photoWallButton.setOnClickListener(this);
        takePhotoListenerButton.setOnClickListener(this);
        settingButton.setOnClickListener(this);
//        uploadButton.setOnClickListener(this);

        int width = APPUtil.getScreenWidth();
        int height = APPUtil.getScreenHeight();
        if (Math.min(width, height) > 2000) {
            takePhotoListenerButton.setVisibility(View.GONE);
        }
//        infoTextView.setText(String.format("width = %d, height = %d",
//                APPUtil.getScreenWidth(), APPUtil.getScreenHeight()));
    }

    @Override
    public void onClick(View v) {
        if (v == photoWallButton) {
            Intent intent = new Intent(this, PhotoWallActivity.class);
            startActivity(intent);
        } else if (v == takePhotoListenerButton) {
            Intent intent = new Intent(this, SenderFileService.class);
            startService(intent);

            if (onepxReceiver == null) {
                onepxReceiver = new OnepxReceiver();
            }

            IntentFilter intentFilter = new IntentFilter();
            intentFilter.addAction(Intent.ACTION_SCREEN_ON);
            intentFilter.addAction(Intent.ACTION_SCREEN_OFF);
            intentFilter.addAction(Intent.ACTION_USER_PRESENT);
            registerReceiver(onepxReceiver, intentFilter);

            moveTaskToBack(true);
        } else if (v == settingButton) {
            Intent intent = new Intent(this, SettingActivity.class);
            startActivity(intent);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (onepxReceiver != null) {
            unregisterReceiver(onepxReceiver);
        }
    }
}
