package com.huawei.pictureWall.ui.adapter;

import android.graphics.BitmapFactory;
import android.support.annotation.NonNull;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import com.huawei.pictureWall.R;
import com.huawei.pictureWall.base.BaseRecycleAdapter;
import com.huawei.pictureWall.base.BaseViewHolder;
import com.huawei.pictureWall.tools.util.ImageLoadUtil;
import com.huawei.pictureWall.tools.widget.view.RoundImageView;
import com.huawei.pictureWall.ui.dialog.PictureShowDialog;

import java.io.File;

/***
 * @date 创建时间 2018/8/16 23:29
 * @author 作者: W.YuLong
 * @description
 */
public class MainPictureAdapter extends BaseRecycleAdapter<File, MainPictureAdapter.HomePictureViewHolder> {

    @NonNull
    @Override
    public HomePictureViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new HomePictureViewHolder(parent);
    }


    private int itemWidth;

    public void setItemWidth(int itemWidth) {
        this.itemWidth = itemWidth;
        notifyDataSetChanged();
    }

    @Override
    public void onBindViewHolder(HomePictureViewHolder holder, int position) {
        File t = dataList.get(position % getRealCount());
        holder.initUIData(itemWidth, t);
        holder.imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PictureShowDialog showDialog = new PictureShowDialog(v.getContext());
                showDialog.show();

                showDialog.setShowImagePath(position % getRealCount(), dataList);
            }
        });
    }

    /***
     *@date 创建时间 2018/10/14 14:24
     *@author 作者: W.YuLong
     *@description
     */
    public static class HomePictureViewHolder extends BaseViewHolder {
        RoundImageView imageView;
        File file;

        public HomePictureViewHolder(ViewGroup viewGroup) {
            super(viewGroup, R.layout.viewholder_image_view_layout);
            imageView = itemView.findViewById(R.id.holder_image_View);
        }

        public <T> void initUIData(int itemWidth, T t) {
            file = (File) t;
//            int paddingSize = (int) (Math.max(APPUtil.getScreenWidth(), APPUtil.getScreenHeight())
//                    * 0.009f);
//            int paddingHorizontalSize = (int) (paddingSize * 1f);
//            itemView.setPadding(paddingHorizontalSize, paddingSize, paddingHorizontalSize, paddingSize);

//            RoundImageView.ShadowBGDrawable shadowBGDrawable = imageView.getShadowDrawable();
//            shadowBGDrawable.setShadowRadius((int) (imageView.getWidth() * 0.1f));
//            imageView.setShadowBg(shadowBGDrawable);

            loadImageRate(itemWidth, file);

        }


        private void loadImageRate(int width, File file) {
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;
            BitmapFactory.decodeFile(file.getAbsolutePath(), options);
            float rate = (float) options.outHeight / options.outWidth;

            FrameLayout.LayoutParams params = (FrameLayout.LayoutParams) imageView.getLayoutParams();
            params.width = width;
            params.height = (int) (width * rate);
            imageView.setLayoutParams(params);

            ImageLoadUtil.loadSmallBitmap(imageView, file, width);
            //设置圆角
//            imageView.setRectRoundRadius((int) (Math.max(params.width, params.height) * 0.04f));
        }


        @Override
        public <T> void initUIData(T t) {
        }
    }


    @Override
    public int getItemCount() {
        if (getRealCount() == 0) {
            return 0;
        }
        return Integer.MAX_VALUE;
    }
}
