package com.huawei.pictureWall.ui.dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.NonNull;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.RelativeLayout;

import com.huawei.pictureWall.R;
import com.huawei.pictureWall.tools.event.DialogDismissEvent;
import com.huawei.pictureWall.tools.event.ReceiveTouchEvent;
import com.huawei.pictureWall.tools.util.APPUtil;

import org.greenrobot.eventbus.EventBus;

/***
 * @date 创建时间 2018/8/23 00:01
 * @author 作者: W.YuLong
 * @description
 */
public class BaseDialog extends Dialog {

    public BaseDialog(@NonNull Context context) {
        super(context);
    }

    public BaseDialog(@NonNull Context context, int themeResId) {
        super(context, themeResId);
    }

    protected int showTime = 15;

    private Handler handler = new Handler(new Handler.Callback() {
        @Override
        public boolean handleMessage(Message msg) {
            if (msg.what == 1) {
                if (isShowing()) {
                    dismiss();
                }
            }
            return true;
        }
    });

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        hideSystemNavigationBar();
        setCancelable(true);
        setCanceledOnTouchOutside(true);
    }

    protected void setAutoDismissTime(int time){
        showTime = time;
    }


    private ReceiveTouchEvent event = new ReceiveTouchEvent();

    @Override
    public boolean dispatchTouchEvent(@NonNull MotionEvent ev) {
        if (showTime > 0){
            handler.removeMessages(1);
            handler.sendEmptyMessageDelayed(1, showTime * 1000);
        }

        if (ev.getAction() == MotionEvent.ACTION_UP || ev.getAction() == MotionEvent.ACTION_DOWN) {
            EventBus.getDefault().post(event);
        }

        return super.dispatchTouchEvent(ev);
    }


    @Override
    public void show() {
        super.show();
        if (showTime > 0){
            handler.removeMessages(1);
            handler.sendEmptyMessageDelayed(1, showTime * 1000);
        }
    }

    protected void setLayoutParams(View view, int bottomSize) {
        RelativeLayout.LayoutParams printParams = (RelativeLayout.LayoutParams) view.getLayoutParams();
        printParams.width = bottomSize;
        printParams.height = bottomSize;
        view.setLayoutParams(printParams);
    }


    /*隐藏虚拟按键*/
    protected void hideSystemNavigationBar() {
        if (Build.VERSION.SDK_INT > 11 && Build.VERSION.SDK_INT < 19) {
            View view = this.getWindow().getDecorView();
            view.setSystemUiVisibility(View.GONE);
        } else if (Build.VERSION.SDK_INT >= 19) {
            Window window = this.getWindow();
            window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);

            getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION);//设置透明导航栏

            int uiOptions =
                    View.SYSTEM_UI_FLAG_LAYOUT_STABLE |
                    View.SYSTEM_UI_FLAG_HIDE_NAVIGATION |
                    View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION |
                    View.SYSTEM_UI_FLAG_IMMERSIVE |
                    View.SYSTEM_UI_FLAG_LOW_PROFILE |
                    View.SYSTEM_UI_FLAG_FULLSCREEN;
            window.getDecorView().setSystemUiVisibility(uiOptions);
        }
    }


    protected void configDialog(float widthMutiple) {
        WindowManager.LayoutParams wl = getWindow().getAttributes();
        wl.gravity = Gravity.BOTTOM | Gravity.CENTER_HORIZONTAL;
        getWindow().setWindowAnimations(R.style.centerDialogWindowAnim);
        getWindow().setAttributes(wl);
        getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        getWindow().setLayout((int) (APPUtil.getScreenWidth() * widthMutiple),
                (int) (APPUtil.getScreenHeight() * 0.8f));

    }

    @Override
    public void dismiss() {
        super.dismiss();
        handler.removeMessages(1);

        EventBus.getDefault().post(new DialogDismissEvent());
    }
}
