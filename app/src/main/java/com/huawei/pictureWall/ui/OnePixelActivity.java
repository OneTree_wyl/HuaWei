package com.huawei.pictureWall.ui;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.PowerManager;
import android.view.Gravity;
import android.view.Window;
import android.view.WindowManager;

import com.huawei.pictureWall.base.BaseActivity;
import com.huawei.pictureWall.tools.util.CLog;
import com.huawei.pictureWall.transfer.service.SenderFileService;

/***
 * @date 创建时间 2018/4/17 20:42
 * @author 作者: yulong
 * @description
 */
public class OnePixelActivity extends BaseActivity {

    private BroadcastReceiver endReceiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //设置1像素
        Window window = getWindow();
        window.setGravity(Gravity.LEFT | Gravity.TOP);
        WindowManager.LayoutParams params = window.getAttributes();
        params.x = 0;
        params.y = 0;
        params.height = 1;
        params.width = 1;
        window.setAttributes(params);


        //结束该页面的广播
        endReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                finish();
            }
        };

        registerReceiver(endReceiver, new IntentFilter("finish activity"));
        //检查屏幕状态
        checkScreen();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (endReceiver != null){
            unregisterReceiver(endReceiver);
            CLog.debug("取消注册:endReceiver" );
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        checkScreen();
    }

    /**
     * 检查屏幕状态  isScreenOn为true  屏幕“亮”结束该Activity
     */
    private void checkScreen() {
        PowerManager pm = (PowerManager) this.getSystemService(Context.POWER_SERVICE);
        boolean isScreenOn = pm.isScreenOn();
        if (isScreenOn) {
            Intent intent = new Intent(this, SenderFileService.class);
            startService(intent);
            CLog.d("启动发送照片服务");

            finish();

        }
    }


}
