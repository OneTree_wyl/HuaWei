package com.huawei.pictureWall.ui.dialog;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.WindowManager;
import android.widget.ImageView;

import com.huawei.pictureWall.R;
import com.huawei.pictureWall.tools.event.ReceiveTouchEvent;
import com.huawei.pictureWall.tools.util.APPUtil;
import com.huawei.pictureWall.tools.util.ImageLoadUtil;
import com.huawei.pictureWall.tools.util.PathUtil;

import org.greenrobot.eventbus.EventBus;

import java.io.File;

/***
 * @date 创建时间 2018/9/30 23:07
 * @author 作者: W.YuLong
 * @description
 */
public class ShowFullImageDialog extends BaseDialog {

    public ShowFullImageDialog(@NonNull Context context) {
        super(context);
    }

    ImageView imageView;
    Bitmap bitmap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_full_screen_image_layout);
        imageView = findViewById(R.id.dialog_full_ImageView);
        configDialog();

        hideSystemNavigationBar();

        //负数表示不发送自动消失的消息
        setAutoDismissTime(-1);

        Bitmap bitmap = ImageLoadUtil.loadBitmapFromFile(
                new File(PathUtil.getGuidePicturePath()), APPUtil.getMinScreenSize());
        imageView.setImageBitmap(bitmap);
    }

    protected void configDialog() {
        WindowManager.LayoutParams wl = getWindow().getAttributes();
        wl.gravity = Gravity.FILL;
        getWindow().setWindowAnimations(R.style.centerDialogWindowAnim);
        getWindow().setAttributes(wl);
        getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
    }


    @Override
    public void show() {
        super.show();
        File file = new File(PathUtil.getGuidePicturePath());
        if (file.exists()) {
            if (bitmap == null && imageView != null) {
                bitmap = ImageLoadUtil.loadBitmapFromFile(
                        new File(PathUtil.getGuidePicturePath()), APPUtil.getMinScreenSize());
                imageView.setImageBitmap(bitmap);
            }
        }
    }

    @Override
    public void dismiss() {
        super.dismiss();
        if (bitmap != null) {
            bitmap.recycle();
            bitmap = null;
        }
    }

    ReceiveTouchEvent event = new ReceiveTouchEvent();

    @Override
    public boolean onTouchEvent(@NonNull MotionEvent ev) {
        if (ev.getAction() == MotionEvent.ACTION_UP) {
            dismiss();
        }

        if (ev.getAction() == MotionEvent.ACTION_UP || ev.getAction() == MotionEvent.ACTION_DOWN) {
            EventBus.getDefault().post(event);
        }
        return super.onTouchEvent(ev);
    }
}
