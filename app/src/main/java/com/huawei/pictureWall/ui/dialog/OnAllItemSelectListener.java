package com.huawei.pictureWall.ui.dialog;

/***
 * @date 创建时间 2018/8/26 23:08
 * @author 作者: W.YuLong
 * @description
 */
public interface OnAllItemSelectListener {

    void isOnItemAllSelected(boolean isAllSelected);
}
