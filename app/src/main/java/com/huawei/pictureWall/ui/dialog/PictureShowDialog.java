package com.huawei.pictureWall.ui.dialog;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v4.print.PrintHelper;
import android.support.v4.view.ViewPager;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.huawei.pictureWall.R;
import com.huawei.pictureWall.base.BasePagerAdapter;
import com.huawei.pictureWall.tools.db.PictureDBSingleton;
import com.huawei.pictureWall.tools.db.entity.PictureInfoEntity;
import com.huawei.pictureWall.tools.event.OnReloadPictureEvent;
import com.huawei.pictureWall.tools.event.UploadFileEvent;
import com.huawei.pictureWall.tools.impl.OnPageChangeListenerImpl;
import com.huawei.pictureWall.tools.singleton.SPSingleton;
import com.huawei.pictureWall.tools.upload.UploadFileUtil;
import com.huawei.pictureWall.tools.util.APPUtil;
import com.huawei.pictureWall.tools.util.AnimatorUtil;
import com.huawei.pictureWall.tools.util.CLog;
import com.huawei.pictureWall.tools.util.DimenSizeUtil;
import com.huawei.pictureWall.tools.util.ImageLoadUtil;
import com.huawei.pictureWall.tools.util.ImageMergeUtil;
import com.huawei.pictureWall.tools.util.PathUtil;
import com.huawei.pictureWall.tools.util.QRCodeUtil;
import com.huawei.pictureWall.tools.util.SPDefine;
import com.huawei.pictureWall.tools.util.ToastUtil;
import com.huawei.pictureWall.tools.util.UIConfig;
import com.huawei.pictureWall.tools.widget.view.PinchImageView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.io.File;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/***
 * @date 创建时间 2018/8/21 22:50
 * @author 作者: W.YuLong
 * @description
 */
public class PictureShowDialog extends BaseDialog implements View.OnClickListener {
    //    @BindView(R.id.picture_show_image_ImageView) PinchImageView photoImageView;
    @BindView(R.id.picture_show_data_ViewPager) ViewPager dataViewPager;
    @BindView(R.id.picture_show_delete_ImageView) ImageView deleteImageView;
    @BindView(R.id.picture_show_print_ImageView) ImageView printImageView;
    @BindView(R.id.picture_show_back_ImageView) ImageView backImageView;
    @BindView(R.id.picture_show_bottom_layout) RelativeLayout bottomLayout;
    @BindView(R.id.picture_show_demo_ImageView) ImageView showImageView;
    @BindView(R.id.picture_show_QR_code_ImageView) ImageView qrCodeImageView;

    @BindView(R.id.picture_show_root_layout) FrameLayout rootLayout;

    @BindView(R.id.picture_show_prev_ImageView) ImageView prevImageView;
    @BindView(R.id.picture_show_next_ImageView) ImageView nextImageView;


    private PictureShowPagerAdapter pagerAdapter;
    private Context context;


    public PictureShowDialog(@NonNull Context context) {
        super(context, R.style.DialogTheme);
        this.context = context;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_pictuer_show_layout);
        ButterKnife.bind(this);

        pagerAdapter = new PictureShowPagerAdapter();
        dataViewPager.setAdapter(pagerAdapter);

        setViewListeners();

        rootLayout.setPadding(0, 0, 0, DimenSizeUtil.getShowAllImageHeight());


        setAutoDismissTime(SPSingleton.get().getInt(SPDefine.KEY_PictureDialogShowTime, 10));

        configDialog(UIConfig.ShowAllDialogWidthMutiple);

        int bottomSize = (int) (0.06f * Math.min(APPUtil.getScreenHeight(), APPUtil.getScreenWidth()));
        int bottomPadding = (int) (bottomSize * 0.2f);
        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) bottomLayout.getLayoutParams();
        params.height = bottomSize;
        params.bottomMargin = bottomPadding;
        bottomLayout.setLayoutParams(params);

        bottomLayout.setPadding(bottomSize, 0, bottomSize, 0);

        setLayoutParams(deleteImageView, bottomSize);
        setLayoutParams(printImageView, bottomSize);
        setLayoutParams(backImageView, bottomSize);
        setLayoutParams(qrCodeImageView, (int) (bottomSize * 1f));

        setLayoutParams(prevImageView, bottomSize);
        setLayoutParams(nextImageView, bottomSize);

        AnimatorUtil.alphaAnimator(prevImageView, null);
        AnimatorUtil.alphaAnimator(nextImageView, null);

        EventBus.getDefault().register(this);
    }


    @Override
    protected void configDialog(float widthMutiple) {
        WindowManager.LayoutParams wl = getWindow().getAttributes();
        wl.gravity = Gravity.BOTTOM | Gravity.CENTER_HORIZONTAL;
        getWindow().setWindowAnimations(R.style.centerDialogWindowAnim);
        getWindow().setAttributes(wl);
        getWindow().setBackgroundDrawableResource(android.R.color.transparent);

        if (APPUtil.getScreenWidth() > APPUtil.getScreenHeight()) {
            getWindow().setLayout((int) (APPUtil.getScreenWidth() * UIConfig.ShowAllDialogWidthMutiple),
                    (int) (APPUtil.getScreenHeight() * 0.9f));
        } else {
            getWindow().setLayout((int) (APPUtil.getScreenHeight() * UIConfig.ShowAllDialogWidthMutiple),
                    (int) (APPUtil.getScreenWidth() * 0.9f));
        }

    }


    private void setViewListeners() {
        backImageView.setOnClickListener(this);
        printImageView.setOnClickListener(this);
        deleteImageView.setOnClickListener(this);
        showImageView.setOnClickListener(this);
        prevImageView.setOnClickListener(this);
        nextImageView.setOnClickListener(this);

        dataViewPager.addOnPageChangeListener(new OnPageChangeListenerImpl() {
            @Override
            public void onPageSelected(int position) {
                if (pagerAdapter.getItem(position) instanceof File) {
                    File file = pagerAdapter.getItem(position);
                    PictureInfoEntity entity = PictureDBSingleton.get().queryFileUrl(file.getName());
                    if (entity != null) {
                        showQrCode(entity.getUrl());
                    } else {
                        UploadFileUtil.doUploadFile(file);

                        qrCodeImageView.setVisibility(View.GONE);
                        CLog.d("无数据");
                    }
                }
            }
        });
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onReceiveEvent(UploadFileEvent event) {
        File file = pagerAdapter.getItem(dataViewPager.getCurrentItem());
        if (file != null && file.getName().equals(event.getFileName())) {
            showQrCode(event.getUrl());
        }
    }

    /*显示二维码*/
    private void showQrCode(String url) {
        Bitmap QRCodeBitmap = QRCodeUtil.createCode(url, APPUtil.dp2px(400), APPUtil.dp2px(3));

        if (QRCodeBitmap != null) {
            ImageLoadUtil.writeBitmapToFile(QRCodeBitmap, PathUtil.getRootPath() + "QRCode.jpg");

            qrCodeImageView.setImageBitmap(QRCodeBitmap);
            qrCodeImageView.setVisibility(View.VISIBLE);
        }
    }

    public void setShowImagePath(int position, List<File> dataList) {
        pagerAdapter.setDataList(dataList);
        dataViewPager.setCurrentItem(position);

        if (dataList != null && dataList.size() <= 1) {
            prevImageView.setVisibility(View.GONE);
            nextImageView.setVisibility(View.GONE);
        }
    }

    @Override
    public void dismiss() {
        super.dismiss();


        EventBus.getDefault().unregister(this);

        Runtime.getRuntime().gc();
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public void onClick(View v) {
        if (v == prevImageView) {
            if (dataViewPager.getCurrentItem() == 0) {
                ToastUtil.showToastShort("当前已是第一张");
            } else {
                dataViewPager.setCurrentItem(dataViewPager.getCurrentItem() - 1, true);
            }
        } else if (v == nextImageView) {
            if (dataViewPager.getCurrentItem() == dataViewPager.getAdapter().getCount() - 1) {
                ToastUtil.showToastShort("当前已是最后一张");
            } else {
                dataViewPager.setCurrentItem(dataViewPager.getCurrentItem() + 1, true);
            }
        } else if (v == backImageView) {
            dismiss();
        } else if (v == showImageView) {
            showImageView.setVisibility(View.GONE);
        } else if (v == deleteImageView) {
            CommAlertDialog.with(getContext()).setMessage("确定删除吗?")
                    .setLeftText("删除").setRightText("取消")
                    .setMessageMinHeight((int) (Math.min(APPUtil.getScreenHeight(), APPUtil.getScreenWidth()) * 0.15))
                    .setCancelAble(true).setClickDismiss(true)
                    .setOnViewClickListener(new OnDialogViewClickListener() {
                        @Override
                        public void onViewClick(Dialog dialog, View v, int tag) {
                            if (tag == CommAlertDialog.TAG_CLICK_LEFT) {
                                File file = pagerAdapter.getItem(dataViewPager.getCurrentItem());
                                if (file.exists()) {
                                    if (file.getAbsolutePath().contains(PathUtil.getDemoPhotoPath())) {
                                        ToastUtil.showToastLong("系统预设照片，不能删除！");
                                    } else {
                                        file.delete();
                                        EventBus.getDefault().post(new OnReloadPictureEvent());
                                        dismiss();

                                    }

                                }

                            }
                        }
                    }).create().show();

        } else if (v == printImageView) {
            File file = pagerAdapter.getItem(dataViewPager.getCurrentItem());
            Bitmap bitmap = ImageMergeUtil.createBitmap(file.getAbsolutePath());


            if (SPSingleton.get().getBoolean(SPDefine.KEY_PrintMode, true)) {
                PrintHelper printHelper = new PrintHelper(context);
                printHelper.setScaleMode(PrintHelper.SCALE_MODE_FIT);
                printHelper.setColorMode(PrintHelper.COLOR_MODE_COLOR);
                printHelper.setOrientation(PrintHelper.ORIENTATION_PORTRAIT);
                // 打印图片
                printHelper.printBitmap(file.getName(), bitmap, new PrintHelper.OnPrintFinishCallback() {
                    @Override
                    public void onFinish() {
//                        ToastUtil.showToastLong("已发送到打印机，正在打印，请稍后");
                    }
                });
            } else {
                showImageView.setImageBitmap(bitmap);
                showImageView.setVisibility(View.VISIBLE);

//                ToastUtil.showToastLong("待开发");
            }

        }
    }


    private static class PictureShowPagerAdapter extends BasePagerAdapter<File> {

        @NonNull
        @Override
        public Object instantiateItem(@NonNull ViewGroup container, int position) {
            View v = LayoutInflater.from(container.getContext()).inflate(R.layout.item_picture_show_layout, container, false);
            PinchImageView imageView = v.findViewById(R.id.item_picture_ImageView);

            File f = dataList.get(position);
            Bitmap bitmap = ImageLoadUtil.getRoundCorner(f, Math.min(APPUtil.getScreenHeight(), APPUtil.getScreenWidth()));
            imageView.setImageBitmap(bitmap);
            container.addView(v);
            return v;
        }
    }

}
