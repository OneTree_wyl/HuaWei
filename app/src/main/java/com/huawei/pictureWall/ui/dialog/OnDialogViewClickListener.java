package com.huawei.pictureWall.ui.dialog;

import android.app.Dialog;
import android.view.View;

/***
 * @date 创建时间 2018/6/7 19:04
 * @author 作者: W.YuLong
 * @description 对话框的点击事件
 */
public interface OnDialogViewClickListener {
    void onViewClick(Dialog dialog, View v, int tag);
}
