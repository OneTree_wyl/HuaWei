package com.huawei.pictureWall.ui;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.huawei.pictureWall.tools.util.CLog;

/***
 * @date 创建时间 2018/4/17 21:03
 * @author 作者: yulong
 * @description
 */
public class OnepxReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
            CLog.debug("接收服务");
        if (intent.getAction().equals(Intent.ACTION_SCREEN_OFF)) {
            Intent it = new Intent(context, OnePixelActivity.class);
            it.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(it);
            CLog.debug("1px--screen off-");
        } else if (intent.getAction().equals(Intent.ACTION_SCREEN_ON)) {
            context.sendBroadcast(new Intent("finish activity"));
            CLog.debug("1px--screen on-");
        }
    }
}
