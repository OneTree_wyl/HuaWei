package com.huawei.pictureWall.ui.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.huawei.pictureWall.R;
import com.huawei.pictureWall.base.BaseRecycleAdapter;
import com.huawei.pictureWall.base.BaseViewHolder;
import com.huawei.pictureWall.tools.util.APPUtil;
import com.huawei.pictureWall.tools.util.ImageLoadUtil;
import com.huawei.pictureWall.tools.util.ToastUtil;
import com.huawei.pictureWall.tools.util.UIConfig;
import com.huawei.pictureWall.ui.dialog.OnAllItemSelectListener;
import com.huawei.pictureWall.ui.dialog.PictureShowDialog;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/***
 * @date 创建时间 2018/8/22 22:32
 * @author 作者: W.YuLong
 * @description
 */
public class ShowAllPictureAdapter extends BaseRecycleAdapter<File, ShowAllPictureAdapter.RectPictureViewHolder> {
    private List<File> selectList = new ArrayList<>();
    private boolean isSelectMode = false;

    private OnAllItemSelectListener onAllItemSelectListener;

    @NonNull
    @Override
    public RectPictureViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new RectPictureViewHolder(parent);
    }

    public void setAllSelectAction(boolean isALlSelect) {
        selectList.clear();
        if (isALlSelect) {
            for (File file : dataList) {
                selectList.add(file);
            }
        }
        notifyDataSetChanged();
    }


    public void setOnAllItemSelectListener(OnAllItemSelectListener onAllItemSelectListener) {
        this.onAllItemSelectListener = onAllItemSelectListener;
    }

    public void setSelectMode(boolean selectMode) {
        isSelectMode = selectMode;
        notifyDataSetChanged();
    }

    public boolean cancelSelectMode() {
        if (isSelectMode) {
            isSelectMode = false;
            setAllSelectAction(false);
            return true;
        }
        return false;
    }


    @Override
    public void onBindViewHolder(RectPictureViewHolder holder, int position) {
//        super.onBindViewHolder(holder, position);
        File file = dataList.get(position % getRealCount());
        holder.onSelectPosition(selectList.contains(file));
        holder.initUIData(file);
        holder.setSelectMode(isSelectMode);

        View.OnClickListener onClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (holder.selectImageView.isSelected()) {
                    if (selectList.contains(file)) {
                        selectList.remove(file);
                    }
                    holder.selectImageView.setSelected(false);
                } else {
                    if (!selectList.contains(file)) {
                        selectList.add(file);
                    }
                    holder.selectImageView.setSelected(true);
                }
                if (onAllItemSelectListener != null) {
                    onAllItemSelectListener.isOnItemAllSelected(
                            selectList.size() > 0 && selectList.size() == getRealCount());
                }

//                notifyDataSetChanged();
            }
        };
        //暂时禁用长按事件
//        holder.rectImageView.setOnLongClickListener(onLongClickListener);
        holder.selectImageView.setOnClickListener(onClickListener);
        holder.rectImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isSelectMode) {
                    holder.selectImageView.performClick();
                } else {
                    if (file != null) {
                        PictureShowDialog dialog = new PictureShowDialog(v.getContext());
                        dialog.show();
                        dialog.setShowImagePath(position % getRealCount(), dataList);
                    } else {
                        ToastUtil.showToastShort("无图片路径");
                    }
                }
            }
        });
    }

    private View.OnLongClickListener onLongClickListener = new View.OnLongClickListener() {
        @Override
        public boolean onLongClick(View v) {
            isSelectMode = true;
            notifyDataSetChanged();
            return false;
        }
    };

    public static class RectPictureViewHolder extends BaseViewHolder implements View.OnClickListener {
        private ImageView rectImageView;
        private ImageView selectImageView;
//        private File file;

        public RectPictureViewHolder(ViewGroup viewGroup) {
            super(viewGroup, R.layout.viewholder_rect_image_view);
            rectImageView = itemView.findViewById(R.id.item_rect_picture_ImageView);
            selectImageView = itemView.findViewById(R.id.item_rect_select_ImageView);

            rectImageView.setOnClickListener(this);
        }

        public void setSelectMode(boolean isSelectMode) {
            selectImageView.setVisibility(isSelectMode ? View.VISIBLE : View.GONE);
        }

        @Override
        public void onSelectPosition(boolean isSelected) {
            selectImageView.setSelected(isSelected);
        }


        @Override
        public <T> void initUIData(T t) {
//            file = (File) t;

            RecyclerView.LayoutParams params = (RecyclerView.LayoutParams) itemView.getLayoutParams();
            int maxSize = Math.max(APPUtil.getScreenHeight(), APPUtil.getScreenWidth());
            params.width = params.height = (int) (maxSize * UIConfig.ShowAllDialogWidthMutiple) / 8;// - APPUtil.dp2px(5);
            itemView.setLayoutParams(params);
//            params.leftMargin = params.rightMargin = params.topMargin = params.bottomMargin = padding;

//            int padding = (int) (params.width * 0.01f);
//            itemView.setPadding(padding, padding, padding, padding);

//            ImageLoadUtil.loadImage(rectImageView, t);


//            rectImageView.setImageBitmap(loadBitmapFromFile((File) t, params.width));
            ImageLoadUtil.loadSmallBitmap(rectImageView, (File)t, params.width);
        }


        @Override
        public void onClick(View v) {

        }
    }


}
