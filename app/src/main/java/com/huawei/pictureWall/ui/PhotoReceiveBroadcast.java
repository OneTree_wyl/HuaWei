package com.huawei.pictureWall.ui;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.huawei.pictureWall.tools.impl.OnReloadPictureListener;
import com.huawei.pictureWall.tools.impl.OnUploadPictureListener;
import com.huawei.pictureWall.tools.upload.UploadFileUtil;
import com.huawei.pictureWall.tools.util.APPUtil;
import com.huawei.pictureWall.tools.util.CLog;
import com.huawei.pictureWall.tools.util.ToastUtil;
import com.huawei.pictureWall.tools.util.ValueConstant;
import com.huawei.pictureWall.ui.dialog.PictureShowDialog;

import java.io.File;
import java.util.ArrayList;

/***
 * @date 创建时间 2018/9/25 22:48
 * @author 作者: W.YuLong
 * @description
 */
public class PhotoReceiveBroadcast extends BroadcastReceiver {
    private boolean isPhoto(String path) {
        return path.endsWith(".png") || path.endsWith(".jpg") || path.endsWith(".jpeg")
                || path.endsWith(".bmp");
    }

    private OnReloadPictureListener onReloadPictureListener;
    private OnUploadPictureListener onUploadPictureListener;

    public void setOnUploadPictureListener(OnUploadPictureListener onUploadPictureListener) {
        this.onUploadPictureListener = onUploadPictureListener;
    }

    public void setOnReloadPictureListener(OnReloadPictureListener onReloadPictureListener) {
        this.onReloadPictureListener = onReloadPictureListener;
    }

    public PhotoReceiveBroadcast() {

    }

    private PictureShowDialog showDialog ;

    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent.getAction() == null || intent.getAction().equals(ValueConstant.RECEIVE_PHOTO)) {
            String path = intent.getStringExtra(ValueConstant.EXTRA_FILE_NAME);
            if (path != null) {
                if (isPhoto(path)) {
                    if (APPUtil.isImageFile(path)) {
                        long time = System.currentTimeMillis();

                        File file = new File(path);

                        if (showDialog != null){
                            showDialog.dismiss();
                        }

                        showDialog = new PictureShowDialog(context);
                        showDialog.show();

                        ArrayList<File> list = new ArrayList();
                        list.add(file);
                        showDialog.setShowImagePath(0, list);

                        if (onReloadPictureListener != null) {
                            onReloadPictureListener.onReloadPicture();
                        }

                        //将接收到的照片进行上传，并且上传成功之后会发送事件消息
                        //通知显示的Dialog显示二维码
                        UploadFileUtil.doUploadFile(file);

//                        if (onUploadPictureListener != null){
//                            onUploadPictureListener.doUploadPicture(file);
//                        }

                        CLog.debug("加载图片时间:" + (System.currentTimeMillis() - time),
                                "接收的图片路径:" + path);

                    } else {
                        ToastUtil.showToastShort("收到的图片文件有问题");
                    }
                } else {
                    CLog.e("收到的错误文件为:" + path);
                    if (path.endsWith(".mp4")) {
                        ToastUtil.showToastLong("视频文件暂不支持，请切换手机的拍摄模式");
                    } else {
                        ToastUtil.showToastLong("当前文件不支持，请尽量调整到系统相机拍照模式");
                    }
                }
            } else {
                ToastUtil.showToastShort("未收到图片");
            }
        }

    }
}
