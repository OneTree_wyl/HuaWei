package com.huawei.pictureWall.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;

import com.huawei.pictureWall.base.BaseRootActivity;
import com.huawei.pictureWall.tools.util.PermissionGroupDefine;
import com.huawei.pictureWall.tools.util.PermissionsUtil;

/***
 * @date 创建时间 2018/8/16 23:28
 * @author 作者: W.YuLong
 * @description 闪屏页
 */
public class SplashActivity extends BaseRootActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        PermissionsUtil.requestPermission(this, PermissionGroupDefine.PERMISSION_SD_CARD, new PermissionsUtil.OnPermissionCallbackImpl() {
            @Override
            public void onSuccess(String[] permission) {
                gotoHomeActivity();
            }

            @Override
            public void onRefused(String[] permission) {
                onBackPressed();
            }
        });
    }


    private void gotoHomeActivity() {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
        finish();
        enterPendingAnim();

    }


}
