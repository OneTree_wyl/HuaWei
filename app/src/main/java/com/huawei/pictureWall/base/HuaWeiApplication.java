package com.huawei.pictureWall.base;

import com.tencent.tinker.loader.app.TinkerApplication;
import com.tencent.tinker.loader.shareutil.ShareConstants;

/***
 * @date 创建时间 2018/6/15 17:58
 * @author 作者: W.YuLong
 * @description
 */
public class HuaWeiApplication extends TinkerApplication {
    public HuaWeiApplication() {
        super(ShareConstants.TINKER_ENABLE_ALL, "com.huawei.pictureWall.base.APP",
                "com.tencent.tinker.loader.TinkerLoader", false);
    }
}