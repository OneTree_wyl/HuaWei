package com.huawei.pictureWall.base;

import android.app.Application;
import android.content.Intent;

import com.huawei.pictureWall.tools.util.APPUtil;
import com.huawei.pictureWall.tools.util.CLog;
import com.hwariot.lib.APILibAPP;
import com.meituan.android.walle.WalleChannelReader;
import com.tencent.bugly.Bugly;
import com.tencent.bugly.crashreport.CrashReport;
import com.tencent.tinker.loader.app.DefaultApplicationLike;
import com.umeng.commonsdk.UMConfigure;

/***
 * @date 创建时间 2018/8/16 22:36
 * @author 作者: W.YuLong
 * @description
 */
public class APP extends DefaultApplicationLike implements APILibAPP.IBridgeInterface {


    public APP(Application application, int tinkerFlags, boolean tinkerLoadVerifyFlag, long applicationStartElapsedTime, long applicationStartMillisTime, Intent tinkerResultIntent) {
        super(application, tinkerFlags, tinkerLoadVerifyFlag, applicationStartElapsedTime, applicationStartMillisTime, tinkerResultIntent);
    }

    private static APP instance;


    public static Application get() {
        return instance.getApplication();
    }

    public static APP getInstance() {
        return instance;
    }


    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;

        initApp();

        APILibAPP.init(get());
        APILibAPP.setBridgeInterface(this);

    }

    // 各种初始化
    private void initApp() {
        boolean isDebug = APPUtil.isApkInDebug();
        String channel = WalleChannelReader.getChannel(get());
        /*注意: 即使您已经在AndroidManifest.xml中配置过appkey和channel值，也需要在App代码中调用初始化接口（如需要使用AndroidManifest.xml中配置好的appkey和channel值，UMConfigure.init调用中appkey和channel参数请置为null）。*/
        UMConfigure.init(get(), "5b759133f29d9875a70003bd", channel, UMConfigure.DEVICE_TYPE_PHONE, "");

        //当前的是否是为开发设备
        Bugly.setIsDevelopmentDevice(get(), isDebug);
        //设置渠道名称
        Bugly.setAppChannel(get(), channel);
        // 这里实现SDK初始化，appId替换成你的在Bugly平台申请的appId
        // 调试时，将第三个参数改为true
        Bugly.init(get(), "b52e5ba98c", isDebug);

        CrashReport.UserStrategy strategy = new CrashReport.UserStrategy(get());
        strategy.setAppChannel(channel);  //设置渠道
        strategy.setAppVersion(APPUtil.getVersionName());      //App的版本
        strategy.setAppPackageName(get().getPackageName());  //App的包名
        CLog.d("当前的渠道为:" + channel);

    }


    @Override
    public String getToken() {
        return "";
    }

    @Override
    public String getBaseAPIUrl(String serviceType) {
        return "http://api.easy-promoting.com";
    }

    @Override
    public String getDefaultService() {
        return "";
    }

    @Override
    public String getAPIVersion() {
        return "";
    }

    @Override
    public String getSerialNumberId() {
        return "";
    }

    @Override
    public void tokenExpired() {

    }
}
